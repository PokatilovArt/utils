# @os-team/escape-string-regexp [![NPM version](https://img.shields.io/npm/v/@os-team/escape-string-regexp)](https://yarnpkg.com/package/@os-team/escape-string-regexp) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/escape-string-regexp)](https://bundlephobia.com/result?p=@os-team/escape-string-regexp)

Escapes special characters in a string for regular expressions.

## Usage

### Install the package

Install the package using the following command:

```
yarn add @os-team/escape-string-regexp
```

### Simple usage

```ts
import escapeStringRegexp from 'escape-string-regexp';

const escapedString = escapeStringRegexp('a\\b'); // a\\b
```

Based on [escape-string-regexp](https://github.com/sindresorhus/escape-string-regexp).
