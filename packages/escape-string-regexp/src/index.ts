const escapeStringRegexp = (value: string): string =>
  value.replace(/[|\\{}()[\]^$+*?.]/g, '\\$&').replace(/-/g, '\\x2d');

export default escapeStringRegexp;
