# @os-team/i18next-tld-language-detector [![NPM version](https://img.shields.io/npm/v/@os-team/i18next-tld-language-detector)](https://yarnpkg.com/package/@os-team/i18next-tld-language-detector) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/i18next-tld-language-detector)](https://bundlephobia.com/result?p=@os-team/i18next-tld-language-detector)

The i18next language detector, which is used to detect the user's language by a top-level domain (TLD).

## Usage

Install the package using the following command:

```
yarn add @os-team/i18next-tld-language-detector
```

### Client-side usage example

```ts
import i18next from 'i18next';
import HttpBackend from 'i18next-http-backend';
import LanguageDetector from 'i18next-browser-languagedetector';
import { browserTldLanguageDetector } from '@os-team/i18next-tld-language-detector';
import { initReactI18next } from 'react-i18next';

const languageDetector = new LanguageDetector();
languageDetector.addDetector(browserTldLanguageDetector); // Add tldLanguageDetector

i18next
  .use(HttpBackend)
  .use(languageDetector)
  .use(initReactI18next)
  .init({
    detection: {
      order: ['cookie', 'tld'], // Add tld
      lookupCookie: 'lng',
      caches: ['cookie'],
    },
  });

export default i18next;
```

### Server-side usage example

```ts
import i18next from 'i18next';
import FsBackend from 'i18next-fs-backend';
import { LanguageDetector } from 'i18next-http-middleware';
import { httpTldLanguageDetector } from '@os-team/i18next-tld-language-detector';
import path from 'path';

const languageDetector = new LanguageDetector();
languageDetector.addDetector(httpTldLanguageDetector); // Add tldLanguageDetector

i18next
  .use(FsBackend)
  .use(languageDetector)
  .init({
    fallbackLng: 'en',
    ns: ['validation'],
    backend: {
      loadPath: path.join(__dirname, '../locales/{{lng}}/{{ns}}.json'),
    },
    detection: {
      order: ['tld'], // Add tld
    },
  });
```
