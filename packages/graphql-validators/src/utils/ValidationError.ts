import { ApolloError } from 'apollo-server-express';

export interface Constraint {
  name: string;
  message: string;
}

class ValidationError extends ApolloError {
  public constructor(constraints: Record<string, Constraint>) {
    super('Failed to make request due to validation errors', 'BAD_ARGUMENTS', {
      constraints,
    });
    Object.defineProperty(this, 'name', { value: 'ValidationError' });
  }
}

export default ValidationError;
