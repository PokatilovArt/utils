import { createMethodDecorator, ResolverData } from 'type-graphql';
import ValidationError, { Constraint } from './ValidationError';

/* eslint-disable guard-for-in,no-restricted-syntax,no-await-in-loop,@typescript-eslint/no-explicit-any,no-continue */

export interface Validator {
  name: string;
  validate: (value: any, data: ResolverData) => Promise<boolean> | boolean;
  isWall?: boolean;
  tKeys?: Record<string, any>;
}

export interface ValidateArgsOptions {
  arg?: string;
  ns?: string;
  tKey?: string;
}

const ValidateArgs = (
  validatorsMap: Record<string, Validator[]>,
  options: ValidateArgsOptions = {}
): MethodDecorator => {
  const { arg, ns = 'validation', tKey } = options;
  const tKeyPrefix = tKey ? `${tKey}.` : '';

  return createMethodDecorator<any>(async (data, next) => {
    const { args, context } = data;
    const input = arg ? args[arg] : args;

    // Check if the input is an object
    if (typeof input !== 'object') {
      throw new Error(
        `The ${arg ? `${arg} argument` : 'args'} must be an object`
      );
    }

    const constraints: Record<string, Constraint> = {};
    const { t } = context.req;

    // Validate the argument and save the error messages
    for (const key in validatorsMap) {
      const validators = validatorsMap[key];
      for (const validator of validators) {
        // Run the current validator
        const isValid = await validator.validate(input[key], data);

        // Skip the next validators if the current validator is wall and is valid
        if (validator.isWall) {
          if (isValid) break;
          else continue;
        }

        // Add a new constraint if the value is invalid
        if (!isValid) {
          const validatorTKey = `${tKeyPrefix}${key}.${validator.name}`;
          constraints[key] = {
            name: validator.name,
            message:
              typeof t === 'function'
                ? t(`${ns}:${validatorTKey}`, validator.tKeys)
                : validatorTKey,
          };
          break;
        }
      }
    }

    // Throw out the error if there are any constraints
    if (Object.keys(constraints).length > 0) {
      throw new ValidationError(constraints);
    }

    return next();
  });
};

export default ValidateArgs;
