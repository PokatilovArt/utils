import { Validator } from '../utils/ValidateArgs';

/**
 * Checks if the trimmed string is not empty.
 */
const isNotEmpty: Validator = {
  name: 'isNotEmpty',
  validate: (value) => typeof value === 'string' && value.trim().length > 0,
};

export default isNotEmpty;
