import { Validator } from '../utils/ValidateArgs';

// Supports unicode
const EMAIL_RE =
  /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;

/**
 * Checks if the string is an email.
 */
const isEmail: Validator = {
  name: 'isEmail',
  validate: (value) => typeof value === 'string' && EMAIL_RE.test(value),
};

export default isEmail;
