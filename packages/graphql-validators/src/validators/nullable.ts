import { Validator } from '../utils/ValidateArgs';

/**
 * Checks if the value is null.
 */
const nullable: Validator = {
  name: 'nullable',
  validate: (value) => value === undefined || value === null,
  isWall: true,
};

export default nullable;
