import { Validator } from '../utils/ValidateArgs';

/**
 * Checks if the string is equal to one of the given values.
 */
const oneOf = (values: string[] | ReadonlyArray<string>): Validator => ({
  name: 'oneOf',
  validate: (value) => typeof value === 'string' && values.includes(value),
  tKeys: { values },
});

export default oneOf;
