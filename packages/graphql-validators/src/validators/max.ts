import { Validator } from '../utils/ValidateArgs';

/**
 * Checks if the number is not more than given number.
 */
const max = (n: number): Validator => ({
  name: 'max',
  validate: (value) => typeof value === 'number' && value <= n,
  tKeys: { max: n },
});

export default max;
