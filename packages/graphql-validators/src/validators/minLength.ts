import { Validator } from '../utils/ValidateArgs';

/**
 * Checks if the string length is not less than given number.
 */
const minLength = (n: number): Validator => ({
  name: 'minLength',
  validate: (value) => typeof value === 'string' && value.length >= n,
  tKeys: { min: n },
});

export default minLength;
