import { Validator } from '../utils/ValidateArgs';

/**
 * Checks if the string length is equal to the given number.
 */
const length = (n: number): Validator => ({
  name: 'length',
  validate: (value) => typeof value === 'string' && value.length === n,
  tKeys: { length: n },
});

export default length;
