import { Validator } from 'class-validator';
import MinLength from '../validators/MinLength';

const validator = new Validator();

const MIN_LENGTH = 2;

class User {
  @MinLength(MIN_LENGTH)
  name!: string;
}

it('Should succeed', () => {
  expect.assertions(1);
  const user = new User();
  user.name = 'a'.repeat(MIN_LENGTH);
  return validator.validate(user).then((errors) => {
    expect(errors.length).toEqual(0);
    return undefined;
  });
});

it('Should fail', () => {
  expect.assertions(1);
  const user = new User();
  user.name = 'a'.repeat(MIN_LENGTH - 1);
  return validator.validate(user).then((errors) => {
    expect(errors.length).toEqual(1);
    return undefined;
  });
});
