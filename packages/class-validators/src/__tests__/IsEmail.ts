import { Validator } from 'class-validator';
import IsEmail from '../validators/IsEmail';

const validator = new Validator();

class User {
  @IsEmail()
  email!: string;
}

it('Should succeed', () => {
  expect.assertions(1);
  const user = new User();
  user.email = 'name@domain.com';
  return validator.validate(user).then((errors) => {
    expect(errors.length).toEqual(0);
    return undefined;
  });
});

it('Should fail', () => {
  expect.assertions(1);
  const user = new User();
  user.email = 'name';
  return validator.validate(user).then((errors) => {
    expect(errors.length).toEqual(1);
    return undefined;
  });
});
