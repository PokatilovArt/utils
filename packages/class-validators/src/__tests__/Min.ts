import { Validator } from 'class-validator';
import Min from '../validators/Min';

const validator = new Validator();

const MIN = 10;

class Payment {
  @Min(MIN)
  amount!: number;
}

it('Should succeed', () => {
  expect.assertions(1);
  const payment = new Payment();
  payment.amount = MIN;
  return validator.validate(payment).then((errors) => {
    expect(errors.length).toEqual(0);
    return undefined;
  });
});

it('Should fail', () => {
  expect.assertions(1);
  const payment = new Payment();
  payment.amount = MIN - 1;
  return validator.validate(payment).then((errors) => {
    expect(errors.length).toEqual(1);
    return undefined;
  });
});
