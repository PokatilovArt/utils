import { Validator } from 'class-validator';
import IsNotEmpty from '../validators/IsNotEmpty';

const validator = new Validator();

class User {
  @IsNotEmpty()
  name!: string;
}

it('Should succeed', () => {
  expect.assertions(1);
  const user = new User();
  user.name = 'name';
  return validator.validate(user).then((errors) => {
    expect(errors.length).toEqual(0);
    return undefined;
  });
});

it('Should fail', () => {
  expect.assertions(1);
  const user = new User();
  user.name = '  ';
  return validator.validate(user).then((errors) => {
    expect(errors.length).toEqual(1);
    return undefined;
  });
});
