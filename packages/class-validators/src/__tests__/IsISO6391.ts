import { Validator } from 'class-validator';
import IsISO6391 from '../validators/IsISO6391';

const validator = new Validator();

class User {
  @IsISO6391()
  languageCode!: string;
}

it('Should succeed', () => {
  expect.assertions(1);
  const user = new User();
  user.languageCode = 'en';
  return validator.validate(user).then((errors) => {
    expect(errors.length).toEqual(0);
    return undefined;
  });
});

it('Should fail', () => {
  expect.assertions(1);
  const user = new User();
  user.languageCode = 'zz';
  return validator.validate(user).then((errors) => {
    expect(errors.length).toEqual(1);
    return undefined;
  });
});
