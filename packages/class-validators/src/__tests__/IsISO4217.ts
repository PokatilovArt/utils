import { Validator } from 'class-validator';
import IsISO4217 from '../validators/IsISO4217';

const validator = new Validator();

class Payment {
  @IsISO4217()
  currency!: string;
}

it('Should succeed', () => {
  expect.assertions(1);
  const payment = new Payment();
  payment.currency = 'USD';
  return validator.validate(payment).then((errors) => {
    expect(errors.length).toEqual(0);
    return undefined;
  });
});

it('Should fail', () => {
  expect.assertions(1);
  const payment = new Payment();
  payment.currency = 'ABC';
  return validator.validate(payment).then((errors) => {
    expect(errors.length).toEqual(1);
    return undefined;
  });
});
