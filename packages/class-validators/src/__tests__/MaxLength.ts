import { Validator } from 'class-validator';
import MaxLength from '../validators/MaxLength';

const validator = new Validator();

const MAX_LENGTH = 40;

class User {
  @MaxLength(MAX_LENGTH)
  name!: string;
}

it('Should succeed', () => {
  expect.assertions(1);
  const user = new User();
  user.name = 'a'.repeat(MAX_LENGTH);
  return validator.validate(user).then((errors) => {
    expect(errors.length).toEqual(0);
    return undefined;
  });
});

it('Should fail', () => {
  expect.assertions(1);
  const user = new User();
  user.name = 'a'.repeat(MAX_LENGTH + 1);
  return validator.validate(user).then((errors) => {
    expect(errors.length).toEqual(1);
    return undefined;
  });
});
