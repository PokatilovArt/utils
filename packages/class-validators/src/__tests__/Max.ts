import { Validator } from 'class-validator';
import Max from '../validators/Max';

const validator = new Validator();

const MAX = 1000;

class Payment {
  @Max(MAX)
  amount!: number;
}

it('Should succeed', () => {
  expect.assertions(1);
  const payment = new Payment();
  payment.amount = MAX;
  return validator.validate(payment).then((errors) => {
    expect(errors.length).toEqual(0);
    return undefined;
  });
});

it('Should fail', () => {
  expect.assertions(1);
  const payment = new Payment();
  payment.amount = MAX + 1;
  return validator.validate(payment).then((errors) => {
    expect(errors.length).toEqual(1);
    return undefined;
  });
});
