import { Validator } from 'class-validator';
import IsInt from '../validators/IsInt';

const validator = new Validator();

class User {
  @IsInt()
  balance!: number;
}

it('Should succeed', () => {
  expect.assertions(1);
  const user = new User();
  user.balance = 10;
  return validator.validate(user).then((errors) => {
    expect(errors.length).toEqual(0);
    return undefined;
  });
});

it('Should fail', () => {
  expect.assertions(1);
  const user = new User();
  user.balance = 10.5;
  return validator.validate(user).then((errors) => {
    expect(errors.length).toEqual(1);
    return undefined;
  });
});
