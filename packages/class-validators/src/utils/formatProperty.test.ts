import formatProperty from './formatProperty';

it('Should insert space before capital letters', () => {
  expect(formatProperty('camelCaseCamelCase')).toBe('camel case camel case');
});

it('Should capitalize the first letter', () => {
  expect(formatProperty('camelCaseCamelCase', true)).toBe(
    'Camel case camel case'
  );
});
