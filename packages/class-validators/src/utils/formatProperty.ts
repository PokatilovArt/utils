const formatProperty = (property: string, capitalize = false): string => {
  const res = property
    .replace(/([A-Z])/g, ' $1')
    .trimLeft()
    .toLowerCase();
  if (capitalize) return res.replace(/^\w/, (c) => c.toUpperCase());
  return res;
};

export default formatProperty;
