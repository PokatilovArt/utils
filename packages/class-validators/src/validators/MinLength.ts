import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
} from 'class-validator';
import formatProperty from '../utils/formatProperty';

/**
 * Checks if the string length is not less than given number.
 */
const MinLength =
  (min: number, validationOptions?: ValidationOptions) =>
  (
    object: Object, // eslint-disable-line @typescript-eslint/ban-types
    propertyName: string
  ): void => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      name: 'minLength',
      options: validationOptions,
      constraints: [],
      validator: {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        validate(value: any) {
          return typeof value === 'string' && value.length >= min;
        },
        defaultMessage(validationArguments?: ValidationArguments): string {
          const property = validationArguments
            ? formatProperty(validationArguments.property)
            : 'field';
          return `Please give a longer ${property}, from ${min} characters`;
        },
      },
    });
  };

export default MinLength;
