import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
} from 'class-validator';
import formatProperty from '../utils/formatProperty';

/**
 * Checks if the trimmed string is not empty.
 */
const IsNotEmpty =
  (validationOptions?: ValidationOptions) =>
  (
    object: Object, // eslint-disable-line @typescript-eslint/ban-types
    propertyName: string
  ): void => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      name: 'isEmpty',
      options: validationOptions,
      constraints: [],
      validator: {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        validate(value: any) {
          return typeof value === 'string' && value.trim().length > 0;
        },
        defaultMessage(validationArguments?: ValidationArguments): string {
          const property = validationArguments
            ? formatProperty(validationArguments.property)
            : 'field';
          return `Please enter the ${property}`;
        },
      },
    });
  };

export default IsNotEmpty;
