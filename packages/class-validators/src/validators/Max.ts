import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
} from 'class-validator';
import formatProperty from '../utils/formatProperty';

/**
 * Checks if the number is not more than given number.
 */
const Max =
  (max: number, validationOptions?: ValidationOptions) =>
  (
    object: Object, // eslint-disable-line @typescript-eslint/ban-types
    propertyName: string
  ): void => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      name: 'max',
      options: validationOptions,
      constraints: [],
      validator: {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        validate(value: any) {
          return typeof value === 'number' && value <= max;
        },
        defaultMessage(validationArguments?: ValidationArguments): string {
          const property = validationArguments
            ? formatProperty(validationArguments.property)
            : 'field';
          return `Please give a smaller ${property}, up to ${max}`;
        },
      },
    });
  };

export default Max;
