import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
} from 'class-validator';
import formatProperty from '../utils/formatProperty';

/**
 * Checks if the number is not less than given number.
 */
const Min =
  (min: number, validationOptions?: ValidationOptions) =>
  (
    object: Object, // eslint-disable-line @typescript-eslint/ban-types
    propertyName: string
  ): void => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      name: 'min',
      options: validationOptions,
      constraints: [],
      validator: {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        validate(value: any) {
          return typeof value === 'number' && value >= min;
        },
        defaultMessage(validationArguments?: ValidationArguments): string {
          const property = validationArguments
            ? formatProperty(validationArguments.property)
            : 'field';
          return `Please give a larger ${property}, from ${min}`;
        },
      },
    });
  };

export default Min;
