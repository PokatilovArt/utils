import { registerDecorator, ValidationOptions } from 'class-validator';

// Supports unicode
const EMAIL_RE =
  /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;

/**
 * Checks if the string is an email.
 */
const IsEmail =
  (validationOptions?: ValidationOptions) =>
  (
    object: Object, // eslint-disable-line @typescript-eslint/ban-types
    propertyName: string
  ): void => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      name: 'isEmail',
      options: validationOptions,
      constraints: [],
      validator: {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        validate(value: any) {
          return typeof value === 'string' && EMAIL_RE.test(value);
        },
        defaultMessage(): string {
          return 'Should be an email';
        },
      },
    });
  };

export default IsEmail;
