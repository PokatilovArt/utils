import { registerDecorator, ValidationOptions } from 'class-validator';

/**
 * Checks if the number is an integer.
 */
const IsInt =
  (validationOptions?: ValidationOptions) =>
  (
    object: Object, // eslint-disable-line @typescript-eslint/ban-types
    propertyName: string
  ): void => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      name: 'isInt',
      options: validationOptions,
      constraints: [],
      validator: {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        validate(value: any) {
          return Number.isInteger(value);
        },
        defaultMessage(): string {
          return 'Should be an integer';
        },
      },
    });
  };

export default IsInt;
