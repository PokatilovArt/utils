import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
} from 'class-validator';
import formatProperty from '../utils/formatProperty';

/**
 * Checks if the string length is not more than given number.
 */
const MaxLength =
  (max: number, validationOptions?: ValidationOptions) =>
  (
    object: Object, // eslint-disable-line @typescript-eslint/ban-types
    propertyName: string
  ): void => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      name: 'maxLength',
      options: validationOptions,
      constraints: [],
      validator: {
        // eslint-disable-next-line @typescript-eslint/no-explicit-any
        validate(value: any) {
          return typeof value === 'string' && value.length <= max;
        },
        defaultMessage(validationArguments?: ValidationArguments): string {
          const property = validationArguments
            ? formatProperty(validationArguments.property)
            : 'field';
          return `Please give a shorter ${property}, up to ${max} characters`;
        },
      },
    });
  };

export default MaxLength;
