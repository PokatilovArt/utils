# @os-team/class-validators [![NPM version](https://img.shields.io/npm/v/@os-team/class-validators)](https://yarnpkg.com/package/@os-team/class-validators) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/class-validators)](https://bundlephobia.com/result?p=@os-team/class-validators)

Additional class validators.

- `@IsEmail()` – Checks if the string is an email.
- `@IsInt()` – Checks if the number is an integer.
- `@IsISO4217()` – Checks if the string is a valid ISO 4217.
- `@IsISO6391()` – Checks if the string is a valid ISO 639-1.
- `@IsNotEmpty()` – Checks if the trimmed string is not empty.
- `@Max(n)` – Checks if the number is not more than given number.
- `@MaxLength(n)` – Checks if the string length is not more than given number.
- `@Max(n)` – Checks if the number is not less than given number.
- `@MinLength(n)` – Checks if the string length is not less than given number.

## Installation

Install the package using the following command:

```
yarn add @os-team/class-validators
```
