import { Field, ObjectType } from 'type-graphql';

@ObjectType({
  description: JSON.stringify({
    en: 'The object that stores the status of the operation.',
    ru: 'Объект, в котором хранится статус операции.',
  }),
})
class StatusPayload {
  @Field({
    description: JSON.stringify({
      en: 'Whether the operation was completed successfully.',
      ru: 'Была ли операция завершена успешно.',
    }),
  })
  public ok!: boolean;
}

export default StatusPayload;
