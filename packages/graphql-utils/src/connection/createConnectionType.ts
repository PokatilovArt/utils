// eslint-disable-next-line max-classes-per-file
import { ClassType, Field, ObjectType } from 'type-graphql';
import PageInfo from './PageInfo';
import {
  ConnectionCursor,
  Edge as EdgeType,
  Connection as ConnectionType,
} from './connectionTypes';

const createConnectionType = <T>(
  NodeType: ClassType<T>
): ClassType<ConnectionType<T>> => {
  @ObjectType(`${NodeType.name}Edge`, {
    description: JSON.stringify({
      en: 'A list item.',
      ru: 'Элемент списка.',
    }),
  })
  class Edge implements EdgeType<T> {
    @Field(() => NodeType, {
      description: JSON.stringify({
        en: 'An object.',
        ru: 'Объект.',
      }),
    })
    node!: T;

    @Field({
      description: JSON.stringify({
        en: 'A cursor that is used to get the next page.',
        ru: 'Курсор, который используется для получения следующей страницы.',
      }),
    })
    cursor!: ConnectionCursor;
  }

  @ObjectType(`${NodeType.name}Connection`, {
    description: JSON.stringify({
      en: 'A type that provides a standard mechanism for paginating the result set.',
      ru: 'Тип, обеспечивающий стандартный механизм разбиения набора результатов на страницы.',
    }),
  })
  class Connection implements ConnectionType<T> {
    @Field(() => [Edge], {
      description: JSON.stringify({
        en: 'Item list.',
        ru: 'Список элементов.',
      }),
    })
    edges!: Edge[];

    @Field({
      description: JSON.stringify({
        en: 'Information about the current page.',
        ru: 'Информация о текущей странице.',
      }),
    })
    pageInfo!: PageInfo;
  }

  return Connection;
};

export default createConnectionType;
