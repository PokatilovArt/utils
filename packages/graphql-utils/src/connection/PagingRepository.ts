import {
  EntityRepository,
  FindManyOptions,
  FindOneOptions,
  FindOperator,
  LessThan,
  MoreThan,
  Repository,
} from 'typeorm';
import { FindConditions } from 'typeorm/find-options/FindConditions';
import ConnectionArgs from './ConnectionArgs';
import getPagingParams from './getPagingParams';
import BaseEntity from '../node/BaseEntity';
import { Connection } from './connectionTypes';
import connectionFromArray from './connectionFromArray';

interface Options<T> extends FindOneOptions<T> {
  where?: FindConditions<T>[] | FindConditions<T>;
}

const getIdFindOperator = (
  operator: '>' | '<',
  id: number
): FindOperator<number> => (operator === '>' ? MoreThan(id) : LessThan(id));

@EntityRepository()
class PagingRepository<T extends BaseEntity> extends Repository<T> {
  public async findAndPaginate(
    args: ConnectionArgs,
    options?: Options<T>
  ): Promise<Connection<T>> {
    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    const { where = {}, order = {}, ...restOptions } = (options || {}) as any;
    const { id, ...restOrder } = order;
    const { limit, sortOrder, condition } = getPagingParams(args, {
      reverseOrder: id === 'DESC' || id === -1,
    });

    if (process.env.NODE_ENV !== 'production' && where.id) {
      throw new Error(
        'You cannot specify where.id in the findAndPaginate method'
      );
    }

    const findOptions: FindManyOptions<T> = {
      where,
      order: {
        id: sortOrder,
        ...restOrder,
      },
      take: limit + 1, // To detect whether there are more nodes
      ...restOptions,
    };

    if (condition) {
      findOptions.where = Array.isArray(where)
        ? where.map((item) => ({
            id: getIdFindOperator(condition.operator, condition.params.id),
            ...item,
          }))
        : {
            id: getIdFindOperator(condition.operator, condition.params.id),
            ...where,
          };
    }

    const nodes = await this.find(findOptions);

    return connectionFromArray({ nodes, args, limit });
  }
}

export default PagingRepository;
