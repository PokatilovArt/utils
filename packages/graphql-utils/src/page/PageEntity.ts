import { Column } from 'typeorm';
import { Field, ObjectType } from 'type-graphql';
import BaseEntity from '../node/BaseEntity';
import pageDescription from './pageDescriptions.json';

@ObjectType({ isAbstract: true })
class PageEntity extends BaseEntity {
  /**
   * Used to find an object by URL.
   */
  @Field({
    description: JSON.stringify(pageDescription.urlSlug),
  })
  @Column({ unique: true })
  public urlSlug!: string;

  /**
   * The title meta tag.
   */
  @Field({
    description: JSON.stringify(pageDescription.metaTitle),
  })
  @Column()
  public metaTitle!: string;

  /**
   * The description meta tag.
   */
  @Field({
    description: JSON.stringify(pageDescription.metaDescription),
  })
  @Column()
  public metaDescription!: string;
}

export default PageEntity;
