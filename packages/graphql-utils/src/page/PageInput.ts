import { Field, InputType } from 'type-graphql';
import { isNotEmpty, maxLength, minLength } from '@os-team/graphql-validators';
import pageDescription from './pageDescriptions.json';

@InputType({ isAbstract: true })
class PageInput {
  @Field({
    description: JSON.stringify(pageDescription.urlSlug),
  })
  urlSlug!: string;

  @Field({
    description: JSON.stringify(pageDescription.metaTitle),
  })
  metaTitle!: string;

  @Field({
    description: JSON.stringify(pageDescription.metaDescription),
  })
  metaDescription!: string;
}

const TEST = process.env.NODE_ENV === 'test';

export const pageValidators = {
  urlSlug: [isNotEmpty, maxLength(65)],
  metaTitle: [isNotEmpty, minLength(TEST ? 0 : 35), maxLength(100)],
  metaDescription: [isNotEmpty, minLength(TEST ? 0 : 70), maxLength(320)],
};

export default PageInput;
