import { DeleteDateColumn } from 'typeorm';
import { Field, ObjectType } from 'type-graphql';
import BaseEntity from './BaseEntity';

@ObjectType({ isAbstract: true })
abstract class BaseDeletableEntity extends BaseEntity {
  @Field(() => Date, {
    nullable: true,
    description: JSON.stringify({
      en: 'The date the object was deleted.',
      ru: 'Дата удаления объекта.',
    }),
  })
  @DeleteDateColumn()
  public deletedAt?: Date | null;
}

export default BaseDeletableEntity;
