import {
  BaseEntity as TypeORMBaseEntity,
  CreateDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Field, ID, ObjectType } from 'type-graphql';
import Node from './Node';

@ObjectType({ isAbstract: true })
abstract class BaseEntity extends TypeORMBaseEntity implements Node {
  @Field(() => ID, {
    description: JSON.stringify({
      en: 'The object ID.',
      ru: 'ID объекта.',
    }),
  })
  @PrimaryGeneratedColumn()
  public id!: number;

  @Field({
    description: JSON.stringify({
      en: 'The date the object was created.',
      ru: 'Дата создания объекта.',
    }),
  })
  @CreateDateColumn()
  public createdAt!: Date;

  @Field({
    description: JSON.stringify({
      en: 'The date of the last update of the object.',
      ru: 'Дата последнего обновления объекта.',
    }),
  })
  @UpdateDateColumn()
  public updatedAt!: Date;
}

export default BaseEntity;
