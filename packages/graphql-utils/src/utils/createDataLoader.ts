import DataLoader from 'dataloader';
import { Connection, ObjectType } from 'typeorm';
import BaseEntity from '../node/BaseEntity';

const batch =
  <T extends BaseEntity>(conn: Connection, target: ObjectType<T>) =>
  async (ids) => {
    const entities = await conn.getRepository(target).findByIds(ids);

    const entitiesMap: { [key: number]: T } = {};
    entities.forEach((item) => {
      entitiesMap[item.id] = item;
    });

    return ids.map((id) => entitiesMap[id] || null);
  };

/**
 * Creates the data loader instance for the target entity.
 */
const createDataLoader = <T extends BaseEntity>(
  conn: Connection,
  target: ObjectType<T>
): DataLoader<number, T> => new DataLoader<number, T>(batch(conn, target));

export default createDataLoader;
