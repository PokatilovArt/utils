import BaseEntity from '../node/BaseEntity';
import Base64 from './Base64';
import GlobalId from './GlobalId';

class Example extends BaseEntity {}

describe('encode', () => {
  it('Should encode by entity class and number id', () => {
    expect(GlobalId.encode(Example, 10)).toBe(Base64.encode('Example:10'));
  });

  it('Should throw the error because the id is required', () => {
    const entity = new Example();
    expect(() => GlobalId.encode(entity)).toThrowError();
  });

  it('Should encode by entity', () => {
    const entity = new Example();
    entity.id = 10;
    expect(GlobalId.encode(entity)).toBe(GlobalId.encode(Example, 10));
  });
});

describe('decode', () => {
  it('Should throw the error because the global id is incorrect', () => {
    const globalId = Base64.encode('Example10');
    expect(() => GlobalId.decode(Example, globalId)).toThrowError();
  });

  it('Should throw the error because the entity class name is incorrect', () => {
    const globalId = Base64.encode('Incorrect:10');
    expect(() => GlobalId.decode(Example, globalId)).toThrowError();
  });

  it('Should decode', () => {
    const globalId = Base64.encode('Example:10');
    const id = GlobalId.decode(Example, globalId);
    expect(id).toBe(10);
  });
});
