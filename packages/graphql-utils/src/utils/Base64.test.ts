import Base64 from './Base64';

describe('encode', () => {
  it('Should encode and replace + to -', () => {
    expect(Base64.encode('~~~')).toBe('fn5-'); // Pure base64: fn5+
  });

  it('Should encode and replace / to _', () => {
    expect(Base64.encode('???')).toBe('Pz8_'); // Pure base64: Pz8/
  });

  it('Should encode and remove ==', () => {
    expect(Base64.encode('0')).toBe('MA'); // Pure base64: MA==
  });

  it('Should encode and remove =', () => {
    expect(Base64.encode('00')).toBe('MDA'); // Pure base64: MDA=
  });

  it('Should encode without any change', () => {
    expect(Base64.encode('000')).toBe('MDAw'); // Pure base64: MDAw
  });
});

describe('decode', () => {
  it('Should replace - to + and decode', () => {
    expect(Base64.decode('fn5-')).toBe('~~~'); // Pure base64: fn5+
  });

  it('Should replace _ to / and decode', () => {
    expect(Base64.decode('Pz8_')).toBe('???'); // Pure base64: Pz8/
  });

  it('Should append == and decode', () => {
    expect(Base64.decode('MA')).toBe('0'); // Pure base64: MA==
  });

  it('Should append = and decode', () => {
    expect(Base64.decode('MDA')).toBe('00'); // Pure base64: MDA=
  });

  it('Should decode without any change', () => {
    expect(Base64.decode('MDAw')).toBe('000'); // Pure base64: MDAw
  });
});
