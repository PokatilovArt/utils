# @os-team/graphql-utils [![NPM version](https://img.shields.io/npm/v/@os-team/graphql-utils)](https://yarnpkg.com/package/@os-team/graphql-utils) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/graphql-utils)](https://bundlephobia.com/result?p=@os-team/graphql-utils)

Shared utils for GraphQL.

## Installation

Install the package using the following command:

```
yarn add @os-team/graphql-utils
```

Install peer dependencies using the following command:

```
npx install-peerdeps @os-team/graphql-utils
```
