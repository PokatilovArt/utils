# @os-team/i18next-react-native-language-detector [![NPM version](https://img.shields.io/npm/v/@os-team/i18next-react-native-language-detector)](https://yarnpkg.com/package/@os-team/i18next-react-native-language-detector) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/i18next-react-native-language-detector)](https://bundlephobia.com/result?p=@os-team/i18next-react-native-language-detector)

The i18next language detector, which is used to detect the user's language in React Native.

## Usage

Install the package using the following command:

```
yarn add @os-team/i18next-react-native-language-detector
```

Example of usage:

```ts
import i18next from 'i18next';
import { initReactI18next } from 'react-i18next';
import RNLanguageDetector from '@os-team/i18next-react-native-language-detector';

i18next
  .use(RNLanguageDetector) // Add the language detector
  .use(initReactI18next)
  .init({
    fallbackLng: 'en',
    supportedLngs: ['en', 'ru'],
    ns: [],
    defaultNS: undefined,

    resources: {
      en: {
        common: require('./locales/en/common.json'),
      },
      ru: {
        common: require('./locales/ru/common.json'),
      },
    },

    interpolation: {
      escapeValue: false,
    },
  });

export default i18next;
```
