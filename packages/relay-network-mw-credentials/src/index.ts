import { RelayNetworkMiddleware } from '@os-team/relay-network-creator';

const credentials: RelayNetworkMiddleware = (next) => (req) => {
  req.fetchOptions.credentials = 'include';
  return next(req);
};

export default credentials;
