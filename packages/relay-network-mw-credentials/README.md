# @os-team/relay-network-mw-credentials [![NPM version](https://img.shields.io/npm/v/@os-team/relay-network-mw-credentials)](https://yarnpkg.com/package/@os-team/relay-network-mw-credentials) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/relay-network-mw-credentials)](https://bundlephobia.com/result?p=@os-team/relay-network-mw-credentials)

The middleware for @os-team/relay-network-creator to include credentials in each request.

## Usage

### Step 1. Install the package

Install the package using the following command:

```
yarn add @os-team/relay-network-mw-credentials
```

### Step 2. Add the middleware

To add this middleware, pass it as the second parameter in the `createRelayNetwork` function:

```ts
import { createRelayNetwork } from '@os-team/relay-network-creator';
import credentials from '@os-team/relay-network-mw-credentials';

createRelayNetwork('https://api.domain.com', [credentials]);
```
