// Utils
export { default as TransformArgs } from './utils/TransformArgs';
export * from './utils/TransformArgs';

// Transformers
export { default as removeDuplicateSpaces } from './transformers/removeDuplicateSpaces';
export { default as toLowerCase } from './transformers/toLowerCase';
export { default as toUpperCase } from './transformers/toUpperCase';
export { default as trim } from './transformers/trim';
