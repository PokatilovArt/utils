import { Transformer } from '../utils/TransformArgs';

const toUpperCase: Transformer = (value) =>
  typeof value === 'string' ? value.toUpperCase() : value;

export default toUpperCase;
