import { Transformer } from '../utils/TransformArgs';

const removeDuplicateSpaces: Transformer = (value) =>
  typeof value === 'string' ? value.replace(/\s{2,}/g, ' ') : value;

export default removeDuplicateSpaces;
