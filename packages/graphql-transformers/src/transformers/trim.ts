import { Transformer } from '../utils/TransformArgs';

const trim: Transformer = (value) =>
  typeof value === 'string' ? value.trim() : value;

export default trim;
