import { Transformer } from '../utils/TransformArgs';

const toLowerCase: Transformer = (value) =>
  typeof value === 'string' ? value.toLowerCase() : value;

export default toLowerCase;
