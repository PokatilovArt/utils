import { createMethodDecorator } from 'type-graphql';

/* eslint-disable @typescript-eslint/no-explicit-any,no-param-reassign */

export type Transformer<T = any> = (value: T) => T;

export interface TransformArgsOptions {
  arg?: string;
}

const TransformArgs = (
  transformersMap: Record<string, Transformer[]>,
  options: TransformArgsOptions = {}
): MethodDecorator => {
  const { arg } = options;

  return createMethodDecorator<any>(({ args }, next) => {
    const input = arg ? args[arg] : args;

    // Check if the input is an object
    if (typeof input !== 'object') {
      throw new Error(
        `The ${arg ? `${arg} argument` : 'args'} must be an object`
      );
    }

    const transformedInput = Object.entries(transformersMap).reduce(
      (acc, [key, transformers]) => ({
        ...acc,
        [key]: transformers.reduce(
          (res, transformer) => transformer(res),
          input[key]
        ),
      }),
      input
    );

    // Replace the arguments
    if (arg) args[arg] = transformedInput;
    else args = transformedInput;

    return next();
  });
};

export default TransformArgs;
