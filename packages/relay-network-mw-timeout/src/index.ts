import { RelayNetworkMiddleware } from '@os-team/relay-network-creator';

const timeout =
  (ms: number): RelayNetworkMiddleware =>
  (next) =>
  async (req) => {
    const abortController = new AbortController();
    req.fetchOptions.signal = abortController.signal;

    const timeoutId = setTimeout(() => {
      abortController.abort();
    }, ms);

    try {
      return await next(req);
    } finally {
      clearTimeout(timeoutId);
    }
  };

export default timeout;
