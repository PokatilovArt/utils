# @os-team/relay-network-mw-timeout [![NPM version](https://img.shields.io/npm/v/@os-team/relay-network-mw-timeout)](https://yarnpkg.com/package/@os-team/relay-network-mw-timeout) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/relay-network-mw-timeout)](https://bundlephobia.com/result?p=@os-team/relay-network-mw-timeout)

The middleware for @os-team/relay-network-creator to abort a request if it takes more than N ms.

## Usage

### Step 1. Install the package

Install the package using the following command:

```
yarn add @os-team/relay-network-mw-timeout
```

### Step 2. Add the middleware

To add this middleware, pass it as the second parameter in the `createRelayNetwork` function:

```ts
import { createRelayNetwork } from '@os-team/relay-network-creator';
import timeout from '@os-team/relay-network-mw-timeout';

createRelayNetwork('https://api.domain.com', [timeout(30000)]);
```
