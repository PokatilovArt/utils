# @os-team/app-ua-parser [![NPM version](https://img.shields.io/npm/v/@os-team/app-ua-parser)](https://yarnpkg.com/package/@os-team/app-ua-parser) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/app-ua-parser)](https://bundlephobia.com/result?p=@os-team/app-ua-parser)

Detects the mobile app, OS, and device information from the user agent created by the @os-team/relay-network-mw-app-user-agent library.

## Usage

Install the package using the following command:

```
yarn add @os-team/app-ua-parser
```

Create a new AppUAParser instance with the user agent:

```ts
import AppUAParser from '@os-team/app-ua-parser';

const appUAParser = new AppUAParser(
  "App com.myproject/1.0.1 (Handset; iOS/12.5.4; 16H50; Apple; Apple; iPhone 6; iPhone7,2; Becca's iPhone; 12ABCDE3-45FG-678H-I90J-K1L2MN34O567)"
);
```

### Getting the app info

```ts
const app = appUAParser.getApp();
// {
//    bundleId: 'com.myproject',
//    version: '1.0',
//    buildNumber: 1,
// }
```

### Getting the OS info

```ts
const os = appUAParser.getOS();
// {
//    name: 'iOS',
//    version: '12.5.4',
//    buildId: '16H50',
// }
```

### Getting the device info

```ts
const device = appUAParser.getDevice();
// {
//    type: 'Handset',
//    manufacturer: 'Apple',
//    brand: 'Apple',
//    model: 'iPhone 6',
//    id: 'iPhone7,2',
//    name: "Becca's iPhone",
//    uniqueId: '12ABCDE3-45FG-678H-I90J-K1L2MN34O567',
// }
```

### Checking if the user agent is valid

```ts
import AppUAParser from '@os-team/app-ua-parser';

const isValid = AppUAParser.isValid('okhttp/3.12.12'); // false
```
