import AppUAParser from './index';

test('Invalid user agent', () => {
  const userAgent = 'okhttp/3.12.12';
  const appUAParser = new AppUAParser(userAgent);
  const app = appUAParser.getApp();
  const os = appUAParser.getOS();
  const device = appUAParser.getDevice();
  expect(AppUAParser.isValid(userAgent)).toBeFalsy();
  expect(app).toStrictEqual({
    bundleId: '',
    version: '',
    buildNumber: 0,
  });
  expect(os).toStrictEqual({
    name: '',
    version: '',
    buildId: '',
  });
  expect(device).toStrictEqual({
    type: '',
    manufacturer: '',
    brand: '',
    model: '',
    id: '',
    name: '',
    uniqueId: '',
  });
});

test('iOS user agent', () => {
  const userAgent =
    "App com.myproject/1.0.1 (Handset; iOS/12.5.4; 16H50; Apple; Apple; iPhone 6; iPhone7,2; Becca's iPhone; 12ABCDE3-45FG-678H-I90J-K1L2MN34O567)";
  const appUAParser = new AppUAParser(userAgent);
  const app = appUAParser.getApp();
  const os = appUAParser.getOS();
  const device = appUAParser.getDevice();
  expect(AppUAParser.isValid(userAgent)).toBeTruthy();
  expect(app).toStrictEqual({
    bundleId: 'com.myproject',
    version: '1.0',
    buildNumber: 1,
  });
  expect(os).toStrictEqual({
    name: 'iOS',
    version: '12.5.4',
    buildId: '16H50',
  });
  expect(device).toStrictEqual({
    type: 'Handset',
    manufacturer: 'Apple',
    brand: 'Apple',
    model: 'iPhone 6',
    id: 'iPhone7,2',
    name: "Becca's iPhone",
    uniqueId: '12ABCDE3-45FG-678H-I90J-K1L2MN34O567',
  });
});

test('Android user agent', () => {
  const userAgent =
    'App com.myproject/1.0.1 (Handset; Android/10; QP1A.190711.020; Xiaomi; Redmi; M2006C3MNG; angelican; Android Bluedroid; 12a345b678c90d12)';
  const appUAParser = new AppUAParser(userAgent);
  const app = appUAParser.getApp();
  const os = appUAParser.getOS();
  const device = appUAParser.getDevice();
  expect(AppUAParser.isValid(userAgent)).toBeTruthy();
  expect(app).toStrictEqual({
    bundleId: 'com.myproject',
    version: '1.0',
    buildNumber: 1,
  });
  expect(os).toStrictEqual({
    name: 'Android',
    version: '10',
    buildId: 'QP1A.190711.020',
  });
  expect(device).toStrictEqual({
    type: 'Handset',
    manufacturer: 'Xiaomi',
    brand: 'Redmi',
    model: 'M2006C3MNG',
    id: 'angelican',
    name: 'Android Bluedroid',
    uniqueId: '12a345b678c90d12',
  });
});
