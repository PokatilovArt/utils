const bundleIdRe = '[a-zA-Z0-9_-]+(?:.[a-zA-Z0-9_-]+)+';
const versionRe = '[0-9]+(?:.[0-9]+)*';
const buildNumberRe = '[0-9]+';
const deviceTypeRe = 'Handset|Tablet|Tv|Desktop|unknown';
const systemNameRe = '[a-zA-Z]+(?:s[a-zA-Z]+)*';
const systemVersionRe = versionRe;
const buildIdRe = '[A-Z0-9]+(?:.[A-Z0-9]+)*';
const manufacturerRe = '.*';
const brandRe = '.*';
const modelRe = '.*';
const deviceIdRe = '.*';
const deviceNameRe = '.*';
const uniqueIdRe = '[a-zA-Z0-9]+(?:-[a-zA-Z0-9]+)*';

const appInfoRe = `(${bundleIdRe})\\/(${versionRe})\\.(${buildNumberRe})`;
const deviceInfoReList = [
  `(${deviceTypeRe})`,
  `(${systemNameRe})\\/(${systemVersionRe})`,
  `(${buildIdRe})`,
  `(${manufacturerRe})`,
  `(${brandRe})`,
  `(${modelRe})`,
  `(${deviceIdRe})`,
  `(${deviceNameRe})`,
  `(${uniqueIdRe})`,
];

const uaRe = new RegExp(
  `^App\\s${appInfoRe}\\s\\(${deviceInfoReList.join(';\\s')}\\)$`
);

interface App {
  bundleId: string;
  version: string;
  buildNumber: number;
}

interface OS {
  name: string;
  version: string;
  buildId: string;
}

interface Device {
  type: string;
  manufacturer: string;
  brand: string;
  model: string;
  id: string;
  name: string;
  uniqueId: string;
}

class AppUAParser {
  private groups: string[];

  public constructor(userAgent: string) {
    this.groups = userAgent.match(uaRe) || [];
  }

  public static isValid(userAgent: string): boolean {
    return uaRe.test(userAgent);
  }

  public getApp(): App {
    return {
      bundleId: this.groups[1] || '',
      version: this.groups[2] || '',
      buildNumber: Number(this.groups[3]) || 0,
    };
  }

  public getOS(): OS {
    return {
      name: this.groups[5] || '',
      version: this.groups[6] || '',
      buildId: this.groups[7] || '',
    };
  }

  public getDevice(): Device {
    return {
      type: this.groups[4] || '',
      manufacturer: this.groups[8] || '',
      brand: this.groups[9] || '',
      model: this.groups[10] || '',
      id: this.groups[11] || '',
      name: this.groups[12] || '',
      uniqueId: this.groups[13] || '',
    };
  }
}

export default AppUAParser;
