# @os-team/sitemap [![NPM version](https://img.shields.io/npm/v/@os-team/sitemap)](https://yarnpkg.com/package/@os-team/sitemap) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/sitemap)](https://bundlephobia.com/result?p=@os-team/sitemap)

Creates the express middleware to generate the sitemap.xml file.

## Usage

### Step 1. Install the package

Install the package using the following command:

```
yarn add @os-team/sitemap
```

### Step 2. Add the sitemap middleware

The `sitemap` function creates the middleware for express.
The first parameter is an array of pages, the second is the domain name.

```ts
import express from 'express';
import sitemap from '@os-team/sitemap';

const app = express();
app.get('/sitemap.xml', sitemap(['/', '/contacts'], 'https://domain.com'));
app.listen(3000);
```

If you use `Next.js`:

```ts
import express from 'express';
import next from 'next';
import sitemap from '@os-team/sitemap';

const app = next({ dev: process.env.NODE_ENV !== 'production' });
const handle = app.getRequestHandler();

app.prepare().then(() => {
  const server = express();
  server.get('/sitemap.xml', sitemap(['/', '/contacts'], 'https://domain.com'));
  server.get('*', (req, res) => handle(req, res));
  server.listen(3000);
});
```
