export type Data = Record<string, string>;

export interface ErrorResponse {
  /**
   * Код ошибки.
   */
  ErrorCode: string;
  /**
   * Краткое описание ошибки.
   */
  Message?: string;
  /**
   * Подробное описание ошибки.
   */
  Details?: string;
}

export interface AgentData {
  /**
   * Признак агента.
   */
  AgentSign?:
    | 'bank_paying_agent' // банковский платежный агент
    | 'bank_paying_subagent' // банковский платежный субагент
    | 'paying_agent' // платежный агент
    | 'paying_subagent' // платежный субагент
    | 'attorney' // поверенный
    | 'commission_agent' // комиссионер
    | 'another'; // другой тип агента
  /**
   * Наименование операции.
   */
  OperationName?: string;
  /**
   * Телефоны платежного агента.
   */
  Phones?: string[];
  /**
   * Телефоны оператора по приему платежей.
   */
  ReceiverPhones?: string[];
  /**
   * Телефоны оператора перевода.
   */
  TransferPhones?: string[];
  /**
   * Наименование оператора перевода.
   */
  OperatorName?: string;
  /**
   * Адрес оператора перевода.
   */
  OperatorAddress?: string;
  /**
   * ИНН оператора перевода.
   */
  OperatorInn?: string;
}

export interface SupplierInfo {
  /**
   * Телефон поставщика.
   */
  Phones: string[];
  /**
   * Наименование поставщика.
   */
  Name: string;
  /**
   * ИНН поставщика.
   */
  Inn: string;
}

export interface Item {
  /**
   * Наименование товара.
   */
  Name: string;
  /**
   * Количество или вес товара.
   */
  Quantity: number;
  /**
   * Стоимость товара в копейках.
   */
  Amount: number;
  /**
   * Цена за единицу товара в копейках.
   */
  Price: number;
  /**
   * Признак способа расчета.
   */
  PaymentMethod?:
    | 'full_payment' // полный расчет (по умолчанию)
    | 'full_prepayment' // предоплата 100%
    | 'prepayment' // предоплата
    | 'advance' // аванс
    | 'partial_payment' // частичный расчет и кредит
    | 'credit' // передача в кредит
    | 'credit_payment'; // оплата кредита
  /**
   * Признак предмета расчета.
   */
  PaymentObject?:
    | 'commodity' // товар (по умолчанию)
    | 'excise' // подакцизный товар
    | 'job' // работа
    | 'service' // услуга
    | 'gambling_bet' // ставка азартной игры
    | 'gambling_prize' // выигрыш азартной игры
    | 'lottery' // лотерейный билет
    | 'lottery_prize' // выигрыш лотереи
    | 'intellectual_activity' // предоставление результатов интеллектуальной деятельности
    | 'payment' // платеж
    | 'agent_commission' // агентское вознаграждение
    | 'composite' // составной предмет расчета
    | 'another'; // иной предмет расчета
  /**
   * Ставка НДС.
   */
  Tax:
    | 'none' // без НДС
    | 'vat0' // 0%
    | 'vat10' // 10%
    | 'vat20' // 20%
    | 'vat110' // 10/110
    | 'vat120'; // 20/120
  /**
   * Маркировка товара.
   */
  Ean13?: string;
  /**
   * Код магазина.
   */
  ShopCode?: string;
  /**
   * Данные агента.
   */
  AgentData?: AgentData;
  /**
   * Данные поставщика платежного агента.
   */
  SupplierInfo?: SupplierInfo;
}

export interface Receipt {
  /**
   * Электронная почта покупателя.
   */
  Email?: string;
  /**
   * Телефон покупателя в формате +71234567890.
   */
  Phone?: string;
  /**
   * Электронная почта продавца.
   */
  EmailCompany?: string;
  /**
   * Система налогообложения.
   */
  Taxation:
    | 'osn' // общая
    | 'usn_income' // упрощенная (доходы)
    | 'usn_income_outcome' // упрощенная (доходы минус расходы)
    | 'patent' // патентная
    | 'envd' // единый налог на вмененный доход
    | 'esn'; // единый сельскохозяйственный налог
  /**
   * Массив позиций чека с информацией о товарах.
   */
  Items: Item[];
}

export type CardType =
  | 0 // списания
  | 1 // пополнения
  | 2; // списания и пополнения

export type CardStatus =
  | 'A' // активная
  | 'I' // неактивная
  | 'D'; // Удалена

export type Status =
  | 'NEW' // Создан
  | 'FORM_SHOWED' // Платежная форма открыта покупателем
  | 'DEADLINE_EXPIRED' // Просрочен
  | 'CANCELED' // Отменен
  | 'PREAUTHORIZING' // Проверка платежных данных
  | 'AUTHORIZING' // Резервируется
  | 'AUTHORIZED' // Зарезервирован
  | 'AUTH_FAIL' // Не прошел авторизацию
  | 'REJECTED' // Отклонен
  | '3DS_CHECKING' // Проверяется по протоколу 3-D Secure
  | '3DS_CHECKED' // Проверен по протоколу 3-D Secure
  | 'REVERSING' // Резервирование отменяется
  | 'PARTIAL_REVERSED' // Резервирование отменено частично
  | 'REVERSED' // Резервирование отменено
  | 'CONFIRMING' // Подтверждается
  | 'CONFIRMED' // Подтвержден
  | 'REFUNDING' // Возвращается
  | 'PARTIAL_REFUNDED' // Возвращен частично
  | 'REFUNDED'; // Возвращен полностью
