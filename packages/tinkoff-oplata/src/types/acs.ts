export interface ACSRequest {
  /**
   * Уникальный идентификатор транзакции в системе банка.
   */
  MD: string;
  /**
   * Результат аутентификации 3-D Secure.
   */
  PaReq: string;
  /**
   * Адрес перенаправления после аутентификации 3-D Secure.
   */
  TermUrl: string;
}

export interface ACSResponse {
  /**
   * Уникальный идентификатор транзакции в системе Банка.
   */
  MD: string;
  /**
   * Шифрованная строка, содержащая результаты 3-D Secure аутентификации.
   */
  PaRes: string;
}
