import { ErrorResponse } from './general';

export interface ResendResponse extends ErrorResponse {
  /**
   * Идентификатор терминала.
   */
  TerminalKey: string;
  /**
   * Количество сообщений, отправляемых повторно.
   */
  Count: number;
  /**
   * Успешность операции.
   */
  Success: boolean;
}
