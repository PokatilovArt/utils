import superagent from 'superagent';
import express, { Request, Response } from 'express';
import IORedis from 'ioredis';
import { Server } from 'http';
import AES from 'crypto-js/aes';
import session, { SessionOptions } from './index';
import SessionManager, {
  getSessionKey,
  getSessionListKey,
  SessionData,
} from './utils/SessionManager';

// Mock nanoid
jest.mock('nanoid');
const nanoidModule = require('nanoid'); // eslint-disable-line @typescript-eslint/no-var-requires,import/newline-after-import
const { nanoid } = nanoidModule;
const mockedNanoid = jest.fn((size = 21) => '0'.repeat(size));
(nanoid as jest.Mock).mockImplementation(mockedNanoid);

const PORT = 3001;
const URL = `http://localhost:${PORT}`;

const redis = new IORedis({
  port: 6379,
  host: 'localhost',
  db: 15,
});

const sessionTtl = 100;
const sessionManager = new SessionManager(redis, {
  ttl: sessionTtl,
});

let server: Server;

beforeEach(async () => {
  // Clear the database
  await redis.flushdb();
});

afterEach(async () => {
  // Close the server
  if (!server) return;
  await new Promise<void>((resolve, reject) => {
    server.close((err) => {
      if (err) reject(err);
      resolve();
    });
  });

  mockedNanoid.mockClear();
});

// Mock Date.now()
beforeAll(() => {
  jest.useFakeTimers('modern');
  jest.setSystemTime(new Date(1000000000000).getTime()); // 2001-09-09T01:46:40.000Z
});

// Disconnect from Redis
afterAll(async () => {
  await redis.flushdb();
  redis.disconnect();
});

/**
 * Starts the server with the session middleware.
 * Each test creates a server with its own custom session middleware options.
 */
const startServer = async (
  sessionOptions: Omit<SessionOptions, 'redis'>,
  middleware: (req: Request, res: Response) => void | Promise<void>
) => {
  const app = express();
  app.use(session({ redis, ...sessionOptions }));
  app.all('*', async (req, res) => {
    await middleware(req, res);
    res.send();
  });
  await new Promise<void>((resolve) => {
    server = app.listen(PORT, resolve);
  });
};

/* eslint-disable @typescript-eslint/no-explicit-any,jest/no-conditional-expect */

describe('Basic features', () => {
  it('Should populate the session management methods', async () => {
    // Test the middleware
    expect.assertions(7);
    await startServer({}, (req: any) => {
      expect(req.session).toBeDefined();
      expect(typeof req.session.create).toBe('function');
      expect(typeof req.session.update).toBe('function');
      expect(typeof req.session.regenerateId).toBe('function');
      expect(typeof req.session.destroy).toBe('function');
      expect(typeof req.session.destroyAll).toBe('function');
      expect(typeof req.session.list).toBe('function');
    });

    // Make the request
    await superagent.get(URL);
  });

  it('Should not find the session because the session ID was not passed', async () => {
    // Test the middleware
    expect.assertions(3);
    await startServer({}, (req: any) => {
      expect(req.session.id).toBeUndefined();
      expect(req.session.data).toBeUndefined();
      expect(req.session.expiresIn).toBeUndefined();
    });

    // Make the request
    await superagent.get(URL);
  });

  it('Should not find the session because an incorrect session ID was passed', async () => {
    // Seed the session to Redis
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create('sessionId', sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
    };

    // Test the middleware
    expect.assertions(3);
    await startServer(sessionOptions, (req: any) => {
      expect(req.session.id).toBeUndefined();
      expect(req.session.data).toBeUndefined();
      expect(req.session.expiresIn).toBeUndefined();
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=incorrect`);
  });

  it('Should find the existing session using a cookie', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(3);
    await startServer(sessionOptions, (req: any) => {
      expect(req.session.id).toBe(sessionId);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should find the existing session using the authorization header', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(3);
    await startServer(sessionOptions, (req: any) => {
      expect(req.session.id).toBe(sessionId);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);
    });

    // Make the request
    await superagent.get(URL).set('Authorization', `Bearer ${sessionId}`);
  });
});

describe('Idle timeout', () => {
  it('Should not delete the session because the idle timeout is 0 (disabled)', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(4);
    await startServer(sessionOptions, async (req: any) => {
      // Check if the session exists
      expect(req.session.id).toBe(sessionId);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Check if the session was not deleted from Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      expect(redisSessionData).not.toBeNull();
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should not delete the session because the idle timeout has not expired yet', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: Date.now(),
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 1000,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(4);
    await startServer(sessionOptions, async (req: any) => {
      // Check if the session exists
      expect(req.session.id).toBe(sessionId);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Check if the session was not deleted from Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      expect(redisSessionData).not.toBeNull();
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should delete the session because the idle timeout has expired', async () => {
    const idleTimeout = 1000;

    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: Date.now() - idleTimeout * 1000,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(9);
    await startServer(sessionOptions, async (req: any, res: Response) => {
      // Check if the session does not exist
      expect(req.session.id).toBeUndefined();
      expect(req.session.data).toBeUndefined();
      expect(req.session.expiresIn).toBeUndefined();

      // Check if the session was deleted from Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      expect(redisSessionData).toBeNull();
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      expect(redisSessionList).toHaveLength(0);

      // Check if the Set-Cookie header was passed in the response
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      expect(
        setCookieHeader.startsWith(`${sessionOptions.cookieName}=;`)
      ).toBeTruthy();
      expect(
        setCookieHeader.includes('Expires=Thu, 01 Jan 1970 00:00:00 GMT')
      ).toBeTruthy();

      // Check if the Cache-Control header was passed in the response
      expect(res.getHeader('Cache-Control')).toBe('no-store');
      expect(res.getHeader('Pragma')).toBe('no-cache');
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });
});

describe('Renewal timeout', () => {
  it('Should not regenerate the session ID because the renewal timeout is 0 (disabled)', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(8);
    await startServer(sessionOptions, async (req: any) => {
      const updatedSessionData = {
        ...sessionData,
        lastSeenAt: Date.now(),
      };

      // Check if the ID was not generated
      expect(mockedNanoid).toHaveBeenCalledTimes(0);

      // Check if the session ID was not updated
      expect(req.session.id).toBe(sessionId);
      expect(req.session.data).toStrictEqual(updatedSessionData);
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Check if the session ID was not updated in Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      expect(redisSessionData).not.toBeNull();
      expect(JSON.parse(redisSessionData as string)).toStrictEqual(
        updatedSessionData
      );
      const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
      expect(redisSessionTtl).toBe(sessionTtl);
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      expect(redisSessionList).toStrictEqual([sessionId]);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should not regenerate the session ID because the renewal timeout has not expired yet', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: Date.now(),
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 1000,
    };

    // Test the middleware
    expect.assertions(8);
    await startServer(sessionOptions, async (req: any) => {
      const updatedSessionData = {
        ...sessionData,
        lastSeenAt: Date.now(),
      };

      // Check if the ID was not generated
      expect(mockedNanoid).toHaveBeenCalledTimes(0);

      // Check if the session ID was not updated
      expect(req.session.id).toBe(sessionId);
      expect(req.session.data).toStrictEqual(updatedSessionData);
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Check if the session ID was not updated in Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      expect(redisSessionData).not.toBeNull();
      expect(JSON.parse(redisSessionData as string)).toStrictEqual(
        updatedSessionData
      );
      const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
      expect(redisSessionTtl).toBe(sessionTtl);
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      expect(redisSessionList).toStrictEqual([sessionId]);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should regenerate the session ID because the renewal timeout has expired', async () => {
    const renewalTimeout = 1000;
    const deletionTimeout = 5;

    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: Date.now() - renewalTimeout * 1000,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      tokenHeaderName: 'tokenHeaderName',
      tokenExpirationHeaderName: 'tokenExpirationHeaderName',
      length: sessionId.length - 1,
      maxLengthExistingIds: sessionId.length,
      absoluteTimeout: sessionTtl * 10, // To check req.session.expiresIn
      idleTimeout: 0,
      renewalTimeout,
      deletionTimeout,
    };

    // Test the middleware
    expect.assertions(22);
    const newSessionId = '0'.repeat(sessionOptions.length);
    const updatedSessionData = {
      ...sessionData,
      regeneratedAt: Date.now(),
      lastSeenAt: Date.now(),
    };
    let isFirstRequest = true;
    await startServer(sessionOptions, async (req: any, res: Response) => {
      if (isFirstRequest) {
        // Check if the ID was generated
        expect(mockedNanoid).toHaveBeenCalledTimes(1);

        // Check if the session ID was updated
        expect(req.session.id).toBe(newSessionId);
        expect(req.session.data).toStrictEqual(updatedSessionData);
        expect(req.session.expiresIn).toBe(sessionTtl);

        // Check if the old session exists and marked as not allowed for regeneration ID
        const redisSessionData = await redis.get(getSessionKey(sessionId));
        expect(redisSessionData).not.toBeNull();
        expect(JSON.parse(redisSessionData as string)).toStrictEqual({
          ...sessionData,
          lastSeenAt: Date.now(),
          renewalNotAllowed: true,
        });

        // Check if the TTL of the old session was updated
        const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
        expect(redisSessionTtl).toBe(deletionTimeout);

        // Check if a new session exists
        const redisNewSessionData = await redis.get(
          getSessionKey(newSessionId)
        );
        expect(redisNewSessionData).not.toBeNull();
        expect(JSON.parse(redisNewSessionData as string)).toStrictEqual(
          updatedSessionData
        );

        // Check if the TTL of the new session is correct
        const redisNewSessionTtl = await redis.ttl(getSessionKey(newSessionId));
        expect(redisNewSessionTtl).toBe(sessionTtl);

        // Check the session list
        const redisSessionList = await redis.lrange(
          getSessionListKey(sessionData.userId),
          0,
          -1
        );
        expect(redisSessionList).toStrictEqual([newSessionId]);

        // Check if the Set-Cookie header was passed in the response
        const expiresAt = new Date(Date.now() + sessionTtl * 1000);
        const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
        expect(
          setCookieHeader.startsWith(
            `${sessionOptions.cookieName}=${newSessionId};`
          )
        ).toBeTruthy();
        expect(
          setCookieHeader.includes(`Expires=${expiresAt.toUTCString()}`)
        ).toBeTruthy();

        // Check if the token header was passed in the response
        expect(res.getHeader(sessionOptions.tokenHeaderName)).toBe(
          newSessionId
        );
        expect(res.getHeader(sessionOptions.tokenExpirationHeaderName)).toBe(
          expiresAt.toUTCString()
        );

        // Check if the Cache-Control header was passed in the response
        expect(res.getHeader('Cache-Control')).toBe('no-store');
        expect(res.getHeader('Pragma')).toBe('no-cache');

        isFirstRequest = false;
        mockedNanoid.mockClear();
      } else {
        // Check if the ID was not generated
        expect(mockedNanoid).toHaveBeenCalledTimes(0);

        // Check if the session ID was not updated
        expect(req.session.id).toBe(sessionId);
        expect(req.session.data).toStrictEqual({
          ...sessionData,
          lastSeenAt: Date.now(),
          renewalNotAllowed: true,
        });
        expect(req.session.expiresIn).toBe(deletionTimeout);

        // Check the session list
        const redisSessionList = await redis.lrange(
          getSessionListKey(sessionData.userId),
          0,
          -1
        );
        expect(redisSessionList).toStrictEqual([newSessionId]);
      }
    });

    // Make the request for the first time (should update the session ID)
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);

    // Make the request for the second time (should NOT update the session ID anymore)
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });
});

describe('create', () => {
  it('Should create a new session', async () => {
    const absoluteTimeout = 1000;

    const sessionOptions = {
      cookieName: 'cookieName',
      tokenHeaderName: 'tokenHeaderName',
      tokenExpirationHeaderName: 'tokenExpirationHeaderName',
      length: 5,
      absoluteTimeout,
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(18);
    const newSessionId = '0'.repeat(sessionOptions.length);
    await startServer(sessionOptions, async (req: any, res: Response) => {
      // Check if the ID was not generated
      expect(mockedNanoid).toHaveBeenCalledTimes(0);

      // Check if the session does not exist
      expect(req.session.id).toBeUndefined();
      expect(req.session.data).toBeUndefined();
      expect(req.session.expiresIn).toBeUndefined();

      // Create a new session
      const newSessionDataInput = {
        userId: '1',
        ip: req.ip,
      };
      await req.session.create(newSessionDataInput);

      // Check if the ID was generated
      expect(mockedNanoid).toHaveBeenCalledTimes(1);

      // Check if the new session was created
      const newSessionData = {
        ...newSessionDataInput,
        createdAt: Date.now(),
        regeneratedAt: Date.now(),
        lastSeenAt: Date.now(),
      };
      expect(req.session.id).toBe(newSessionId);
      expect(req.session.data).toStrictEqual(newSessionData);
      expect(req.session.expiresIn).toBe(absoluteTimeout);

      // Check if the new session was created in Redis
      const redisNewSessionData = await redis.get(getSessionKey(newSessionId));
      expect(redisNewSessionData).not.toBeNull();
      expect(JSON.parse(redisNewSessionData as string)).toStrictEqual(
        newSessionData
      );

      // Check if the TTL of the new session is correct
      const redisNewSessionTtl = await redis.ttl(getSessionKey(newSessionId));
      expect(redisNewSessionTtl).toBe(absoluteTimeout);

      // Check the session list in Redis
      const redisSessionList = await redis.lrange(
        getSessionListKey(newSessionData.userId),
        0,
        -1
      );
      expect(redisSessionList).toStrictEqual([newSessionId]);

      // Check if the Set-Cookie header was passed in the response
      const expiresAt = new Date(Date.now() + absoluteTimeout * 1000);
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      expect(
        setCookieHeader.startsWith(
          `${sessionOptions.cookieName}=${newSessionId};`
        )
      ).toBeTruthy();
      expect(
        setCookieHeader.includes(`Expires=${expiresAt.toUTCString()}`)
      ).toBeTruthy();

      // Check if the token header was passed in the response
      expect(res.getHeader(sessionOptions.tokenHeaderName)).toBe(newSessionId);
      expect(res.getHeader(sessionOptions.tokenExpirationHeaderName)).toBe(
        expiresAt.toUTCString()
      );

      // Check if the Cache-Control header was passed in the response
      expect(res.getHeader('Cache-Control')).toBe('no-store');
      expect(res.getHeader('Pragma')).toBe('no-cache');
    });

    // Make the request
    await superagent.get(URL);
  });

  it('Should create a new session (simulate the session fixation attack)', async () => {
    const absoluteTimeout = 1000;

    // Seed the session for an attacker to Redis
    const attackerSessionId = 'attackerSessionId';
    const attackerSessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(attackerSessionId, attackerSessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      length: attackerSessionId.length,
      absoluteTimeout,
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(13);
    const newSessionId = '0'.repeat(sessionOptions.length);
    await startServer(sessionOptions, async (req: any, res: Response) => {
      const updatedAttackerSessionData = {
        ...attackerSessionData,
        lastSeenAt: Date.now(),
      };

      // Check if the session exists
      expect(req.session.id).toBe(attackerSessionId);
      expect(req.session.data).toStrictEqual(updatedAttackerSessionData);
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Create a new session for the victim user
      const newSessionDataInput = {
        userId: '2',
        privateData: 'privateData',
      };
      await req.session.create(newSessionDataInput);

      // Check if the new session was created
      const newSessionData = {
        ...newSessionDataInput,
        createdAt: Date.now(),
        regeneratedAt: Date.now(),
        lastSeenAt: Date.now(),
      };
      expect(req.session.id).toBe(newSessionId);
      expect(req.session.data).toStrictEqual(newSessionData);
      expect(req.session.expiresIn).toBe(absoluteTimeout);

      // Check if the attacker session exists in Redis
      const redisAttackerSessionData = await redis.get(
        getSessionKey(attackerSessionId)
      );
      expect(redisAttackerSessionData).not.toBeNull();
      expect(JSON.parse(redisAttackerSessionData as string)).toStrictEqual(
        updatedAttackerSessionData
      );

      // Check if the new session was created in Redis
      const redisNewSessionData = await redis.get(getSessionKey(newSessionId));
      expect(redisNewSessionData).not.toBeNull();
      expect(JSON.parse(redisNewSessionData as string)).toStrictEqual(
        newSessionData
      );

      // Check the session list of the attacker in Redis
      const redisAttackerSessionList = await redis.lrange(
        getSessionListKey(attackerSessionData.userId),
        0,
        -1
      );
      expect(redisAttackerSessionList).toStrictEqual([attackerSessionId]);

      // Check the session list of the victim user in Redis
      const redisNewSessionList = await redis.lrange(
        getSessionListKey(newSessionData.userId),
        0,
        -1
      );
      expect(redisNewSessionList).toStrictEqual([newSessionId]);

      // Check if the Set-Cookie header was passed in the response
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      expect(
        setCookieHeader.startsWith(
          `${sessionOptions.cookieName}=${newSessionId};`
        )
      ).toBeTruthy();
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${attackerSessionId}`);
  });

  it('Should send a cookie with the specified options', async () => {
    const sessionOptions = {
      cookieOptions: {
        domain: 'domain.com',
        path: '/path',
        secure: true,
        httpOnly: true,
        sameSite: 'lax',
      },
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(5);
    await startServer(
      sessionOptions as SessionOptions,
      async (req: any, res: Response) => {
        // Create a new session
        await req.session.create({
          userId: '1',
          ip: req.ip,
        });

        // Check if the specified cookie options is used in the Set-Cookie header
        const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
        expect(
          setCookieHeader.includes(
            `Domain=${sessionOptions.cookieOptions.domain}`
          )
        ).toBeTruthy();
        expect(
          setCookieHeader.includes(`Path=${sessionOptions.cookieOptions.path}`)
        ).toBeTruthy();
        expect(setCookieHeader.includes('Secure')).toBeTruthy();
        expect(setCookieHeader.includes('HttpOnly')).toBeTruthy();
        expect(setCookieHeader.includes('SameSite=Lax')).toBeTruthy();
      }
    );

    // Make the request
    await superagent.get(URL);
  });

  it('Should remove the last session if the number of sessions is exceeded', async () => {
    // Seed sessions to Redis
    const sessionId1 = 'sessionId1';
    const sessionId2 = 'sessionId2';
    const sessionId3 = 'sessionId3';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId1, sessionData);
    await sessionManager.create(sessionId2, sessionData);
    await sessionManager.create(sessionId3, sessionData);

    const sessionOptions = {
      maxSessionCountPerUser: 2,
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(5);
    await startServer(sessionOptions, async (req: any) => {
      // Create a new session
      await req.session.create({
        userId: sessionData.userId,
      });

      // Check if the 2 most recent sessions exists in Redis
      const redisNewSessionData = await redis.get(
        getSessionKey(req.session.id)
      );
      expect(redisNewSessionData).not.toBeNull();
      const redisSessionData3 = await redis.get(getSessionKey(sessionId3));
      expect(redisSessionData3).not.toBeNull();

      // Check if the 2 oldest sessions were deleted from Redis
      const redisSessionData2 = await redis.get(getSessionKey(sessionId2));
      expect(redisSessionData2).toBeNull();
      const redisSessionData1 = await redis.get(getSessionKey(sessionId1));
      expect(redisSessionData1).toBeNull();

      // Check the session list in Redis
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      expect(redisSessionList).toStrictEqual([req.session.id, sessionId3]);
    });

    // Make the request
    await superagent.get(URL);
  });
});

describe('update', () => {
  it('Should not update anything because the session does not exist', async () => {
    // Test the middleware
    expect.assertions(4);
    await startServer({}, async (req: any) => {
      // Check if the session does not exist
      expect(req.session.id).toBeUndefined();
      expect(req.session.data).toBeUndefined();
      expect(req.session.expiresIn).toBeUndefined();

      // Try to update the session
      await req.session.update({
        ip: req.ip,
      });

      // Check if nothing happened in Redis
      const keys = await redis.keys('*');
      expect(keys).toHaveLength(0);
    });

    // Make the request
    await superagent.get(URL);
  });

  it('Should update the existing session', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
      custom: 'custom',
      unnecessary: 'unnecessary',
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      absoluteTimeout: 1000,
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(5);
    await startServer(sessionOptions, async (req: any) => {
      const updatedSessionData = {
        ...sessionData,
        lastSeenAt: Date.now(),
      };

      // Check the session data
      expect(req.session.data).toStrictEqual(updatedSessionData);

      // Update the existing session
      await req.session.update({
        userId: '2', // Should be ignored
        createdAt: 10, // Should be ignored
        regeneratedAt: 10, // Should be ignored
        lastSeenAt: 10, // Should be ignored
        custom: 'new-custom', // Should be updated
        unnecessary: undefined, // Should be deleted
        ip: req.ip, // Should be added
      });

      // Check if the session data was updated
      const newSessionData = {
        userId: '1',
        createdAt: 0,
        regeneratedAt: 0,
        lastSeenAt: Date.now(),
        custom: 'new-custom', // Should be updated
        ip: req.ip, // Should be added
      };
      expect(req.session.data).toStrictEqual(newSessionData);

      // Check if the session data was updated in Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      expect(redisSessionData).not.toBeNull();
      expect(JSON.parse(redisSessionData as string)).toStrictEqual(
        newSessionData
      );

      // Check if the TTL of the session is correct
      const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
      expect(redisSessionTtl).toBe(sessionTtl);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });
});

describe('regenerateId', () => {
  it('Should not regenerate ID because the session does not exist', async () => {
    // Test the middleware
    expect.assertions(4);
    await startServer({}, async (req: any) => {
      // Check if the session does not exist
      expect(req.session.id).toBeUndefined();
      expect(req.session.data).toBeUndefined();
      expect(req.session.expiresIn).toBeUndefined();

      // Try to regenerate the session ID
      await req.session.regenerateId();

      // Check if nothing happened in Redis
      const keys = await redis.keys('*');
      expect(keys).toHaveLength(0);
    });

    // Make the request
    await superagent.get(URL);
  });

  it('Should regenerate the session ID and delete the old session immediately', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      tokenHeaderName: 'tokenHeaderName',
      tokenExpirationHeaderName: 'tokenExpirationHeaderName',
      length: sessionId.length - 1,
      maxLengthExistingIds: sessionId.length,
      absoluteTimeout: sessionTtl * 10, // To check req.session.expiresIn
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(18);
    const newSessionId = '0'.repeat(sessionOptions.length);
    await startServer(sessionOptions, async (req: any, res: Response) => {
      const updatedSessionData = {
        ...sessionData,
        lastSeenAt: Date.now(),
      };

      // Check if the session exists
      expect(req.session.id).toBe(sessionId);
      expect(req.session.data).toStrictEqual(updatedSessionData);
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Regenerate the session ID
      await req.session.regenerateId();

      // Check if the ID was generated
      expect(mockedNanoid).toHaveBeenCalledTimes(1);

      const regeneratedSessionData = {
        ...updatedSessionData,
        regeneratedAt: Date.now(),
      };

      // Check if the session ID was updated
      expect(req.session.id).toBe(newSessionId);
      expect(req.session.data).toStrictEqual(regeneratedSessionData);
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Check if the old session was deleted from Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      expect(redisSessionData).toBeNull();

      // Check if a new session exists
      const redisNewSessionData = await redis.get(getSessionKey(newSessionId));
      expect(redisNewSessionData).not.toBeNull();
      expect(JSON.parse(redisNewSessionData as string)).toStrictEqual(
        regeneratedSessionData
      );

      // Check if the TTL of the new session is correct
      const redisNewSessionTtl = await redis.ttl(getSessionKey(newSessionId));
      expect(redisNewSessionTtl).toBe(sessionTtl);

      // Check the session list
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      expect(redisSessionList).toStrictEqual([newSessionId]);

      // Check if the Set-Cookie header was passed in the response
      const expiresAt = new Date(Date.now() + sessionTtl * 1000);
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      expect(
        setCookieHeader.startsWith(
          `${sessionOptions.cookieName}=${newSessionId};`
        )
      ).toBeTruthy();
      expect(
        setCookieHeader.includes(`Expires=${expiresAt.toUTCString()}`)
      ).toBeTruthy();

      // Check if the token header was passed in the response
      expect(res.getHeader(sessionOptions.tokenHeaderName)).toBe(newSessionId);
      expect(res.getHeader(sessionOptions.tokenExpirationHeaderName)).toBe(
        expiresAt.toUTCString()
      );

      // Check if the Cache-Control header was passed in the response
      expect(res.getHeader('Cache-Control')).toBe('no-store');
      expect(res.getHeader('Pragma')).toBe('no-cache');
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should regenerate the session ID and delete the old session after delay', async () => {
    const deletionTimeout = 5;

    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      tokenHeaderName: 'tokenHeaderName',
      tokenExpirationHeaderName: 'tokenExpirationHeaderName',
      length: sessionId.length - 1,
      maxLengthExistingIds: sessionId.length,
      absoluteTimeout: sessionTtl * 10, // To check req.session.expiresIn
      idleTimeout: 0,
      renewalTimeout: 0,
      deletionTimeout,
    };

    // Test the middleware
    expect.assertions(20);
    const newSessionId = '0'.repeat(sessionOptions.length);
    await startServer(sessionOptions, async (req: any, res: Response) => {
      const updatedSessionData = {
        ...sessionData,
        lastSeenAt: Date.now(),
      };

      // Check if the session exists
      expect(req.session.id).toBe(sessionId);
      expect(req.session.data).toStrictEqual(updatedSessionData);
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Regenerate the session ID
      await req.session.regenerateId(true);

      // Check if the ID was generated
      expect(mockedNanoid).toHaveBeenCalledTimes(1);

      const regeneratedSessionData = {
        ...updatedSessionData,
        regeneratedAt: Date.now(),
      };

      // Check if the session ID was updated
      expect(req.session.id).toBe(newSessionId);
      expect(req.session.data).toStrictEqual(regeneratedSessionData);
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Check if the old session exists and marked as not allowed for regeneration ID
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      expect(redisSessionData).not.toBeNull();
      expect(JSON.parse(redisSessionData as string)).toStrictEqual({
        ...updatedSessionData,
        renewalNotAllowed: true,
      });

      // Check if the TTL of the old session was updated
      const redisSessionTtl = await redis.ttl(getSessionKey(sessionId));
      expect(redisSessionTtl).toBe(deletionTimeout);

      // Check if a new session exists
      const redisNewSessionData = await redis.get(getSessionKey(newSessionId));
      expect(redisNewSessionData).not.toBeNull();
      expect(JSON.parse(redisNewSessionData as string)).toStrictEqual(
        regeneratedSessionData
      );

      // Check if the TTL of the new session is correct
      const redisNewSessionTtl = await redis.ttl(getSessionKey(newSessionId));
      expect(redisNewSessionTtl).toBe(sessionTtl);

      // Check the session list
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      expect(redisSessionList).toStrictEqual([newSessionId]);

      // Check if the Set-Cookie header was passed in the response
      const expiresAt = new Date(Date.now() + sessionTtl * 1000);
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      expect(
        setCookieHeader.startsWith(
          `${sessionOptions.cookieName}=${newSessionId};`
        )
      ).toBeTruthy();
      expect(
        setCookieHeader.includes(`Expires=${expiresAt.toUTCString()}`)
      ).toBeTruthy();

      // Check if the token header was passed in the response
      expect(res.getHeader(sessionOptions.tokenHeaderName)).toBe(newSessionId);
      expect(res.getHeader(sessionOptions.tokenExpirationHeaderName)).toBe(
        expiresAt.toUTCString()
      );

      // Check if the Cache-Control header was passed in the response
      expect(res.getHeader('Cache-Control')).toBe('no-store');
      expect(res.getHeader('Pragma')).toBe('no-cache');
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });
});

describe('destroy', () => {
  it('Should not delete anything because the session does not exist', async () => {
    // Test the middleware
    expect.assertions(4);
    await startServer({}, async (req: any) => {
      // Check if the session does not exist
      expect(req.session.id).toBeUndefined();
      expect(req.session.data).toBeUndefined();
      expect(req.session.expiresIn).toBeUndefined();

      // Try to delete the session
      await req.session.destroy();

      // Check if nothing happened in Redis
      const keys = await redis.keys('*');
      expect(keys).toHaveLength(0);
    });

    // Make the request
    await superagent.get(URL);
  });

  it('Should delete the current session', async () => {
    // Seed sessions to Redis
    const sessionId1 = 'sessionId1';
    const sessionId2 = 'sessionId2';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId1, sessionData);
    await sessionManager.create(sessionId2, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(13);
    await startServer(sessionOptions, async (req: any, res: Response) => {
      // Check if the current session exists
      expect(req.session.id).toBe(sessionId2);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Delete the existing session
      await req.session.destroy();

      // Check if the current session was deleted
      expect(req.session.id).toBeUndefined();
      expect(req.session.data).toBeUndefined();
      expect(req.session.expiresIn).toBeUndefined();

      // Check if the current session was deleted from Redis
      const redisSessionData2 = await redis.get(getSessionKey(sessionId2));
      expect(redisSessionData2).toBeNull();

      // Check if the other session exists in Redis
      const redisSessionData1 = await redis.get(getSessionKey(sessionId1));
      expect(redisSessionData1).not.toBeNull();

      // Check the session list in Redis
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      expect(redisSessionList).toStrictEqual([sessionId1]);

      // Check if the Set-Cookie header was passed in the response
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      expect(
        setCookieHeader.startsWith(`${sessionOptions.cookieName}=;`)
      ).toBeTruthy();
      expect(
        setCookieHeader.includes('Expires=Thu, 01 Jan 1970 00:00:00 GMT')
      ).toBeTruthy();

      // Check if the Cache-Control header was passed in the response
      expect(res.getHeader('Cache-Control')).toBe('no-store');
      expect(res.getHeader('Pragma')).toBe('no-cache');
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId2}`);
  });

  it('Should delete another session of the current user', async () => {
    // Seed sessions to Redis
    const sessionId1 = 'sessionId1';
    const sessionId2 = 'sessionId2';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId1, sessionData);
    await sessionManager.create(sessionId2, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
      secret: 'secret',
    };

    // Test the middleware
    expect.assertions(8);
    await startServer(sessionOptions, async (req: any) => {
      // Check if the current session exists
      expect(req.session.id).toBe(sessionId2);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Get the ID
      const id = AES.encrypt(sessionId1, sessionOptions.secret).toString();

      // Delete the existing session
      await req.session.destroy(id);

      // Check if the current session exists
      expect(req.session.id).toBe(sessionId2);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Check if the other session was deleted from Redis
      const redisSessionData1 = await redis.get(getSessionKey(sessionId1));
      expect(redisSessionData1).toBeNull();

      // Check if the current session exists in Redis
      const redisSessionData2 = await redis.get(getSessionKey(sessionId2));
      expect(redisSessionData2).not.toBeNull();
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId2}`);
  });

  it('Should not delete another session of the another user', async () => {
    // Seed sessions to Redis
    const sessionId = 'sessionId';
    const sessionIdAnotherUser = 'sessionIdAnotherUser';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    const sessionDataAnotherUser: SessionData = {
      userId: '2',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);
    await sessionManager.create(sessionIdAnotherUser, sessionDataAnotherUser);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
      secret: 'secret',
    };

    // Test the middleware
    expect.assertions(8);
    await startServer(sessionOptions, async (req: any) => {
      // Check if the current session exists
      expect(req.session.id).toBe(sessionId);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Get the ID
      const id = AES.encrypt(
        sessionIdAnotherUser,
        sessionOptions.secret
      ).toString();

      // Delete the existing session
      await req.session.destroy(id);

      // Check if the current session exists
      expect(req.session.id).toBe(sessionId);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Check if sessions exists in Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      expect(redisSessionData).not.toBeNull();
      const redisSessionDataAnotherUser = await redis.get(
        getSessionKey(sessionIdAnotherUser)
      );
      expect(redisSessionDataAnotherUser).not.toBeNull();
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });

  it('Should do nothing because the ID is incorrect', async () => {
    // Seed the session to Redis
    const sessionId = 'sessionId';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId, sessionData);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
      secret: 'secret',
    };

    // Test the middleware
    expect.assertions(7);
    await startServer(sessionOptions, async (req: any) => {
      // Check if the current session exists
      expect(req.session.id).toBe(sessionId);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Delete the existing session
      await req.session.destroy('incorrect');

      // Check if the current session exists
      expect(req.session.id).toBe(sessionId);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Check if the session exists in Redis
      const redisSessionData = await redis.get(getSessionKey(sessionId));
      expect(redisSessionData).not.toBeNull();
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId}`);
  });
});

describe('destroyAll', () => {
  it('Should not delete anything because the session does not exist', async () => {
    // Test the middleware
    expect.assertions(4);
    await startServer({}, async (req: any) => {
      // Check if the session does not exist
      expect(req.session.id).toBeUndefined();
      expect(req.session.data).toBeUndefined();
      expect(req.session.expiresIn).toBeUndefined();

      // Try to delete all sessions
      await req.session.destroyAll();

      // Check if nothing happened in Redis
      const keys = await redis.keys('*');
      expect(keys).toHaveLength(0);
    });

    // Make the request
    await superagent.get(URL);
  });

  it("Should delete all user's sessions", async () => {
    // Seed sessions to Redis
    const sessionId1 = 'sessionId1';
    const sessionId2 = 'sessionId2';
    const sessionIdAnotherUser = 'sessionIdAnotherUser';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    const sessionDataAnotherUser: SessionData = {
      userId: '2',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId1, sessionData);
    await sessionManager.create(sessionId2, sessionData);
    await sessionManager.create(sessionIdAnotherUser, sessionDataAnotherUser);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(15);
    await startServer(sessionOptions, async (req: any, res: Response) => {
      // Check if the current session exists
      expect(req.session.id).toBe(sessionId2);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Delete all user's sessions
      await req.session.destroyAll();

      // Check if the current session was deleted
      expect(req.session.id).toBeUndefined();
      expect(req.session.data).toBeUndefined();
      expect(req.session.expiresIn).toBeUndefined();

      // Check if all user's sessions were deleted from Redis
      const redisSessionData1 = await redis.get(getSessionKey(sessionId1));
      expect(redisSessionData1).toBeNull();
      const redisSessionData2 = await redis.get(getSessionKey(sessionId2));
      expect(redisSessionData2).toBeNull();

      // Check the session list in Redis
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      expect(redisSessionList).toStrictEqual([]);

      // Check if the session associated with another user exists in Redis
      const redisSessionDataAnotherUser = await redis.get(
        getSessionKey(sessionIdAnotherUser)
      );
      expect(redisSessionDataAnotherUser).not.toBeNull();

      // Check the session list associated with another user in Redis
      const redisSessionListAnotherUser = await redis.lrange(
        getSessionListKey(sessionDataAnotherUser.userId),
        0,
        -1
      );
      expect(redisSessionListAnotherUser).toStrictEqual([sessionIdAnotherUser]);

      // Check if the Set-Cookie header was passed in the response
      const setCookieHeader = (res.getHeader('Set-Cookie') as string) || '';
      expect(
        setCookieHeader.startsWith(`${sessionOptions.cookieName}=;`)
      ).toBeTruthy();
      expect(
        setCookieHeader.includes('Expires=Thu, 01 Jan 1970 00:00:00 GMT')
      ).toBeTruthy();

      // Check if the Cache-Control header was passed in the response
      expect(res.getHeader('Cache-Control')).toBe('no-store');
      expect(res.getHeader('Pragma')).toBe('no-cache');
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId2}`);
  });

  it("Should delete all user's sessions except the current one", async () => {
    // Seed sessions to Redis
    const sessionId1 = 'sessionId1';
    const sessionId2 = 'sessionId2';
    const sessionIdAnotherUser = 'sessionIdAnotherUser';
    const sessionData: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    const sessionDataAnotherUser: SessionData = {
      userId: '2',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId1, sessionData);
    await sessionManager.create(sessionId2, sessionData);
    await sessionManager.create(sessionIdAnotherUser, sessionDataAnotherUser);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(11);
    await startServer(sessionOptions, async (req: any) => {
      // Check if the current session exists
      expect(req.session.id).toBe(sessionId2);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Delete all user's sessions except the current one
      await req.session.destroyAll(true);

      // Check if the current session exists
      expect(req.session.id).toBe(sessionId2);
      expect(req.session.data).toStrictEqual({
        ...sessionData,
        lastSeenAt: Date.now(),
      });
      expect(req.session.expiresIn).toBe(sessionTtl);

      // Check if all user's sessions were deleted from Redis except the current one
      const redisSessionData1 = await redis.get(getSessionKey(sessionId1));
      expect(redisSessionData1).toBeNull();
      const redisSessionData2 = await redis.get(getSessionKey(sessionId2));
      expect(redisSessionData2).not.toBeNull();

      // Check the session list in Redis
      const redisSessionList = await redis.lrange(
        getSessionListKey(sessionData.userId),
        0,
        -1
      );
      expect(redisSessionList).toStrictEqual([sessionId2]);

      // Check if the session associated with another user exists in Redis
      const redisSessionDataAnotherUser = await redis.get(
        getSessionKey(sessionIdAnotherUser)
      );
      expect(redisSessionDataAnotherUser).not.toBeNull();

      // Check the session list associated with another user in Redis
      const redisSessionListAnotherUser = await redis.lrange(
        getSessionListKey(sessionDataAnotherUser.userId),
        0,
        -1
      );
      expect(redisSessionListAnotherUser).toStrictEqual([sessionIdAnotherUser]);
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId2}`);
  });
});

describe('list', () => {
  it('Should return the empty array because the session does not exist', async () => {
    // Test the middleware
    expect.assertions(4);
    await startServer({}, async (req: any) => {
      // Check if the session does not exist
      expect(req.session.id).toBeUndefined();
      expect(req.session.data).toBeUndefined();
      expect(req.session.expiresIn).toBeUndefined();

      // Try to get a list of all user's sessions
      const list = await req.session.list();

      // Check if the list is empty
      expect(list).toHaveLength(0);
    });

    // Make the request
    await superagent.get(URL);
  });

  it("Should return all user's sessions", async () => {
    // Seed sessions to Redis
    const sessionId1 = 'sessionId1';
    const sessionId2 = 'sessionId2';
    const sessionIdAnotherUser = 'sessionIdAnotherUser';
    const sessionData1: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
      os: 'macos',
    };
    const sessionData2: SessionData = {
      userId: '1',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
      os: 'ios',
    };
    const sessionDataAnotherUser: SessionData = {
      userId: '2',
      createdAt: 0,
      regeneratedAt: 0,
      lastSeenAt: 0,
    };
    await sessionManager.create(sessionId1, sessionData1);
    await sessionManager.create(sessionId2, sessionData2);
    await sessionManager.create(sessionIdAnotherUser, sessionDataAnotherUser);

    const sessionOptions = {
      cookieName: 'cookieName',
      idleTimeout: 0,
      renewalTimeout: 0,
    };

    // Test the middleware
    expect.assertions(15);
    await startServer(sessionOptions, async (req: any) => {
      // Get a list of all user's sessions
      const list = await req.session.list();

      // Check the session list
      expect(list).toHaveLength(2);

      expect(list[0].userId).toBe(sessionData2.userId);
      expect(list[0].createdAt).toBe(sessionData2.createdAt);
      expect(list[0].regeneratedAt).toBe(sessionData2.regeneratedAt);
      expect(list[0].lastSeenAt).toBe(Date.now());
      expect(list[0].os).toBe(sessionData2.os);
      expect(typeof list[0].id).toBe('string');
      expect(list[0].current).toBeTruthy();

      expect(list[1].userId).toBe(sessionData1.userId);
      expect(list[1].createdAt).toBe(sessionData1.createdAt);
      expect(list[1].regeneratedAt).toBe(sessionData1.regeneratedAt);
      expect(list[1].lastSeenAt).toBe(sessionData1.lastSeenAt);
      expect(list[1].os).toBe(sessionData1.os);
      expect(typeof list[1].id).toBe('string');
      expect(list[1].current).toBeFalsy();
    });

    // Make the request
    await superagent
      .get(URL)
      .set('Cookie', `${sessionOptions.cookieName}=${sessionId2}`);
  });
});
