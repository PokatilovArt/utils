import { Request } from 'express';
import cookieExtractor from './cookieExtractor';

const cookieName = 'sid';
const sessionId = 'V1StGXR8_Z5jdHi6B-myT';
const maxLength = sessionId.length;

it('Should return null because there is no cookies', () => {
  const req = {
    headers: {},
  } as Request;

  const extractedSessionId = cookieExtractor(req, cookieName, maxLength);
  expect(extractedSessionId).toBeNull();
});

it('Should return null because the cookie name is incorrect', () => {
  const req = {
    headers: {
      cookie: `_${cookieName}=${sessionId}`,
    },
  } as Request;

  const extractedSessionId = cookieExtractor(req, cookieName, maxLength);
  expect(extractedSessionId).toBeNull();
});

it('Should return null because the session ID contains a colon (do not use in the Redis key)', () => {
  const req = {
    headers: {
      cookie: `${cookieName}=${sessionId.replace('_', ':')}`,
    },
  } as Request;

  const extractedSessionId = cookieExtractor(req, cookieName, maxLength);
  expect(extractedSessionId).toBeNull();
});

it('Should return null because the session ID is too long', () => {
  const req = {
    headers: {
      cookie: `${cookieName}=${sessionId}`,
    },
  } as Request;

  const extractedSessionId = cookieExtractor(req, cookieName, maxLength - 1);
  expect(extractedSessionId).toBeNull();
});

it('Should return the session ID', () => {
  const req = {
    headers: {
      cookie: `${cookieName}=${sessionId}`,
    },
  } as Request;

  const extractedSessionId = cookieExtractor(req, cookieName, maxLength);
  expect(extractedSessionId).toBe(sessionId);
});

it('Should return the session ID (multiple cookies)', () => {
  const req = {
    headers: {
      cookie: `_ga=GA1.2.1234567890.1234567890; ${cookieName}=${sessionId}; _gid=GA1.2.1234567890.1234567890`,
    },
  } as Request;

  const extractedSessionId = cookieExtractor(req, cookieName, maxLength);
  expect(extractedSessionId).toBe(sessionId);
});
