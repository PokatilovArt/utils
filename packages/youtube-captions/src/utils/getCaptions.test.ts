import getCaptions from './getCaptions';

it('Should return an empty array because the YouTube ID is incorrect', async () => {
  const captions = await getCaptions('incorrect', 'en');
  expect(captions).toHaveLength(0);
});

it('Should return an empty array because the video has no subtitles in the specified language', async () => {
  const captions = await getCaptions('R7p-nPg8t_g', 'ru');
  expect(captions).toHaveLength(0);
});

it('Should the subtitles for the video', async () => {
  const captions = await getCaptions('R7p-nPg8t_g', 'en');
  expect(captions).toHaveLength(24);
});
