import { execSync } from 'child_process';
import crypto from 'crypto';
import readline from 'readline';
import fs from 'fs';
import path from 'path';

interface Caption {
  start: number;
  end: number;
  text: string;
}

const timeToMs = (time: string) => {
  const t = time.match(/^(\d{2}):(\d{2}):(\d{2})\.(\d{3})$/);
  if (!t) return 0;
  const s = Number(t[1]) * 3600 + Number(t[2]) * 60 + Number(t[3]);
  return s * 1000 + Number(t[4]);
};

const getCaptions = async (
  youtubeId: string,
  language: string,
  dir?: string
): Promise<Caption[]> => {
  const hash = crypto.randomBytes(2).toString('hex');
  const name = `${youtubeId}-${hash}`;
  const filePath = path.resolve(dir || '', name);

  // Save the file with subtitles
  try {
    execSync(
      `youtube-dl --write-sub --sub-lang ${language} --convert-subs vtt -o ${filePath} --skip-download 'https://www.youtube.com/watch?v=${youtubeId}'`
    ).toString();

    const fullFilePath = `${filePath}.${language}.vtt`;

    const rl = readline.createInterface({
      input: fs.createReadStream(fullFilePath),
      crlfDelay: Infinity,
    });

    const captions: Caption[] = [];
    let cur: Caption | undefined;

    // eslint-disable-next-line no-restricted-syntax
    for await (const line of rl) {
      const trimmedLine = line.trim();
      const tm = trimmedLine.match(
        /^(\d{2}:\d{2}:\d{2}\.\d{3}) --> (\d{2}:\d{2}:\d{2}\.\d{3})$/
      );

      if (tm) {
        cur = {
          text: '',
          start: timeToMs(tm[1]),
          end: timeToMs(tm[2]),
        };
        // eslint-disable-next-line no-continue
        continue;
      }

      if (cur) {
        if (trimmedLine) {
          cur.text += `${cur.text} ${trimmedLine}`.trim();
        } else {
          captions.push(cur);
          cur = undefined;
        }
      }
    }

    // Delete the file with subtitles
    fs.unlinkSync(fullFilePath);

    return captions;
  } catch (e) {
    return [];
  }
};

export default getCaptions;
