import { execSync } from 'child_process';

const getLanguages = (youtubeId: string): string[] => {
  let stdout = '';

  try {
    stdout = execSync(
      `youtube-dl --list-subs 'https://www.youtube.com/watch?v=${youtubeId}'`
    ).toString();
  } catch (e) {
    return [];
  }

  const languages: string[] = [];
  let readingStarted = false;

  stdout.split('\n').forEach((line) => {
    if (line === `Available subtitles for ${youtubeId}:`) {
      readingStarted = true;
    }
    if (readingStarted) {
      const groups = line.match(
        /^([a-z]{2}(-[A-Z]{2})?)\s+[a-z0-9]+(,\s[a-z0-9]+)*$/
      );
      if (groups) {
        languages.push(groups[1]);
      }
    }
  });

  return languages;
};

export default getLanguages;
