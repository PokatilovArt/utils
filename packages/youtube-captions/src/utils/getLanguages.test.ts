import getLanguages from './getLanguages';

it('Should return an empty array because the YouTube ID is incorrect', () => {
  const languages = getLanguages('incorrect');
  expect(languages).toHaveLength(0);
});

it('Should return an empty array because the video has no subtitles', () => {
  const languages = getLanguages('U_85TaXbeIo');
  expect(languages).toHaveLength(0);
});

it('Should return the subtitle languages available for the video', () => {
  const languages = getLanguages('R7p-nPg8t_g');
  expect(languages).toStrictEqual(['en', 'de', 'pl', 'pt', 'es']);
});
