import { RelayNetworkMiddleware } from '@os-team/relay-network-creator';

const upload: RelayNetworkMiddleware = (next) => (req) => {
  const { uploadables } = req;
  if (!uploadables) return next(req);
  if (!req.variables.input)
    throw new Error('Pass all params to the input variable');

  const formData = new FormData();
  const map = {};

  Object.keys(uploadables).forEach((key, index) => {
    map[index.toString()] = [`variables.input.${key}`];
  });

  formData.append(
    'operations',
    JSON.stringify({
      query: req.request.text,
      variables: req.variables,
    })
  );
  formData.append('map', JSON.stringify(map));

  // Append files
  Object.values(uploadables).forEach((value, index) => {
    formData.append(index.toString(), value);
  });

  delete req.fetchOptions.headers['Content-Type'];
  req.fetchOptions.body = formData;

  return next(req);
};

export default upload;
