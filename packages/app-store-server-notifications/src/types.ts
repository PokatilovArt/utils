export interface BaseReceiptInfo {
  /**
   * The time Apple customer support canceled a transaction, or the time
   * an auto-renewable subscription plan was upgraded, in a date-time format
   * similar to the ISO 8601. This field is only present for refunded transactions.
   */
  cancellation_date?: string;
  /**
   * The time Apple customer support canceled a transaction, or the time
   * an auto-renewable subscription plan was upgraded, in UNIX epoch time format,
   * in milliseconds. This field is only present for refunded transactions.
   * Use this time format for processing dates.
   * See https://developer.apple.com/documentation/appstorereceipts/cancellation_date_ms
   */
  cancellation_date_ms?: string;
  /**
   * The time Apple customer support canceled a transaction, or the time
   * an auto-renewable subscription plan was upgraded, in the Pacific Time zone.
   * This field is only present for refunded transactions.
   */
  cancellation_date_pst?: string;
  /**
   * The reason for a refunded transaction. When a customer cancels a
   * transaction, the App Store gives them a refund and provides a value for
   * this key. A value of “1” indicates that the customer canceled their
   * transaction due to an actual or perceived issue within your app.
   * A value of “0” indicates that the transaction was canceled for another
   * reason; for example, if the customer made the purchase accidentally.
   */
  cancellation_reason?: '1' | '0';
  /**
   * The time a subscription expires or when it will renew, in a date-time
   * format similar to the ISO 8601.
   */
  expires_date: string;
  /**
   * The time a subscription expires or when it will renew, in UNIX epoch time
   * format, in milliseconds. Use this time format for processing dates.
   * See https://developer.apple.com/documentation/appstorereceipts/expires_date_ms
   */
  expires_date_ms: string;
  /**
   * The time a subscription expires or when it will renew, in the Pacific Time
   * zone.
   */
  expires_date_pst: string;
  /**
   * A value that indicates whether the user is the purchaser of the product,
   * or is a family member with access to the product through Family Sharing.
   * See https://developer.apple.com/documentation/appstorereceipts/in_app_ownership_type
   */
  in_app_ownership_type:
    | 'FAMILY_SHARED' // The transaction belongs to a family member who benefits from service
    | 'PURCHASED'; // The transaction belongs to the purchaser
  /**
   * An indicator of whether an auto-renewable subscription is in the
   * introductory price period.
   * See https://developer.apple.com/documentation/appstorereceipts/is_in_intro_offer_period
   */
  is_in_intro_offer_period:
    | 'true' // The customer’s subscription is in an introductory price period
    | 'false'; // The subscription is not in an introductory price period
  /**
   * An indicator of whether a subscription is in the free trial period.
   * See https://developer.apple.com/documentation/appstorereceipts/is_trial_period
   */
  is_trial_period:
    | 'true' // The subscription is in the free trial period
    | 'false'; // The subscription is not in the free trial period
  /**
   * The time of the original in-app purchase, in a date-time format similar to
   * ISO 8601.
   */
  original_purchase_date: string;
  /**
   * The time of the original in-app purchase, in UNIX epoch time format, in
   * milliseconds. For an auto-renewable subscription, this value indicates
   * the date of the subscription's initial purchase. The original purchase date
   * applies to all product types and remains the same in all transactions for
   * the same product ID. This value corresponds to the original transaction’s
   * transactionDate property in StoreKit. Use this time format for processing
   * dates.
   */
  original_purchase_date_ms: string;
  /**
   * The time of the original in-app purchase, in the Pacific Time zone.
   */
  original_purchase_date_pst: string;
  /**
   * The transaction identifier of the original purchase.
   * See https://developer.apple.com/documentation/appstorereceipts/original_transaction_id
   */
  original_transaction_id: string;
  /**
   * The unique identifier of the product purchased. You provide this value when
   * creating the product in App Store Connect, and it corresponds to
   * the productIdentifier property of the SKPayment object stored in
   * the transaction’s payment property.
   */
  product_id: string;
  /**
   * The identifier of the subscription offer redeemed by the user.
   * See https://developer.apple.com/documentation/appstorereceipts/promotional_offer_id
   */
  promotional_offer_id?: string;
  /**
   * The time the App Store charged the user’s account for a purchased or
   * restored product, or the time the App Store charged the user’s account
   * for a subscription purchase or renewal after a lapse, in a date-time
   * format similar to ISO 8601.
   */
  purchase_date: string;
  /**
   * For consumable, non-consumable, and non-renewing subscription products,
   * the time the App Store charged the user’s account for a purchased or
   * restored product, in the UNIX epoch time format, in milliseconds.
   * For auto-renewable subscriptions, the time the App Store charged
   * the user’s account for a subscription purchase or renewal after a lapse,
   * in the UNIX epoch time format, in milliseconds. Use this time format
   * for processing dates.
   */
  purchase_date_ms: string;
  /**
   * The time the App Store charged the user’s account for a purchased or
   * restored product, or the time the App Store charged the user’s account
   * for a subscription purchase or renewal after a lapse, in the Pacific Time
   * zone.
   */
  purchase_date_pst: string;
  /**
   * The number of consumable products purchased. This value corresponds to
   * the quantity property of the SKPayment object stored in the transaction’s
   * payment property. The value is usually “1” unless modified with a mutable
   * payment. The maximum value is 10.
   */
  quantity: string;
  /**
   * A unique identifier for a transaction such as a purchase, restore, or renewal.
   * See https://developer.apple.com/documentation/appstorereceipts/transaction_id
   */
  transaction_id: string;
  /**
   * A unique identifier for purchase events across devices, including
   * subscription-renewal events. This value is the primary key for identifying
   * subscription purchases.
   */
  web_order_line_item_id: string;
}

export type ExpirationIntent =
  | '1' // The customer voluntarily canceled their subscription
  | '2' // Billing error; for example, the customer's payment information was no longer valid
  | '3' // The customer did not agree to a recent price increase
  | '4' // The product was not available for purchase at the time of renewal
  | '5'; // Unknown error

export interface LatestReceiptInfo extends BaseReceiptInfo {
  /**
   * An indicator that a subscription has been canceled due to an upgrade.
   * This field is only present for upgrade transactions.
   */
  is_upgraded?: 'true';
  /**
   * The reference name of a subscription offer that you configured in App Store Connect.
   * This field is present when a customer redeemed a subscription offer code.
   * See https://developer.apple.com/documentation/appstorereceipts/offer_code_ref_name
   */
  offer_code_ref_name?: string;
  /**
   * The identifier of the subscription group to which the subscription belongs.
   * The value for this field is identical to the subscriptionGroupIdentifier
   * property in SKProduct.
   */
  subscription_group_identifier: string;
}

export interface PendingRenewalInfo {
  /**
   * The value for this key corresponds to the productIdentifier property of
   * the product that the customer’s subscription renews.
   */
  auto_renew_product_id: string;
  /**
   * The current renewal status for the auto-renewable subscription.
   * See https://developer.apple.com/documentation/appstorereceipts/auto_renew_status
   */
  auto_renew_status:
    | '1' // The subscription will renew at the end of the current subscription period
    | '0'; // The customer has turned off automatic renewal for the subscription
  /**
   * The reason a subscription expired. This field is only present for a receipt
   * that contains an expired auto-renewable subscription.
   * See https://developer.apple.com/documentation/appstorereceipts/expiration_intent
   */
  expiration_intent?: ExpirationIntent;
  /**
   * The time at which the grace period for subscription renewals expires,
   * in a date-time format similar to the ISO 8601.
   */
  grace_period_expires_date?: string;
  /**
   * The time at which the grace period for subscription renewals expires,
   * in UNIX epoch time format, in milliseconds. This key is only present for
   * apps that have Billing Grace Period enabled and when the user experiences
   * a billing error at the time of renewal. Use this time format for processing
   * dates.
   */
  grace_period_expires_date_ms?: string;
  /**
   * The time at which the grace period for subscription renewals expires, in
   * the Pacific Time zone.
   */
  grace_period_expires_date_pst?: string;
  /**
   * A flag that indicates Apple is attempting to renew an expired subscription
   * automatically. This field is only present if an auto-renewable subscription
   * is in the billing retry state.
   * See https://developer.apple.com/documentation/appstorereceipts/is_in_billing_retry_period
   */
  is_in_billing_retry_period?:
    | '1' // The App Store is attempting to renew the subscription
    | '0'; // The App Store has stopped attempting to renew the subscription
  /**
   * The reference name of a subscription offer that you configured in
   * App Store Connect. This field is present when a customer redeemed
   * a subscription offer code.
   * See https://developer.apple.com/documentation/appstorereceipts/offer_code_ref_name
   */
  offer_code_ref_name?: string;
  /**
   * The transaction identifier of the original purchase.
   * See https://developer.apple.com/documentation/appstorereceipts/original_transaction_id
   */
  original_transaction_id: string;
  /**
   * The price consent status for a subscription price increase. This field is
   * only present if the customer was notified of the price increase.
   * The default value is "0" and changes to "1" if the customer consents.
   */
  price_consent_status?: '1' | '0';
  /**
   * The unique identifier of the product purchased. You provide this value when
   * creating the product in App Store Connect, and it corresponds to
   * the productIdentifier property of the SKPayment object stored in
   * the transaction's payment property.
   */
  product_id: string;
  /**
   * The identifier of the promotional offer for an auto-renewable subscription
   * that the user redeemed. You provide this value in the Promotional Offer
   * Identifier field when you create the promotional offer in App Store Connect.
   * See https://developer.apple.com/documentation/appstorereceipts/promotional_offer_id
   */
  promotional_offer_id?: string;
}

export interface Receipt {
  /**
   * See app_item_id.
   */
  adam_id: number;
  /**
   * Generated by App Store Connect and used by the App Store to uniquely
   * identify the app purchased. Apps are assigned this identifier only in
   * production. Treat this value as a 64-bit long integer.
   */
  app_item_id: number;
  /**
   * The app’s version number. The app's version number corresponds to the value
   * of CFBundleVersion (in iOS) or CFBundleShortVersionString (in macOS) in
   * the Info.plist. In production, this value is the current version of
   * the app on the device based on the receipt_creation_date_ms.
   * In the sandbox, the value is always "1.0".
   */
  application_version: string;
  /**
   * The bundle identifier for the app to which the receipt belongs.
   * You provide this string on App Store Connect. This corresponds to
   * the value of CFBundleIdentifier in the Info.plist file of the app.
   */
  bundle_id: string;
  /**
   * A unique identifier for the app download transaction.
   */
  download_id: number;
  /**
   * The time the receipt expires for apps purchased through the Volume Purchase
   * Program, in a date-time format similar to the ISO 8601.
   */
  expiration_date?: string;
  /**
   * The time the receipt expires for apps purchased through the Volume Purchase
   * Program, in UNIX epoch time format, in milliseconds. If this key is not
   * present for apps purchased through the Volume Purchase Program, the receipt
   * does not expire. Use this time format for processing dates.
   */
  expiration_date_ms?: string;
  /**
   * The time the receipt expires for apps purchased through the Volume Purchase
   * Program, in the Pacific Time zone.
   */
  expiration_date_pst?: string;
  /**
   * An array that contains the in-app purchase receipt fields for all
   * in-app purchase transactions.
   */
  in_app: BaseReceiptInfo[];
  /**
   * The version of the app that the user originally purchased. This value
   * does not change, and corresponds to the value of CFBundleVersion (in iOS)
   * or CFBundleShortVersionString (in macOS) in the Info.plist file of
   * the original purchase. In the sandbox environment, the value is always "1.0".
   */
  original_application_version: string;
  /**
   * The time of the original app purchase, in a date-time format similar to
   * ISO 8601.
   */
  original_purchase_date: string;
  /**
   * The time of the original app purchase, in UNIX epoch time format, in
   * milliseconds. Use this time format for processing dates.
   */
  original_purchase_date_ms: string;
  /**
   * The time of the original app purchase, in the Pacific Time zone.
   */
  original_purchase_date_pst: string;
  /**
   * The time the user ordered the app available for pre-order, in
   * a date-time format similar to ISO 8601.
   */
  preorder_date?: string;
  /**
   * The time the user ordered the app available for pre-order, in UNIX epoch
   * time format, in milliseconds. This field is only present if the user
   * pre-orders the app. Use this time format for processing dates.
   */
  preorder_date_ms?: string;
  /**
   * The time the user ordered the app available for pre-order, in the Pacific
   * Time zone.
   */
  preorder_date_pst?: string;
  /**
   * The time the App Store generated the receipt, in a date-time format similar
   * to ISO 8601.
   */
  receipt_creation_date: string;
  /**
   * The time the App Store generated the receipt, in UNIX epoch time format,
   * in milliseconds. Use this time format for processing dates. This value
   * does not change.
   */
  receipt_creation_date_ms: string;
  /**
   * The time the App Store generated the receipt, in the Pacific Time zone.
   */
  receipt_creation_date_pst: string;
  /**
   * The type of receipt generated. The value corresponds to the environment
   * in which the app or VPP purchase was made.
   */
  receipt_type:
    | 'Production'
    | 'ProductionVPP'
    | 'ProductionSandbox'
    | 'ProductionVPPSandbox';
  /**
   * The time the request to the verifyReceipt endpoint was processed and
   * the response was generated, in a date-time format similar to ISO 8601.
   */
  request_date: string;
  /**
   * The time the request to the verifyReceipt endpoint was processed and
   * the response was generated, in UNIX epoch time format, in milliseconds.
   * Use this time format for processing dates.
   */
  request_date_ms: string;
  /**
   * The time the request to the verifyReceipt endpoint was processed and
   * the response was generated, in the Pacific Time zone.
   */
  request_date_pst: string;
  /**
   * An arbitrary number that identifies a revision of your app.
   * In the sandbox, this key's value is “0”.
   */
  version_external_identifier: number;
}

export interface UnifiedReceipt {
  /**
   * The environment for which App Store generated the receipt.
   */
  environment: 'Sandbox' | 'Production';
  /**
   * The latest Base64-encoded app receipt.
   */
  latest_receipt: string;
  /**
   * An array that contains the latest 100 in-app purchase transactions of
   * the decoded value in latest_receipt. This array excludes transactions
   * for consumable products your app has marked as finished. The contents
   * of this array are identical to those in responseBody.Latest_receipt_info
   * in the verifyReceipt endpoint response for receipt validation.
   */
  latest_receipt_info: LatestReceiptInfo[];
  /**
   * An array where each element contains the pending renewal information
   * for each auto-renewable subscription identified in product_id.
   * The contents of this array are identical to those in
   * responseBody.Pending_renewal_info in the verifyReceipt endpoint response
   * for receipt validation.
   */
  pending_renewal_info: PendingRenewalInfo[];
  /**
   * The status code, where 0 indicates that the notification is valid.
   */
  status: number;
}

export interface Notification {
  /**
   * An identifier that App Store Connect generates and the App Store uses to
   * uniquely identify the auto-renewable subscription that the user’s
   * subscription renews. Treat this value as a 64-bit integer.
   */
  auto_renew_adam_id?: string;
  /**
   * The product identifier of the auto-renewable subscription that the user’s
   * subscription renews.
   */
  auto_renew_product_id: string;
  /**
   * The current renewal status for an auto-renewable subscription product.
   * Note that these values are different from those of the auto_renew_status
   * in the receipt.
   */
  auto_renew_status: 'true' | 'false';
  /**
   * The time at which the user turned on or off the renewal status for
   * an auto-renewable subscription, in a date-time format similar to
   * the ISO 8601 standard.The time at which the user turned on or off
   * the renewal status for an auto-renewable subscription, in a date-time
   * format similar to the ISO 8601 standard.
   */
  auto_renew_status_change_date?: string;
  /**
   * The time at which the user turned on or off the renewal status for
   * an auto-renewable subscription, in UNIX epoch time format, in milliseconds.
   * Use this time format to process dates.
   */
  auto_renew_status_change_date_ms?: string;
  /**
   * The time at which the user turned on or off the renewal status for
   * an auto-renewable subscription, in the Pacific time zone.
   */
  auto_renew_status_change_date_pst?: string;
  /**
   * The environment for which App Store generated the receipt.
   */
  environment: 'Sandbox' | 'PROD';
  /**
   * The reason a subscription expired. This field is only present for
   * an expired auto-renewable subscription.
   * See https://developer.apple.com/documentation/appstorereceipts/expiration_intent
   */
  expiration_intent?: ExpirationIntent;
  /**
   * The subscription event that triggered the notification.
   */
  notification_type:
    | 'CANCEL' // Indicates that Apple Support canceled the auto-renewable subscription and the customer received a refund
    | 'CONSUMPTION_REQUEST' // Indicates that the customer initiated a refund request for a consumable in-app purchase, and the App Store is requesting that you provide consumption data.
    | 'DID_CHANGE_RENEWAL_PREF' // Indicates that the customer made a change in their subscription plan that takes effect at the next renewal. The currently active plan isn’t affected.
    | 'DID_CHANGE_RENEWAL_STATUS' // Indicates a change in the subscription renewal status.
    | 'DID_FAIL_TO_RENEW' // Indicates a subscription that failed to renew due to a billing issue.
    | 'DID_RECOVER' // Indicates a successful automatic renewal of an expired subscription that failed to renew in the past.
    | 'DID_RENEW' // Indicates that a customer’s subscription has successfully auto-renewed for a new transaction period.
    | 'INITIAL_BUY' // Occurs at the user’s initial purchase of the subscription.
    | 'INTERACTIVE_RENEWAL' // Indicates the customer renewed a subscription interactively, either by using your app’s interface, or on the App Store in the account’s Subscriptions settings.
    | 'PRICE_INCREASE_CONSENT' // Indicates that App Store has started asking the customer to consent to your app’s subscription price increase.
    | 'REFUND' // Indicates that the App Store successfully refunded a transaction for a consumable in-app purchase, a non-consumable in-app purchase, or a non-renewing subscription.
    | 'REVOKE'; // Indicates that an in-app purchase the user was entitled to through Family Sharing is no longer available through sharing.
  /**
   * The same value as the shared secret you submit in the password field of
   * the requestBody when validating receipts.
   */
  password?: string;
  /**
   * An object that contains information about the most-recent, in-app purchase
   * transactions for the app.
   */
  unified_receipt: UnifiedReceipt;
  /**
   * A string that contains the app bundle ID.
   */
  bid: string;
  /**
   * A string that contains the app bundle version.
   */
  bvrs: string;
}
