# @os-team/app-store-server-notifications [![NPM version](https://img.shields.io/npm/v/@os-team/app-store-server-notifications)](https://yarnpkg.com/package/@os-team/app-store-server-notifications) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/app-store-server-notifications)](https://bundlephobia.com/result?p=@os-team/app-store-server-notifications)

Types for the App Store Server Notification. Contains the function to validate receipts.

Fully complies with the [App Store Server Notifications](https://developer.apple.com/documentation/appstoreservernotifications) and [verifyReceipt](https://developer.apple.com/documentation/appstorereceipts/verifyreceipt) documentations.

## Usage

Install the package using the following command:

```
yarn add @os-team/app-store-server-notifications
```

### Using the Notification type

Let's assume that you use the `express` library.
Add a new route to receive notifications from the App Store as follows:

```ts
import { Notification } from '@os-team/app-store-server-notifications';

app.post('/app-store-notification', (req, res) => {
  const { notification_type } = req.body as Notification;

  if (notification_type === 'INITIAL_BUY') {
    // Do whatever you want
  }

  res.status(200).json({ ok: true });
});
```

### Verifying a receipt

```ts
import { verifyReceipt } from '@os-team/app-store-server-notifications';

app.post('/verify-receipt', async (req, res) => {
  const response = await verifyReceipt({
    receiptData: req.body.receipt,
    password: 'appSpecificSharedSecret',
    excludeOldTransactions: true,
  });

  res.status(200).json({ ok: response.status === 0 });
});
```

You can pass the environment parameter with one of the possible values:

- `sandbox` – The receipt will be sent to the sandbox URL https://sandbox.itunes.apple.com/verifyReceipt to verification.
- `production` – The receipt will be sent to the production URL https://buy.itunes.apple.com/verifyReceipt to verification.
- `auto` (default) – At first, it tries to verify the receipt using the production URL. If the response has the 21007 status code (the receipt is from the test environment, but it was sent to the production environment for verification), it tries to verify the receipt using the sandbox URL.

```ts
const response = await verifyReceipt(
  {
    receiptData: req.body.receipt,
    password: 'appSpecificSharedSecret',
    excludeOldTransactions: true,
  },
  'sandbox'
);
```
