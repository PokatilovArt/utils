interface Language {
  formsLength: number;
  getIndex: (n: number) => number;
}

const LANGUAGES: Record<string, Language> = {
  en: {
    formsLength: 2,
    getIndex: (n) => (n === 1 ? 0 : 1),
  },
  ru: {
    formsLength: 3,
    getIndex: (n) => {
      if (n % 10 === 1 && n % 100 !== 11) return 0;
      return n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20)
        ? 1
        : 2;
    },
  },
};

interface Options {
  language: string;
  forms: string[];
  number: number;
}

const pluralForms = (options: Options): string => {
  // Check if the language is supported
  const rule = LANGUAGES[options.language];
  if (!rule) {
    const supportedLanguages = Object.keys(LANGUAGES).join(', ');
    throw new Error(
      `The language ${options.language} is not found. Only ${supportedLanguages} are supported.`
    );
  }

  // Check if the forms length is correct
  if (options.forms.length !== rule.formsLength) {
    throw new Error(`The forms length should be equals ${rule.formsLength}`);
  }

  return options.forms[rule.getIndex(options.number)];
};

export default pluralForms;
