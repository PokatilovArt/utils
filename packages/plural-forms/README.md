# @os-team/plural-forms [![NPM version](https://img.shields.io/npm/v/@os-team/plural-forms)](https://yarnpkg.com/package/@os-team/plural-forms) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/plural-forms)](https://bundlephobia.com/result?p=@os-team/plural-forms)

Declines nouns in the plural form. English and Russian languages are supported.

## Usage

Install the package using the following command:

```
yarn add @os-team/plural-forms
```

### Example 1. English language

```ts
const years = 21;
const yearsPluralForm = pluralForms({
  language: 'en',
  forms: ['year', 'years'],
  number: years,
});
console.log(`${years} ${yearsPluralForm}`); // 21 years
```

### Example 2. Russian language

```ts
const years = 21;
const yearsPluralForm = pluralForms({
  language: 'ru',
  forms: ['год', 'года', 'лет'],
  number: years,
});
console.log(`${years} ${yearsPluralForm}`); // 21 год
```
