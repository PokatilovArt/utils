# @os-team/typeorm-utils [![NPM version](https://img.shields.io/npm/v/@os-team/typeorm-utils)](https://yarnpkg.com/package/@os-team/typeorm-utils) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/typeorm-utils)](https://bundlephobia.com/result?p=@os-team/typeorm-utils)

Additional functions to work with databases using TypeORM.

- `createDatabaseIfNotExists` – Creates a new database if not exists.
- `dropDatabaseIfExists` – Drops a database if exists.
- `getConnectionByDatabase` – Creates a new connection by the database name. Creates database if not exists.
- `getDatabaseNameByHost` – Transforms a host to use it in the database name.
- `getNameByHost` – Transforms a host to the second-level domain name.
- `hasDatabase` – Check if a database exists.

## Installation

Install the package using the following command:

```
yarn add @os-team/typeorm-utils
```
