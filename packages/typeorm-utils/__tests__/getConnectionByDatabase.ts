import { Connection, createConnection } from 'typeorm';
import { PostgresConnectionOptions } from 'typeorm/browser/driver/postgres/PostgresConnectionOptions';
import getConnectionByDatabase, {
  closeConnections,
} from '../src/utils/getConnectionByDatabase';
import hasDatabase from '../src/utils/hasDatabase';
import dropDatabaseIfExists from '../src/utils/dropDatabaseIfExists';

const TEST_DATABASE = '_test_';

let conn: Connection;

beforeAll(async () => {
  conn = await createConnection();
});

afterAll(async () => {
  await closeConnections();
  await dropDatabaseIfExists(conn, TEST_DATABASE);
  await conn.close();
});

it('Should create the new database and return the connection', async () => {
  const connection = await getConnectionByDatabase(conn, TEST_DATABASE);
  expect(connection).toBeDefined();

  expect(await hasDatabase(conn, TEST_DATABASE)).toBeTruthy();
  expect(await getConnectionByDatabase(conn, TEST_DATABASE)).toStrictEqual(
    connection
  );
});

it('Should not create the new database', async () => {
  const options = conn.options as PostgresConnectionOptions;
  const connection = await getConnectionByDatabase(
    conn,
    options.database as string
  );
  expect(connection).toBeDefined();

  expect(
    await getConnectionByDatabase(conn, options.database as string)
  ).toStrictEqual(connection);
});
