import { Connection, createConnection, getConnectionOptions } from 'typeorm';
import { PostgresConnectionOptions } from 'typeorm/browser/driver/postgres/PostgresConnectionOptions';
import hasDatabase from '../src/utils/hasDatabase';
import createDatabaseIfNotExists from '../src/utils/createDatabaseIfNotExists';
import dropDatabaseIfExists from '../src/utils/dropDatabaseIfExists';

const TEST_DATABASE = '_test_';

let conn: Connection;

beforeAll(async () => {
  conn = await createConnection();
});

afterAll(async () => {
  await dropDatabaseIfExists(conn, TEST_DATABASE);
  await conn.close();
});

it('Should create the non-existent database', async () => {
  await createDatabaseIfNotExists(conn, TEST_DATABASE);
  expect(await hasDatabase(conn, TEST_DATABASE)).toBeTruthy();
});

it('Should not create the existing database', async () => {
  const options = (await getConnectionOptions()) as PostgresConnectionOptions;
  await createDatabaseIfNotExists(conn, options.database as string);
  expect(await hasDatabase(conn, options.database as string)).toBeTruthy();
});
