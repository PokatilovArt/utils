import getDatabaseNameByHost from '../src/utils/getDatabaseNameByHost';

test('localhost:4000', () => {
  const name = getDatabaseNameByHost('localhost:4000');
  expect(name).toBe('localhost');
});

test('example.com', () => {
  const name = getDatabaseNameByHost('example.com');
  expect(name).toBe('example');
});

test('sub.example.com', () => {
  const name = getDatabaseNameByHost('sub.example.com');
  expect(name).toBe('example_sub');
});

test('sub.super-example.com', () => {
  const name = getDatabaseNameByHost('sub.super-example.com');
  expect(name).toBe('super_example_sub');
});
