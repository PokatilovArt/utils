import { Connection, createConnection } from 'typeorm';
import hasDatabase from '../src/utils/hasDatabase';
import createDatabaseIfNotExists from '../src/utils/createDatabaseIfNotExists';
import dropDatabaseIfExists from '../src/utils/dropDatabaseIfExists';

const TEST_DATABASE = '_test_';

let conn: Connection;

beforeAll(async () => {
  conn = await createConnection();
  await createDatabaseIfNotExists(conn, TEST_DATABASE);
});

afterAll(async () => {
  await conn.close();
});

it('Should drop the existing database', async () => {
  await dropDatabaseIfExists(conn, TEST_DATABASE);
  expect(await hasDatabase(conn, TEST_DATABASE)).toBeFalsy();
});

it('Should not drop the non-existent database', async () => {
  await dropDatabaseIfExists(conn, 'non-existent');
  expect(await hasDatabase(conn, 'non-existent')).toBeFalsy();
});
