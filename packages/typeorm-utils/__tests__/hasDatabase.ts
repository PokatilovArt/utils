import { Connection, createConnection, getConnectionOptions } from 'typeorm';
import { PostgresConnectionOptions } from 'typeorm/browser/driver/postgres/PostgresConnectionOptions';
import hasDatabase from '../src/utils/hasDatabase';

let conn: Connection;

beforeAll(async () => {
  conn = await createConnection();
});

afterAll(async () => {
  await conn.close();
});

test('The existing database', async () => {
  const options = (await getConnectionOptions()) as PostgresConnectionOptions;
  expect(await hasDatabase(conn, options.database as string)).toBeTruthy();
});

test('The non-existent database', async () => {
  expect(await hasDatabase(conn, 'non-existent')).toBeFalsy();
});
