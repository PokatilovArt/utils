/**
 * Transforms a host to use it in the database name.
 * @example
 * getDatabaseNameByHost('example.com') // example
 * getDatabaseNameByHost('sub.company-example.com') // company_example_sub
 */
const getDatabaseNameByHost = (host: string): string =>
  host
    .trim()
    .toLowerCase()
    .replace(/(.*):[0-9]+$/, '$1')
    .replace(/(.*)\.[a-z]+$/, '$1')
    .split('.')
    .reverse()
    .join('_')
    .replace('-', '_');

export default getDatabaseNameByHost;
