import { Connection } from 'typeorm';

/**
 * Check if a database exists.
 */
const hasDatabase = async (
  conn: Connection,
  database: string
): Promise<boolean> => {
  const res = await conn.query('SELECT 1 FROM pg_database WHERE datname=$1', [
    database,
  ]);
  return res.length > 0;
};

export default hasDatabase;
