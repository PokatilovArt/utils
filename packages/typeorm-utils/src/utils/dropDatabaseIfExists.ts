import { Connection } from 'typeorm';
import hasDatabase from './hasDatabase';

/**
 * Drops a database if exists.
 */
const dropDatabaseIfExists = async (
  conn: Connection,
  database: string
): Promise<void> => {
  if (await hasDatabase(conn, database)) {
    await conn.query(`DROP DATABASE ${database}`);
  }
};

export default dropDatabaseIfExists;
