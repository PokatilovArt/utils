import { Connection, ConnectionOptions, createConnection } from 'typeorm';
import { SqljsConnectionOptions } from 'typeorm/driver/sqljs/SqljsConnectionOptions';
import createDatabaseIfNotExists from './createDatabaseIfNotExists';

export interface ConnectionsMap {
  [database: string]: Connection;
}

const CONNECTION_MAP: ConnectionsMap = {};

/**
 * Closes all database connections.
 */
export const closeConnections = (): Promise<void[]> => {
  const promises = Object.values(CONNECTION_MAP).map((conn) => conn.close());
  return Promise.all(promises);
};

/**
 * Creates a new connection by the database name. Creates database if not exists.
 * Does not work with SqljsConnectionOptions.
 */
const getConnectionByDatabase = async (
  conn: Connection,
  database: string
): Promise<Connection> => {
  if (CONNECTION_MAP[database] === undefined) {
    await createDatabaseIfNotExists(conn, database);

    CONNECTION_MAP[database] = await createConnection({
      ...(conn.options as Exclude<ConnectionOptions, SqljsConnectionOptions>),
      database,
      name: database,
    });
  }

  return CONNECTION_MAP[database];
};

export default getConnectionByDatabase;
