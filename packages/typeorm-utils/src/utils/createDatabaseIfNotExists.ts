import { Connection, createConnection } from 'typeorm';
import hasDatabase from './hasDatabase';

/**
 * Creates a new database if not exists.
 * Uses a new connection to do it. It fixes the "database is being accessed by other users" error.
 */
const createDatabaseIfNotExists = async (
  conn: Connection,
  database: string
): Promise<void> => {
  if (!(await hasDatabase(conn, database))) {
    const connection = await createConnection({
      ...conn.options,
      name: '_db_creator',
    });
    await connection.query(`CREATE DATABASE ${database}`);
    await connection.close();
  }
};

export default createDatabaseIfNotExists;
