export { default as createDatabaseIfNotExists } from './utils/createDatabaseIfNotExists';
export { default as dropDatabaseIfExists } from './utils/dropDatabaseIfExists';
export { default as getConnectionByDatabase } from './utils/getConnectionByDatabase';
export { default as getDatabaseNameByHost } from './utils/getDatabaseNameByHost';
export { default as hasDatabase } from './utils/hasDatabase';

export * from './utils/getConnectionByDatabase';
