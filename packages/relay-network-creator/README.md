# @os-team/relay-network-creator [![NPM version](https://img.shields.io/npm/v/@os-team/relay-network-creator)](https://yarnpkg.com/package/@os-team/relay-network-creator) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/relay-network-creator)](https://bundlephobia.com/result?p=@os-team/relay-network-creator)

Super tiny network layer for Relay.

## Usage

### Step 1. Install the package

Install the package using the following command:

```
yarn add @os-team/relay-network-creator
```

It depends on `relay-runtime` (in peerDependencies), so if you haven't installed it yet, do so with the following command:

```
npx install-peerdeps @os-team/relay-network-creator
```

### Step 2. Create a Relay Network

To create a Relay Network call the `createRelayNetwork` function and pass the URL to the API.

```ts
import { createRelayNetwork } from '@os-team/relay-network-creator';

createRelayNetwork('https://api.domain.com');
```

You can also pass middlewares:

```ts
import { createRelayNetwork } from '@os-team/relay-network-creator';
import upload from '@os-team/relay-network-mw-upload';

createRelayNetwork('https://api.domain.com', [upload]);
```

Read more about the middlewares below.

## Example of creating a Relay Environment

```ts
import { Environment, RecordSource, Store } from 'relay-runtime';
import { createRelayNetwork } from '@os-team/relay-network-creator';

const getApiUrl = () => {
  if (process.env.NODE_ENV !== 'production') {
    return 'http://localhost:4000/graphql';
  }
  return 'https://api.domain.com/graphql';
};

let environment: Environment;

const createRelayEnvironment = (): Environment => {
  if (!environment) {
    environment = new Environment({
      network: createRelayNetwork(getApiUrl()), // Create a Relay Network
      store: new Store(new RecordSource()),
    });
  }

  return environment;
};

export default createRelayEnvironment;
```

If you use `Next.js`:

```ts
import 'isomorphic-unfetch';
import { Environment, RecordSource, Store } from 'relay-runtime';
import { RecordMap } from 'relay-runtime/lib/store/RelayStoreTypes';
import { createRelayNetwork } from '@os-team/relay-network-creator';

let environment: Environment;

const getApiUrl = () => {
  if (process.env.NODE_ENV !== 'production') {
    return 'http://localhost:4000/graphql';
  }
  return 'https://api.domain.com/graphql';
};

const createRelayEnvironment = (initialRecords?: RecordMap): Environment => {
  const env =
    environment ||
    new Environment({
      network: createRelayNetwork(getApiUrl()), // Create a Relay Network
      store: new Store(new RecordSource(initialRecords)),
    });

  // For SSG and SSR always create a new Relay environment
  if (typeof window === 'undefined') return env;

  // Create the Relay environment once in the client
  if (!environment) environment = env;

  return environment;
};

export default createRelayEnvironment;
```

## Middlewares

The following shared middlewares are currently available:

- [@os-team/relay-network-mw-app-user-agent](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/relay-network-mw-app-user-agent)
- [@os-team/relay-network-mw-credentials](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/relay-network-mw-credentials)
- [@os-team/relay-network-mw-timeout](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/relay-network-mw-timeout)
- [@os-team/relay-network-mw-upload](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/relay-network-mw-upload)

All middlewares will have the prefix `relay-network-mw-`, so you can see any middlewares in `npm` by this prefix.

### Creating a simple middleware

Sometimes you need to transform any request or response. For example, to pass `credentials: 'include'` to fetch options in each request, create the following middleware:

```ts
import { RelayNetworkMiddleware } from '@os-team/relay-network-creator';

const credentials: RelayNetworkMiddleware = (next) => (req) => {
  req.fetchOptions.credentials = 'include';
  return next(req);
};

export default credentials;
```

Now, to connect this middleware, pass it as the second parameter in the `createRelayNetwork` function:

```ts
import { createRelayNetwork } from '@os-team/relay-network-creator';
import credentials from './credentials';

createRelayNetwork('https://api.domain.com', [credentials]);
```

The example above shows the `@os-team/relay-network-mw-credentials` middleware, so if you need to include credentials in each request, just use this middleware.

In middlewares, you can change not only `fetchOptions`, but also `url`, `request`, `variables`, `cacheConfig`, and `uploadables`.
See the source of the [@os-team/relay-network-mw-upload](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/relay-network-mw-upload) library to see a slightly more complex middleware.

### Middleware to check the authorization

Let's look at another example.
Most likely, your application has authorization.
It would be a good idea to check every response coming from the GraphQL server for an authorization error.

If an authorization error was found, we will redirect the user to the login page.
Thus, if your API deletes a user's session (for example, from Redis), the user will be redirected to the login page when he or she tries to make the next authorized request.

```ts
import { RelayNetworkMiddleware } from '@os-team/relay-network-creator';

const redirectUnauthorized: RelayNetworkMiddleware = (next) => async (req) => {
  const res = await next(req);
  const { errors } = res;
  if (
    Array.isArray(errors) &&
    errors.length > 0 &&
    errors[0].message.startsWith('Access denied!')
  ) {
    window.location.href = 'https://login.domain.com';
  }
  return res;
};

export default redirectUnauthorized;
```

Now connect it the same way:

```ts
import { createRelayNetwork } from '@os-team/relay-network-creator';
import credentials from '@os-team/relay-network-mw-credentials';
import redirectUnauthorized from './redirectUnauthorized';

createRelayNetwork('https://api.domain.com', [
  credentials,
  redirectUnauthorized,
]);
```
