import { FetchFunction, Network, INetwork } from 'relay-runtime';
import RelayRequest from './RelayRequest';
import createRelayResponse, { RelayResponse } from './createRelayResponse';

export type RelayNetworkNextFunction = (
  req: RelayRequest
) => Promise<RelayResponse>;

export type RelayNetworkMiddleware = (
  next: RelayNetworkNextFunction
) => RelayNetworkNextFunction;

const createRelayNetwork = (
  url: string,
  middlewares: RelayNetworkMiddleware[] = []
): INetwork => {
  const fetchFn = (request, variables, cacheConfig, uploadables) => {
    const next: RelayNetworkNextFunction = async (req) => {
      const res = await fetch(req.url, req.fetchOptions);
      return createRelayResponse(res);
    };

    const fn = middlewares.reduceRight(
      (previousNextFunction, currentMiddleware) =>
        currentMiddleware(previousNextFunction),
      (req: RelayRequest) => next(req)
    );

    const req = new RelayRequest({
      url,
      request,
      variables,
      cacheConfig,
      uploadables,
    });

    return fn(req);
  };

  return Network.create(fetchFn as FetchFunction);
};

export default createRelayNetwork;
