import {
  CacheConfig,
  RequestParameters,
  UploadableMap,
  Variables,
} from 'relay-runtime';

export interface RelayRequestInitProps {
  url: string;
  request: RequestParameters;
  variables?: Variables;
  cacheConfig?: CacheConfig;
  uploadables?: UploadableMap | null;
}

interface FetchOptions extends RequestInit {
  headers: HeadersInit;
}

class RelayRequest {
  url: string;

  fetchOptions: FetchOptions;

  request: RequestParameters;

  variables: Variables;

  cacheConfig: CacheConfig;

  uploadables?: UploadableMap | null;

  constructor(props: RelayRequestInitProps) {
    this.request = props.request;
    this.variables = props.variables || {};
    this.cacheConfig = props.cacheConfig || {};
    this.uploadables = props.uploadables;

    this.url = props.url;
    this.fetchOptions = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        query: this.request.text,
        variables: this.variables,
      }),
    };
  }
}

export default RelayRequest;
