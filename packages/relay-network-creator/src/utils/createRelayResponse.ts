import { PayloadData, PayloadError } from 'relay-runtime';

export interface RelayResponse {
  data: PayloadData | null;
  errors?: PayloadError[];
  headers: Headers;
  ok: boolean;
  redirected: boolean;
  status: number;
  statusText: string;
  type: ResponseType;
  url: string;
}

const createRelayResponse = async (res: Response): Promise<RelayResponse> => ({
  ...(await res.json()),
  headers: res.headers,
  ok: res.ok,
  redirected: res.redirected,
  status: res.status,
  statusText: res.statusText,
  type: res.type,
  url: res.url,
});

export default createRelayResponse;
