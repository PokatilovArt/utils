const getPasswordScore = (password: string): number => {
  if (!password) return 0;
  let score = 0;

  // Award every unique letter until 5 repetitions
  const letters = {};
  for (let i = 0; i < password.length; i += 1) {
    letters[password[i]] = (letters[password[i]] || 0) + 1;
    score += 5.0 / letters[password[i]];
  }

  // Bonus points for mixing it up
  const variations = {
    digits: /\d/.test(password),
    lower: /[a-z]/.test(password),
    upper: /[A-Z]/.test(password),
    nonWords: /\W/.test(password),
  };

  let variationCount = 0;
  Object.values(variations).forEach((has) => {
    variationCount += has ? 1 : 0;
  });
  score += (variationCount - 1) * 10;

  return Math.round(score);
};

export default getPasswordScore;
