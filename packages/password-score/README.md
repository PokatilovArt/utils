# @os-team/password-score [![NPM version](https://img.shields.io/npm/v/@os-team/password-score)](https://yarnpkg.com/package/@os-team/password-score) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/password-score)](https://bundlephobia.com/result?p=@os-team/password-score)

Calculates a password strength score.

## Usage

### Install the package

Install the package using the following command:

```
yarn add @os-team/password-score
```

### Simple usage

```ts
import getPasswordScore from '@os-team/password-score';

const passwordScore = getPasswordScore('password'); // 38
```

The following thresholds can be used to determine the strength of a password:

- `[score] < 30` – weak.
- `30 ≤ [score] < 60` – good.
- `60 ≤ [score] < 80` – strong.
- `[score] ≥ 80` – powerful.

Based on [this answer](https://stackoverflow.com/a/11268104/8896318).
