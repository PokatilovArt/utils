import draftToHtml from './index';

test('No blocks', () => {
  const state = {
    blocks: [],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe('');
});

test('Unstyled', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'Unstyled',
        type: 'unstyled',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe('<p>Unstyled</p>');
});

test('Paragraph', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'Paragraph',
        type: 'paragraph',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe('<p>Paragraph</p>');
});

test('Header 1', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'Header 1',
        type: 'header-one',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe('<h1>Header 1</h1>');
});

test('Header 2', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'Header 2',
        type: 'header-two',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe('<h2>Header 2</h2>');
});

test('Header 3', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'Header 3',
        type: 'header-three',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe('<h3>Header 3</h3>');
});

test('Header 4', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'Header 4',
        type: 'header-four',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe('<h4>Header 4</h4>');
});

test('Header 5', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'Header 5',
        type: 'header-five',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe('<h5>Header 5</h5>');
});

test('Header 6', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'Header 6',
        type: 'header-six',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe('<h6>Header 6</h6>');
});

test('Unordered list', () => {
  const state = {
    blocks: [
      {
        key: 'key1',
        data: {},
        text: 'Unordered list item 1',
        type: 'unordered-list-item',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
      {
        key: 'key2',
        data: {},
        text: 'Unordered list item 2',
        type: 'unordered-list-item',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe(
    '<ul><li>Unordered list item 1</li><li>Unordered list item 2</li></ul>'
  );
});

test('Ordered list', () => {
  const state = {
    blocks: [
      {
        key: 'key1',
        data: {},
        text: 'Ordered list item 1',
        type: 'ordered-list-item',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
      {
        key: 'key2',
        data: {},
        text: 'Ordered list item 2',
        type: 'ordered-list-item',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe(
    '<ol><li>Ordered list item 1</li><li>Ordered list item 2</li></ol>'
  );
});

test('Blockquote', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'Blockquote',
        type: 'blockquote',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe('<blockquote>Blockquote</blockquote>');
});

test('Code block', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'Code block',
        type: 'code-block',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe('<pre>Code block</pre>');
});

test('Atomic', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'Atomic',
        type: 'atomic',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe('<figure>Atomic</figure>');
});

test('Inline styles', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'bold, code, italic, strikethrough, underline, custom',
        type: 'paragraph',
        depth: 0,
        entityRanges: [],
        inlineStyleRanges: [
          {
            style: 'BOLD',
            length: 4,
            offset: 0,
          },
          {
            style: 'CODE',
            length: 4,
            offset: 6,
          },
          {
            style: 'ITALIC',
            length: 6,
            offset: 12,
          },
          {
            style: 'STRIKETHROUGH',
            length: 13,
            offset: 20,
          },
          {
            style: 'UNDERLINE',
            length: 9,
            offset: 35,
          },
          {
            style: 'CUSTOM',
            length: 6,
            offset: 46,
          },
        ],
      },
    ],
  };

  const html = draftToHtml({
    value: state,
  });

  expect(html).toBe(
    '<p><b>bold</b>, <code>code</code>, <i>italic</i>, <s>strikethrough</s>, <u>underline</u>, custom</p>'
  );
});

describe('Custom block element', () => {
  test('paragraph', () => {
    const state = {
      blocks: [
        {
          key: 'key',
          data: {},
          text: 'Paragraph',
          type: 'paragraph',
          depth: 0,
          entityRanges: [],
          inlineStyleRanges: [],
        },
      ],
    };

    const html = draftToHtml({
      value: state,
      blockRenderer: (block, children) => {
        if (block.type === 'paragraph') {
          return `<div>${children}</div>`;
        }
        return null;
      },
    });

    expect(html).toBe('<div>Paragraph</div>');
  });

  test('atomic:image', () => {
    const state = {
      blocks: [
        {
          key: 'key',
          data: {
            src: 'https://picsum.photos/200',
          },
          text: 'Image description',
          type: 'atomic:image',
          depth: 0,
          entityRanges: [],
          inlineStyleRanges: [],
        },
      ],
    };

    const html = draftToHtml({
      value: state,
      blockRenderer: (block) => {
        if (block.type === 'atomic:image') {
          return `<img src="${block.data.src}" alt="${block.text}" />`;
        }
        return null;
      },
    });

    expect(html).toBe(
      '<img src="https://picsum.photos/200" alt="Image description" />'
    );
  });
});

test('Custom entity element', () => {
  const state = {
    blocks: [
      {
        key: 'key',
        data: {},
        text: 'Lorem ipsum link dolor sit amet.',
        type: 'paragraph',
        depth: 0,
        entityRanges: [
          {
            key: 0,
            length: 4,
            offset: 12,
          },
        ],
      },
    ],
    entityMap: {
      '0': {
        data: {
          url: 'https://google.com',
        },
        type: 'LINK',
        mutability: 'MUTABLE',
      },
    },
  };

  const html = draftToHtml({
    value: state,
    entityRenderer: (entity) => {
      if (
        entity.type === 'LINK' &&
        entity.data &&
        typeof entity.data.url === 'string'
      ) {
        return `<a href="${entity.data.url}">${entity.text}</a>`;
      }
      return null;
    },
  });

  expect(html).toBe(
    '<p>Lorem ipsum <a href="https://google.com">link</a> dolor sit amet.</p>'
  );
});

describe('Custom inline style element', () => {
  test('BOLD', () => {
    const state = {
      blocks: [
        {
          key: 'key',
          data: {},
          text: 'bold',
          type: 'paragraph',
          depth: 0,
          entityRanges: [],
          inlineStyleRanges: [
            {
              style: 'BOLD',
              length: 4,
              offset: 0,
            },
          ],
        },
      ],
    };

    const html = draftToHtml({
      value: state,
      inlineStyleRenderer: (inlineStyle) => {
        if (inlineStyle.style === 'BOLD') {
          return `<span class="bold">${inlineStyle.text}</span>`;
        }
        return null;
      },
    });

    expect(html).toBe('<p><span class="bold">bold</span></p>');
  });

  test('CUSTOM', () => {
    const state = {
      blocks: [
        {
          key: 'key',
          data: {},
          text: 'custom',
          type: 'paragraph',
          depth: 0,
          entityRanges: [],
          inlineStyleRanges: [
            {
              style: 'CUSTOM',
              length: 6,
              offset: 0,
            },
          ],
        },
      ],
    };

    const html = draftToHtml({
      value: state,
      inlineStyleRenderer: (inlineStyle) => {
        if (inlineStyle.style === 'CUSTOM') {
          return `<span class="custom">${inlineStyle.text}</span>`;
        }
        return null;
      },
    });

    expect(html).toBe('<p><span class="custom">custom</span></p>');
  });
});
