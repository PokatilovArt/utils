export interface HTMLElement {
  tag: string;
  children?: HTMLElement | string | Array<HTMLElement | string>;
}

const htmlElementToString = (htmlElement: HTMLElement): string => {
  const { tag, children } = htmlElement;

  if (!children) return `<${tag} />`;

  if (Array.isArray(children)) {
    return `<${tag}>${children
      .map((child) =>
        typeof child === 'object' ? htmlElementToString(child) : child
      )
      .join('')}</${tag}>`;
  }
  if (typeof children === 'object') {
    return `<${tag}>${htmlElementToString(children)}</${tag}>`;
  }
  return `<${tag}>${children}</${tag}>`;
};

export default htmlElementToString;
