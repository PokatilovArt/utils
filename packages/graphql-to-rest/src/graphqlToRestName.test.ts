import graphqlToRestName from './graphqlToRestName';

test('camelCaseCamelCase', () => {
  const restName = graphqlToRestName('camelCaseCamelCase');
  expect(restName).toBe('camel-case-camel-case');
});

test('is1Number', () => {
  const restName = graphqlToRestName('is1Number');
  expect(restName).toBe('is1-number');
});

test('is123Number', () => {
  const restName = graphqlToRestName('is123Number');
  expect(restName).toBe('is123-number');
});

test('manyCCapitalLLLetters', () => {
  const restName = graphqlToRestName('manyCCapitalLLLetters');
  expect(restName).toBe('many-ccapital-llletters');
});
