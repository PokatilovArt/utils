const graphqlToRestName = (name: string) =>
  name.replace(/([A-Z]+[A-Z0-9]*)/g, (c) => `-${c.toLowerCase()}`);

export default graphqlToRestName;
