import 'setimmediate';
import express from 'express';
import superagent from 'superagent';
import { buildSchema } from 'graphql';
import http from 'http';
import graphqlToRest from './index';

const schema = buildSchema(`
  type Car {
    id: ID!
    model: String!
  }
  type User {
    id: ID!
    name: String!
    car: Car!
  }
  type Task {
    id: ID!
    name: String!
    description: String!
    user: User
    subtask: Task
  }
  input CreateTaskInput {
    name: String!
    description: String!
  }
  type Query {
    intArg(arg: Int!): Int!
    floatArg(arg: Float!): Float!
    stringArg(arg: String!): String!
    booleanArg(arg: Boolean!): Boolean!
    idArg(arg: ID!): ID!
    listArg(arg: [Int!]!): [Int!]!
    task: Task!
    taskFieldsArg(fields: String): Task!
    taskWithUser: Task!
    taskWithSubtask: Task!
  }
  type Mutation {
    createTask(input: CreateTaskInput!): Task!
    deleteTask: Boolean!
  }
`);

const BASE_TASK = {
  id: 'task1',
  name: 'name',
  description: 'description',
};

const root = {
  intArg: ({ arg }) => arg,
  floatArg: ({ arg }) => arg,
  stringArg: ({ arg }) => arg,
  booleanArg: ({ arg }) => arg,
  idArg: ({ arg }) => arg,
  listArg: ({ arg }) => arg,
  task: () => BASE_TASK,
  taskFieldsArg: () => BASE_TASK,
  taskWithUser: () => ({
    ...BASE_TASK,
    user: {
      id: 'user1',
      name: 'name',
      car: {
        id: 'car1',
        model: 'model',
      },
    },
  }),
  taskWithSubtask: () => ({
    ...BASE_TASK,
    subtask: {
      ...BASE_TASK,
      id: 'task2',
      subtask: {
        ...BASE_TASK,
        id: 'task3',
      },
    },
  }),
  createTask: ({ input }) => ({
    ...BASE_TASK,
    name: input.name,
    description: input.description,
  }),
  deleteTask: () => true,
};

const PORT = 4001;
let server: http.Server;

beforeAll(() => {
  // Start the server
  const app = express();
  app.use('/rest', graphqlToRest(schema, { rootValue: root }));
  app.use(
    '/depthLimit2',
    graphqlToRest(schema, { rootValue: root, depthLimit: 2 })
  );
  app.use(
    '/circularReferenceDepth2',
    graphqlToRest(schema, {
      rootValue: root,
      circularReferenceDepth: 2,
    })
  );
  app.use(
    '/include',
    graphqlToRest(schema, {
      rootValue: root,
      include: ['task'],
    })
  );
  app.use(
    '/exclude',
    graphqlToRest(schema, {
      rootValue: root,
      exclude: ['task'],
    })
  );
  app.use(
    '/methodDelete',
    graphqlToRest(schema, {
      rootValue: root,
      method: (field) => (field.startsWith('delete') ? 'DELETE' : null),
    })
  );
  app.use(
    '/fieldsKey',
    graphqlToRest(schema, {
      rootValue: root,
      fieldsKey: '_fields',
    })
  );
  app.use(
    '/errorHandler',
    graphqlToRest(schema, {
      rootValue: root,
      errorHandler: (errors) => ({
        body: { error: errors[0] },
        status: 200,
      }),
    })
  );
  server = app.listen(PORT);
});

afterAll(() => {
  server.close();
});

describe('Should convert query argument values depending on their types', () => {
  test('Int', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/int-arg`)
      .query({ arg: 141 });

    expect(res.body).toBe(141);
  });

  test('Float', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/float-arg`)
      .query({ arg: 141.59 });

    expect(res.body).toBe(141.59);
  });

  test('String', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/string-arg`)
      .query({ arg: 'hello' });

    expect(res.body).toBe('hello');
  });

  test('Boolean', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/boolean-arg`)
      .query({ arg: true });

    expect(res.body).toBeTruthy();
  });

  test('ID', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/id-arg`)
      .query({ arg: 'abcd' });

    expect(res.body).toBe('abcd');
  });

  test('List of Int', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/list-arg`)
      .query({ arg: [141, 59, 5] });

    expect(res.body).toStrictEqual([141, 59, 5]);
  });

  it('Should return a GraphQL error if the argument type is specified incorrectly', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/int-arg`)
      .query({ arg: 'incorrect' })
      .ok(() => true);

    expect(res.body.errors).toBeDefined();
  });
});

it('Should limit the depth', async () => {
  // depthLimit: 4 (default)
  const res1 = await superagent.get(
    `http://localhost:${PORT}/rest/task-with-user`
  );
  const task1 = res1.body;
  expect(task1.user).toBeDefined();
  expect(task1.user.car).toBeDefined();

  // depthLimit: 2 (specified)
  const res2 = await superagent.get(
    `http://localhost:${PORT}/depthLimit2/task-with-user`
  );
  const task2 = res2.body;
  expect(task2.user).toBeDefined();
  expect(task2.user.car).toBeUndefined();
});

it('Should limit the circular reference depth', async () => {
  // circularReferenceDepth: 1 (default)
  const res1 = await superagent.get(
    `http://localhost:${PORT}/rest/task-with-subtask`
  );
  const task1 = res1.body;
  expect(task1.subtask).toBeDefined();
  expect(task1.subtask.subtask).toBeUndefined();

  // circularReferenceDepth: 2 (specified)
  const res2 = await superagent.get(
    `http://localhost:${PORT}/circularReferenceDepth2/task-with-subtask`
  );
  const task2 = res2.body;
  expect(task2.subtask).toBeDefined();
  expect(task2.subtask.subtask).toBeDefined();
});

it('Should include only the task query', async () => {
  const res1 = await superagent
    .get(`http://localhost:${PORT}/include/task`)
    .ok((r) => r.status < 500);
  expect(res1.statusCode).toBe(200);

  const res2 = await superagent
    .get(`http://localhost:${PORT}/include/task-with-user`)
    .ok((r) => r.status < 500);
  expect(res2.statusCode).toBe(404);
});

it('Should exclude the task query', async () => {
  const res1 = await superagent
    .get(`http://localhost:${PORT}/exclude/task`)
    .ok((r) => r.status < 500);
  expect(res1.statusCode).toBe(404);

  const res2 = await superagent
    .get(`http://localhost:${PORT}/exclude/task-with-user`)
    .ok((r) => r.status < 500);
  expect(res2.statusCode).toBe(200);
});

it('Should redefine the method type', async () => {
  // All mutations have the POST method (default)
  const res1 = await superagent
    .delete(`http://localhost:${PORT}/rest/delete-task`)
    .ok((r) => r.status < 500);
  expect(res1.statusCode).toBe(404);

  // All methods starting with "delete" have the DELETE method (specified)
  const res2 = await superagent.delete(
    `http://localhost:${PORT}/methodDelete/delete-task`
  );
  const status = res2.body;
  expect(status).toBeTruthy();
});

describe('Should return only selected fields', () => {
  test('Query', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/task`)
      .query({ fields: 'id, name' });

    const task = res.body;
    expect(task.id).toBeDefined();
    expect(task.name).toBeDefined();
    expect(task.description).toBeUndefined();
  });

  test('Mutation', async () => {
    const res = await superagent
      .post(`http://localhost:${PORT}/rest/create-task`)
      .send({
        input: {
          name: 'My task',
          description: 'My description',
        },
        fields: ['id', 'name'],
      });

    const task = res.body;
    expect(task.id).toBeDefined();
    expect(task.name).toBeDefined();
    expect(task.description).toBeUndefined();
  });

  test('Custom fieldsKey', async () => {
    const res1 = await superagent
      .get(`http://localhost:${PORT}/fieldsKey/task`)
      .query({ fields: 'id, name' });
    const task1 = res1.body;
    expect(task1.id).toBeDefined();
    expect(task1.name).toBeDefined();
    expect(task1.description).toBeDefined();

    const res2 = await superagent
      .get(`http://localhost:${PORT}/fieldsKey/task`)
      .query({ _fields: 'id, name' });
    const task2 = res2.body;
    expect(task2.id).toBeDefined();
    expect(task2.name).toBeDefined();
    expect(task2.description).toBeUndefined();
  });

  it('Should ignore selected fields because the query has the same argument', async () => {
    const res = await superagent
      .get(`http://localhost:${PORT}/rest/task-fields-arg`)
      .query({ fields: 'id, name' });

    const task = res.body;
    expect(task.id).toBeDefined();
    expect(task.name).toBeDefined();
    expect(task.description).toBeDefined();
  });
});

it('Should change the error format and status code', async () => {
  const res = await superagent
    .get(`http://localhost:${PORT}/errorHandler/int-arg`)
    .query({ arg: 'incorrect' });

  expect(res.status).toBe(200);
  expect(res.body.error).toBeDefined();
  expect(res.body.errors).toBeUndefined();
});
