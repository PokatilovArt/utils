# @os-team/measurement-protocol [![NPM version](https://img.shields.io/npm/v/@os-team/measurement-protocol)](https://yarnpkg.com/package/@os-team/measurement-protocol) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/measurement-protocol)](https://bundlephobia.com/result?p=@os-team/measurement-protocol)

The Measurement Protocol allows sending events to Google Analytics by making HTTP requests.
This way, for example, you can send a purchase event from your server directly to Google Analytics.

## Usage

### Step 1. Install the package

Install the package using the following command:

```
yarn add @os-team/measurement-protocol
```

### Step 2. Validate an event

To ensure your event is correct, you should validate it using the Measurement Protocol Validation Server.

```ts
import MeasurementProtocol from '@os-team/measurement-protocol';

const measurementProtocol = new MeasurementProtocol({
  measurementId: 'measurementId',
  apiSecret: 'apiSecret',
});

const { validationMessages } = await measurementProtocol.validate({
  client_id: 'XXXXXXXXXX.YYYYYYYYYY',
  events: [
    {
      name: '_badEventName', // Event names must start with an alphabetic character
    },
  ],
});
```

See an [example of the validation response](https://developers.google.com/analytics/devguides/collection/protocol/ga4/validating-events?client_type=gtag#validation_response).

### Step 3. Send an event

```ts
await measurementProtocol.send({
  client_id: 'XXXXXXXXXX.YYYYYYYYYY',
  events: [
    {
      name: 'purchase_refund',
      params: {
        currency: 'USD',
        value: '9.99',
        transaction_id: 'ABC-123',
      },
    },
  ],
});
```

For more information, see the [Measurement Protocol Guide](https://developers.google.com/analytics/devguides/collection/protocol/ga4).
