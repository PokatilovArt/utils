import React, { useContext } from 'react';

/* eslint-disable @typescript-eslint/no-explicit-any */

export interface Constraint {
  name: string;
  message: string;
}

export interface FormContextProps {
  data: any;
  setData: (data: any) => void;

  fieldErrors: Record<string, Constraint>;
  setFieldErrors: (errors: Record<string, Constraint>) => void;

  formError?: string;
  setFormError: (error: string) => void;
}

const FormContext = React.createContext<FormContextProps>({
  data: {},
  setData: () => {},

  fieldErrors: {},
  setFieldErrors: () => {},

  formError: undefined,
  setFormError: () => {},
});

export const useForm = (): FormContextProps => useContext(FormContext);

FormContext.displayName = 'FormContext';

export default FormContext;
