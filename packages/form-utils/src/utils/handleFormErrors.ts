import { Constraint } from './FormContext';
import getErrorConstraints from './getErrorConstraints';

export interface ErrorHandlers {
  setFieldErrors: (errors: Record<string, Constraint>) => void;
  setFormError: (error: string) => void;
}

const handleFormErrors = (error: Error, handlers: ErrorHandlers): void => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const e = error as any;

  if (
    !e.source ||
    !Array.isArray(e.source.errors) ||
    e.source.errors.length === 0 ||
    !e.source.errors[0].extensions ||
    !e.source.errors[0].extensions.code
  ) {
    handlers.setFormError(e.message);
    return;
  }

  if (e.source.errors[0].extensions.code === 'BAD_ARGUMENTS') {
    handlers.setFieldErrors(getErrorConstraints(error));
  } else {
    handlers.setFormError(e.source.errors[0].message);
  }
};

export default handleFormErrors;
