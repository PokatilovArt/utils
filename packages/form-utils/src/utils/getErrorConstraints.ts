import { Constraint } from './FormContext';

const getErrorConstraints = (error: Error): Record<string, Constraint> => {
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const e = error as any;

  if (
    !e.source ||
    !Array.isArray(e.source.errors) ||
    e.source.errors.length === 0 ||
    !e.source.errors[0].extensions ||
    !e.source.errors[0].extensions.code
  ) {
    return {};
  }

  if (e.source.errors[0].extensions.code !== 'BAD_ARGUMENTS') return {};
  return e.source.errors[0].extensions.constraints;
};

export default getErrorConstraints;
