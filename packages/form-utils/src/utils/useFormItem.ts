import { useCallback, useMemo } from 'react';
import { useForm } from './FormContext';

/* eslint-disable @typescript-eslint/no-explicit-any */

export interface UseFormItemOptions {
  /**
   * The function to transform the input value.
   */
  transformer?: (nextData: any) => any;
  /**
   * The help message located at the bottom of the field.
   * @default undefined
   */
  help?: string;
}

interface FormItemProps {
  help?: string;
  hasError: boolean;
}

interface ComponentProps {
  value: any;
  onChange: (value: any) => void;
}

export type UseFormItemRes = [FormItemProps, ComponentProps];

const useFormItem = (
  key: string,
  { transformer = (nextData) => nextData, help }: UseFormItemOptions = {}
): UseFormItemRes => {
  const { data, setData, fieldErrors } = useForm();

  const onChange = useCallback(
    (value: any) => {
      setData(transformer({ ...data, [key]: value }));
    },
    [key, transformer, data, setData]
  );

  const formItemProps = useMemo(
    () => ({
      help: fieldErrors[key] ? fieldErrors[key].message : help,
      hasError: !!fieldErrors[key],
    }),
    [fieldErrors, help, key]
  );

  const componentProps = useMemo(
    () => ({
      value: data[key],
      onChange,
    }),
    [data, key, onChange]
  );

  return [formItemProps, componentProps];
};

export default useFormItem;
