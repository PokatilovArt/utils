# @os-team/form-utils [![NPM version](https://img.shields.io/npm/v/@os-team/form-utils)](https://yarnpkg.com/package/@os-team/form-utils) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/form-utils)](https://bundlephobia.com/result?p=@os-team/form-utils)

Utils for working with forms.

The library is compatible with `os-design` and `os-design-mobile`.

## Usage

Install the package using the following command:

```
yarn add @os-team/form-utils
```
