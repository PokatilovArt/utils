import fetch from 'isomorphic-unfetch';

export interface YandexCloudTranslator<T> {
  translate: (input: T, to: string) => Promise<T>;
}

const createTranslator = <T = string | string[]>(
  apiKey: string
): YandexCloudTranslator<T> => ({
  translate: async (input, to) => {
    const texts = Array.isArray(input) ? input : [input];

    const response = await fetch(
      'https://translate.api.cloud.yandex.net/translate/v2/translate',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Api-Key ${apiKey}`,
        },
        body: JSON.stringify({
          texts,
          targetLanguageCode: to,
        }),
      }
    );

    const res = await response.json();

    if (!res.translations) {
      throw new Error(res.message || 'Translations are not found');
    }

    if (Array.isArray(input)) {
      return res.translations.map((item) => item.text);
    }
    return res.translations[0].text;
  },
});

export default createTranslator;
