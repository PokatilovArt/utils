# @os-team/yoo-money [![NPM version](https://img.shields.io/npm/v/@os-team/yoo-money)](https://yarnpkg.com/package/@os-team/yoo-money) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/yoo-money)](https://bundlephobia.com/result?p=@os-team/yoo-money)

YooMoney API v3. All methods described in the [official documentation](https://yookassa.ru/en/developers/api) are implemented (payments, refunds and receipts).

## Examples

### Create a payment

```ts
const yooMoney = new YooMoney({
  shopId: 'shopId',
  secretKey: 'secretKey',
});

const payment = await yooMoney.payments.create(
  {
    amount: {
      value: '2.00',
      currency: 'RUB',
    },
    payment_method_data: {
      type: 'bank_card',
    },
    confirmation: {
      type: 'redirect',
      return_url: 'https://www.merchant-website.com/return_url',
    },
    description: 'Order No. 1',
  },
  'idempotenceKey'
);
```

### Create a recurring payment

```ts
// Create a payment and save a payment method
const firstPayment = await yooMoney.payments.create(
  {
    amount: {
      value: '2.00',
      currency: 'RUB',
    },
    payment_method_data: {
      type: 'bank_card',
    },
    confirmation: {
      type: 'redirect',
      return_url: 'https://www.merchant-website.com/return_url',
    },
    description: 'Order No. 1',
    save_payment_method: true,
    capture: true,
  },
  'idempotenceKey'
);

// Using the saved payment method, create a recurring payment
const secondPayment = await yooMoney.payments.create(
  {
    amount: {
      value: '2.00',
      currency: 'RUB',
    },
    payment_method_id: firstPayment.payment_method.id,
    description: 'Order No. 2',
    capture: true,
  },
  'idempotenceKey'
);
```

## Supported methods

- payments.create
- payments.list
- payments.get
- payments.capture
- payments.cancel
- refunds.create
- refunds.list
- refunds.get
- receipts.create
- receipts.list
- receipts.get
