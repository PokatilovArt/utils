import { BaseList, BaseListParams } from '../baseList';
import { Receipt, ReceiptStatus } from './receipt';

export interface ReceiptListParams extends BaseListParams {
  /**
   * Filter by receipt status.
   */
  status?: ReceiptStatus;
  /**
   * Filter by payment ID.
   */
  payment_id?: string;
  /**
   * Filter by refund ID.
   */
  refund_id?: string;
}

export type ReceiptList = BaseList<Receipt>;
