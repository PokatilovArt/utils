import { BaseList, BaseListParams } from '../baseList';
import { Refund, RefundStatus } from './refund';

export interface RefundListParams extends BaseListParams {
  /**
   * Filter by payment ID.
   */
  payment_id?: string;
  /**
   * Filter by refund status.
   */
  status?: RefundStatus;
}

export type RefundList = BaseList<Refund>;
