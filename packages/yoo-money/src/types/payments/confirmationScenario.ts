export interface BaseConfirmationScenario {
  /**
   * Confirmation scenario code.
   */
  type: string;
}

export interface EmbeddedConfirmationScenario extends BaseConfirmationScenario {
  /**
   * Confirmation scenario code.
   */
  type: 'embedded';
  /**
   * Token for the YooMoney widget initialization.
   */
  confirmation_token: string;
}

export interface ExternalConfirmationScenario extends BaseConfirmationScenario {
  /**
   * Confirmation scenario code.
   */
  type: 'external';
}

export interface QRCodeConfirmationScenario extends BaseConfirmationScenario {
  /**
   * Confirmation scenario code.
   */
  type: 'qr';
  /**
   * Data for generating the QR code.
   */
  confirmation_data: string;
}

export interface RedirectConfirmationScenario extends BaseConfirmationScenario {
  /**
   * Confirmation scenario code.
   */
  type: 'redirect';
  /**
   * The URL that the user will be redirected to for payment confirmation.
   */
  confirmation_url: string;
  /**
   * A request for making a payment with authentication by 3-D Secure.
   */
  enforce?: boolean;
  /**
   * The URL that the user will return to after confirming or canceling the payment on the webpage.
   */
  return_url?: string;
}

export type ConfirmationScenario =
  | EmbeddedConfirmationScenario
  | ExternalConfirmationScenario
  | QRCodeConfirmationScenario
  | RedirectConfirmationScenario;
