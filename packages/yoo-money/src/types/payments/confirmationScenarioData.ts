export type ConfirmationScenarioDataLocale = 'ru_RU' | 'en_US';

export interface BaseConfirmationScenarioData {
  /**
   * Confirmation scenario code.
   */
  type: string;
  /**
   * Language of the interface, emails, and text messages that will be displayed and
   * sent to the user.
   */
  locale?: ConfirmationScenarioDataLocale;
}

export interface EmbeddedConfirmationScenarioData
  extends BaseConfirmationScenarioData {
  /**
   * Confirmation scenario code.
   */
  type: 'embedded';
}

export interface ExternalConfirmationScenarioData
  extends BaseConfirmationScenarioData {
  /**
   * Confirmation scenario code.
   */
  type: 'external';
}

export interface QRCodeConfirmationScenarioData
  extends BaseConfirmationScenarioData {
  /**
   * Confirmation scenario code.
   */
  type: 'qr';
}

export interface RedirectConfirmationScenarioData
  extends BaseConfirmationScenarioData {
  /**
   * Confirmation scenario code.
   */
  type: 'redirect';
  /**
   * A request for making a payment with authentication by 3-D Secure.
   */
  enforce?: boolean;
  /**
   * The URL that the user will return to after confirming or canceling the payment on the webpage.
   */
  return_url: string;
}

export type ConfirmationScenarioData =
  | EmbeddedConfirmationScenarioData
  | ExternalConfirmationScenarioData
  | QRCodeConfirmationScenarioData
  | RedirectConfirmationScenarioData;
