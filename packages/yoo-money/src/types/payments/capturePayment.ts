import { Amount } from '../amount';
import { CreatePaymentData } from './createPaymentData';

export interface CapturePaymentData
  extends Pick<CreatePaymentData, 'receipt' | 'airline' | 'transfers'> {
  /**
   * Total amount charged to the user.
   */
  amount?: Amount;
}
