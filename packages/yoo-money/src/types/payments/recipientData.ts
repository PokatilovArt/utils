export interface RecipientData {
  /**
   * Subaccount's ID.
   */
  gateway_id: string;
}
