/* eslint-disable-next-line @typescript-eslint/no-explicit-any */
export interface BaseListParams extends Record<string, any> {
  /**
   * Filter by creation time:
   * the time must be greater than or equal to the specified value.
   */
  'created_at.gte'?: string;
  /**
   * Filter by creation time:
   * the time must be greater than the specified value.
   */
  'created_at.gt'?: string;
  /**
   * Filter by creation time:
   * the time must be less than or equal to the specified value.
   */
  'created_at.lte'?: string;
  /**
   * Filter by creation time:
   * the time must be less than the specified value.
   */
  'created_at.lt'?: string;
  /**
   * The count of items (from 1 to 100).
   */
  limit?: number;
  /**
   * Pointer to the next section of the list.
   */
  cursor?: string;
}

export interface BaseList<T> {
  type: 'list';
  items: T[];
  next_cursor?: string;
}
