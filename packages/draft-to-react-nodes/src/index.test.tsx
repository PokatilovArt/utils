import '@testing-library/jest-dom';
import React from 'react';
import { render } from '@testing-library/react';
import draftJsState from './test-files/draftjs-state.json';
import draftToReactNodes from './index';

it('Should render a Draft.js state', () => {
  const content = draftToReactNodes({ value: draftJsState });
  const { container, getByText } = render(content as React.ReactElement);

  // Test block elements
  expect(getByText('Unstyled').tagName).toBe('P');
  expect(getByText('Paragraph').tagName).toBe('P');
  expect(getByText('Header 1').tagName).toBe('H1');
  expect(getByText('Header 2').tagName).toBe('H2');
  expect(getByText('Header 3').tagName).toBe('H3');
  expect(getByText('Header 4').tagName).toBe('H4');
  expect(getByText('Header 5').tagName).toBe('H5');
  expect(getByText('Header 6').tagName).toBe('H6');
  expect(getByText('Unordered list item 1').tagName).toBe('LI');
  expect(getByText('Unordered list item 2').tagName).toBe('LI');
  expect(getByText('Ordered list item 1').tagName).toBe('LI');
  expect(getByText('Ordered list item 2').tagName).toBe('LI');
  expect(getByText('Blockquote').tagName).toBe('BLOCKQUOTE');
  expect(getByText('Code block').tagName).toBe('PRE');
  expect(getByText('Atomic').tagName).toBe('FIGURE');
  expect(container.querySelector('ul')).toContainElement(
    getByText('Unordered list item 1')
  );
  expect(container.querySelector('ol')).toContainElement(
    getByText('Ordered list item 1')
  );

  // Test inline style elements
  expect(getByText('bold').tagName).toBe('B');
  expect(getByText('code').tagName).toBe('CODE');
  expect(getByText('italic').tagName).toBe('I');
  expect(getByText('strikethrough').tagName).toBe('S');
  expect(getByText('underline').tagName).toBe('U');

  // If the last child is a list, then it should also be rendered
  expect(getByText('Last list item')).toBeInTheDocument();

  // Match the entire snapshot
  expect(container).toMatchSnapshot();
});

describe('Should render a custom block element', () => {
  test('paragraph', () => {
    const content = draftToReactNodes({
      value: draftJsState,
      blockRenderer: (block, children) => {
        if (block.type === 'paragraph') {
          return <div key={block.key}>{children}</div>;
        }
        return null;
      },
    });
    const { getByText } = render(content as React.ReactElement);
    expect(getByText('Paragraph').tagName).toBe('DIV');
  });

  test('atomic:image', () => {
    const content = draftToReactNodes({
      value: draftJsState,
      blockRenderer: (block) => {
        if (block.type === 'atomic:image') {
          return <img key={block.key} src={block.data.src} alt={block.text} />;
        }
        return null;
      },
    });
    const { container } = render(content as React.ReactElement);
    const image = container.querySelector('img');
    expect(image).toHaveAttribute('src', 'https://picsum.photos/200');
    expect(image).toHaveAttribute('alt', 'Image description');
  });
});

it('Should render a custom entity element', () => {
  const content = draftToReactNodes({
    value: draftJsState,
    entityRenderer: (entity) => {
      if (
        entity.type === 'LINK' &&
        entity.data &&
        typeof entity.data.url === 'string'
      ) {
        return (
          <a key={entity.key} href={entity.data.url}>
            {entity.text}
          </a>
        );
      }
      return null;
    },
  });
  const { container } = render(content as React.ReactElement);
  const link = container.querySelector('a');
  expect(link).toHaveAttribute('href', 'https://google.com');
  expect(link).toHaveTextContent('link');
});

describe('Should render a custom inline style element', () => {
  test('BOLD', () => {
    const content = draftToReactNodes({
      value: draftJsState,
      inlineStyleRenderer: (inlineStyle) => {
        if (inlineStyle.style === 'BOLD') {
          return (
            <span key={inlineStyle.key} className='bold'>
              {inlineStyle.text}
            </span>
          );
        }
        return null;
      },
    });
    const { getByText } = render(content as React.ReactElement);
    const boldElement = getByText('bold');
    expect(boldElement.tagName).toBe('SPAN');
    expect(boldElement).toHaveClass('bold');
  });

  test('CUSTOM', () => {
    const content = draftToReactNodes({
      value: draftJsState,
      inlineStyleRenderer: (inlineStyle) => {
        if (inlineStyle.style === 'CUSTOM') {
          return (
            <span key={inlineStyle.key} className='custom'>
              {inlineStyle.text}
            </span>
          );
        }
        return null;
      },
    });
    const { getByText } = render(content as React.ReactElement);
    const customElement = getByText('custom');
    expect(customElement.tagName).toBe('SPAN');
    expect(customElement).toHaveClass('custom');
  });
});
