import React from 'react';

export const mapInlineStyleTag = {
  BOLD: 'b',
  CODE: 'code',
  ITALIC: 'i',
  STRIKETHROUGH: 's',
  UNDERLINE: 'u',
};

interface InlineStyle {
  key: number;
  text: string;
  style: string;
}

export type InlineStyleRenderer = (inlineStyle: InlineStyle) => React.ReactNode;

const createInlineStyleElement = (
  inlineStyle: InlineStyle,
  inlineStyleRenderer: InlineStyleRenderer = () => null
): React.ReactNode => {
  // Try to render the inline style by `inlineStyleRenderer`
  const inlineStyleElement = inlineStyleRenderer(inlineStyle);
  if (inlineStyleElement) return inlineStyleElement;

  // Render the inline style by default
  const tag = mapInlineStyleTag[inlineStyle.style];
  if (!tag) return inlineStyle.text;
  return React.createElement(tag, { key: inlineStyle.key }, inlineStyle.text);
};

export default createInlineStyleElement;
