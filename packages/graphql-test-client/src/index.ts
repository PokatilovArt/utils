import superagent, { SuperAgentStatic } from 'superagent';
import fs from 'fs';

type Headers = Record<string, string>;
type MultipartValueSingle =
  | Blob
  | Buffer
  | fs.ReadStream
  | string
  | boolean
  | number;

interface TestClientProps {
  url: string;
  sharedHeaders?: Headers;
  saveCookies?: boolean;
}

interface TestClientQueryProps {
  query: string;
  variables?: Record<string, any>; // eslint-disable-line @typescript-eslint/no-explicit-any
  uploadables?: Record<string, MultipartValueSingle>;
  headers?: Headers;
}

class GraphQLTestClient {
  private readonly url: string;

  private readonly sharedHeaders: Record<string, string>;

  private readonly agent: SuperAgentStatic;

  public constructor(props: TestClientProps) {
    this.url = props.url;
    this.sharedHeaders = props.sharedHeaders || {};
    this.agent = props.saveCookies ? superagent.agent() : superagent;
  }

  /**
   * Make a GraphQL request.
   */
  public query(props: TestClientQueryProps): superagent.SuperAgentRequest {
    const request = this.agent
      .post(this.url)
      .set({ ...this.sharedHeaders, ...props.headers });

    // Uses the specification for GraphQL multipart form requests.
    // See https://github.com/jaydenseric/graphql-multipart-request-spec
    if (props.uploadables) {
      if (!props.variables || !props.variables.input)
        throw new Error('Pass all params to the input variable');
      const map = {};

      Object.keys(props.uploadables).forEach((key, index) => {
        map[index.toString()] = [`variables.input.${key}`];
      });

      request.field({
        operations: JSON.stringify({
          query: props.query,
          variables: props.variables,
        }),
        map: JSON.stringify(map),
      });

      // Append files
      Object.values(props.uploadables).forEach((value, index) => {
        request.attach(index.toString(), value);
      });

      return request;
    }

    return request.send({
      query: props.query,
      variables: props.variables ? JSON.stringify(props.variables) : undefined,
    });
  }

  /**
   * Create a copy of TestClient that saves cookies.
   */
  public clone(): GraphQLTestClient {
    return new GraphQLTestClient({
      url: this.url,
      sharedHeaders: this.sharedHeaders,
      saveCookies: true,
    });
  }
}

export default GraphQLTestClient;
