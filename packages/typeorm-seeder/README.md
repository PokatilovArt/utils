# @os-team/typeorm-seeder [![NPM version](https://img.shields.io/npm/v/@os-team/typeorm-seeder)](https://yarnpkg.com/package/@os-team/typeorm-seeder)

Simple seeder for seeding data in tests using TypeORM.

## Usage

Install the package in devDependencies (-D) using the following command:

```
yarn add -D @os-team/typeorm-seeder
```

### Example of test

```ts
import 'reflect-metadata';
import { Connection, createConnection } from 'typeorm';
import createSeeder, { Seeder } from '@os-team/typeorm-seeder';
import User from '../../entities/User';
import Payment from '../../entities/Payment';

let connection: Connection;
let seeder: Seeder;

beforeEach(async () => {
  connection = await createConnection(); // Create the TypeORM connection
  seeder = createSeeder(connection); // Create the seeder
});

afterEach(async () => {
  await connection.close();
});

it('Should change the payment status to succeeded', async () => {
  // Seed a user
  const userData = {
    id: 1,
    name: 'name',
  };
  await seeder.seed(User, userData);

  // Seed payments
  await seeder.seed(Payment, [
    {
      id: 1,
      amount: 1000,
      status: 'pending',
      user: { id: userData.id },
    },
    {
      id: 2,
      amount: 2000,
      status: 'pending',
      user: { id: userData.id },
    },
  ]);

  // ...
});
```

Most likely you will use multiple test files (e.g. one test file for testing one API method).
In this case:

1. Put `beforeEach`, `afterEach` and global variables (connection, seeder) to a separate file. Export the `seeder` variable to use it in any test file.
1. Add the path to this file to [setupFilesAfterEnv](https://jestjs.io/docs/en/configuration#setupfilesafterenv-array). Jest will run this code before each test file in the suite is executed.
1. Set the `dropSchema` option in the TypeORM config to `true` **ONLY** in the test environment. TypeORM drops the schema each time connection is being established (before each test file). See more about [TypeORM connection options](https://github.com/typeorm/typeorm/blob/master/docs/connection-options.md).
