import { Connection, InsertResult } from 'typeorm';
import { QueryDeepPartialEntity } from 'typeorm/query-builder/QueryPartialEntity';
import { ObjectType } from 'typeorm/common/ObjectType';

export interface Seeder {
  seed: <Entity>(
    entity: ObjectType<Entity>,
    data: QueryDeepPartialEntity<Entity> | QueryDeepPartialEntity<Entity>[]
  ) => Promise<InsertResult>;
}

const createSeeder = (conn: Connection): Seeder => ({
  seed: (entity, data) =>
    conn.createQueryBuilder().insert().into(entity).values(data).execute(),
});

export default createSeeder;
