import {
  Column,
  Connection,
  createConnection,
  Entity,
  getConnectionOptions,
  PrimaryGeneratedColumn,
} from 'typeorm';
import index, { Seeder } from './index';

let conn: Connection;
let seeder: Seeder;

@Entity()
class User {
  @PrimaryGeneratedColumn()
  public id!: number;

  @Column()
  public name!: string;
}

beforeEach(async () => {
  // Initialize
  const options = await getConnectionOptions();
  conn = await createConnection({
    ...options,
    synchronize: true,
    dropSchema: true,
    entities: [User],
  });
  seeder = index(conn);
});

afterEach(async () => {
  await conn.close();
});

it('Should seed a single entity', async () => {
  await seeder.seed(User, { id: 1, name: 'Name' });

  const categories = await conn.getRepository(User).find();
  expect(categories.length).toBe(1);
  expect(categories[0].name).toBe('Name');
});

it('Should seed the array of entities', async () => {
  await seeder.seed(User, [
    { id: 1, name: 'Name1' },
    { id: 2, name: 'Name2' },
  ]);

  const categories = await conn
    .getRepository(User)
    .find({ order: { id: 'ASC' } });
  expect(categories.length).toBe(2);
  expect(categories[0].name).toBe('Name1');
  expect(categories[1].name).toBe('Name2');
});
