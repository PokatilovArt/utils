import * as path from 'path';
import { createReadStream } from 'fs';
import fetch from 'isomorphic-unfetch';
import { Storage } from '@google-cloud/storage';
import Index, { DEFAULT_SIZES, UploadConfig } from './index';

/* eslint-disable jest/expect-expect,jest/no-conditional-expect */

const TEST_FILES_DIR = 'test-files';
const BUCKET_DIR = 'test-image-storage';

const storage = new Storage({
  keyFilename: path.resolve(
    __dirname,
    TEST_FILES_DIR,
    'test-file-storage-key.json'
  ),
  projectId: 'os-team',
});

const imageStorage = new Index({
  bucket: storage.bucket('test-file-storage'),
  maxFileSize: 3 * 1024 * 1024,
});

const readTestFile = (fileName: string) =>
  createReadStream(path.resolve(__dirname, TEST_FILES_DIR, fileName));

const expectFilesCountToBe = async (filesPrefix: string, expected: number) => {
  const count = await imageStorage.count(`${BUCKET_DIR}/${filesPrefix}`);
  expect(count).toBe(expected);
};

const uploadShouldBeFailed = async (config: UploadConfig) => {
  let hasError = false;
  try {
    await imageStorage.upload(config);
  } catch (e) {
    hasError = true;
  }

  // Should throw an error
  expect(hasError).toBeTruthy();

  // There should be no image in the bucket
  await expectFilesCountToBe(config.fileName, 0);
};

const uploadShouldBeSuccess = async (config: UploadConfig) => {
  // There should be no image in the bucket
  await expectFilesCountToBe(config.fileName, 0);

  // Should upload the image successfully
  const res = await imageStorage.upload(config);
  expect(res.startsWith(config.fileName)).toBeTruthy();

  // Should be accessible via a public path
  if (config.sizes) {
    config.sizes.forEach((size) => {
      const url = `https://storage.googleapis.com/test-file-storage/${BUCKET_DIR}/${res}-${size}`;
      fetch(url)
        .then((response) => {
          expect(response.ok).toBeTruthy();
          return undefined;
        })
        .catch((e) => e);
      fetch(`${url}-c`)
        .then((response) => {
          expect(response.ok).toBeTruthy();
          return undefined;
        })
        .catch((e) => e);
    });
  }

  // There should be original and cropped images in the bucket for each size
  await expectFilesCountToBe(
    config.fileName,
    (config.sizes || DEFAULT_SIZES).length * 2
  );
};

beforeAll(async () => {
  await imageStorage.delete(BUCKET_DIR);
});

afterAll(async () => {
  // Comment the imageStorage.delete function if you want to see the resulting files in the bucket
  await imageStorage.delete(BUCKET_DIR);
});

it('Should throw the error because the image type is invalid (txt)', async () => {
  await uploadShouldBeFailed({
    stream: readTestFile('text.txt'),
    fileName: 'txt',
    bucketDir: BUCKET_DIR,
    sizes: [100],
  });
});

it('Should throw the error because the image type is invalid (mov)', async () => {
  await uploadShouldBeFailed({
    stream: readTestFile('video.mov'),
    fileName: 'mov',
    bucketDir: BUCKET_DIR,
    sizes: [100],
  });
});

it('Should throw the error because the image size is too large', async () => {
  await uploadShouldBeFailed({
    stream: readTestFile('3.4mb.png'),
    fileName: 'too-large',
    bucketDir: BUCKET_DIR,
    sizes: [100],
  });
});

it('Should throw the error because the size array is empty', async () => {
  await uploadShouldBeFailed({
    stream: readTestFile('1.2mb.png'),
    fileName: 'no-sizes',
    bucketDir: BUCKET_DIR,
    sizes: [],
  });
});

it('Should upload JPG', async () => {
  await uploadShouldBeSuccess({
    stream: readTestFile('104kb.jpg'),
    fileName: 'jpg',
    bucketDir: BUCKET_DIR,
    sizes: [100],
  });
});

it('Should upload JPEG', async () => {
  await uploadShouldBeSuccess({
    stream: readTestFile('104kb.jpeg'),
    fileName: 'jpeg',
    bucketDir: BUCKET_DIR,
    sizes: [100],
  });
});

it('Should upload PNG', async () => {
  await uploadShouldBeSuccess({
    stream: readTestFile('1.2mb.png'),
    fileName: 'png',
    bucketDir: BUCKET_DIR,
    sizes: [100],
  });
});

it('Should upload WebP', async () => {
  await uploadShouldBeSuccess({
    stream: readTestFile('96kb.webp'),
    fileName: 'webp',
    bucketDir: BUCKET_DIR,
    sizes: [100],
  });
});

it('Should delete old images', async () => {
  const outputFileName = 'old';
  await expectFilesCountToBe(outputFileName, 0);

  const createPromise = () =>
    imageStorage.upload({
      fileName: outputFileName,
      stream: readTestFile('1.2mb.png'),
      bucketDir: BUCKET_DIR,
      sizes: [100],
    });

  await createPromise();
  await createPromise();

  await expectFilesCountToBe(outputFileName, 2);
});

it('Should upload image for each size', async () => {
  await uploadShouldBeSuccess({
    stream: readTestFile('1.2mb.png'),
    fileName: 'multiple-sizes',
    bucketDir: BUCKET_DIR,
    sizes: [100, 200, 500, 1000],
  });
});
