import { FileExtension, stream as fileTypeStream } from 'file-type';
import path from 'path';
import { Bucket } from '@google-cloud/storage';
import sharp, { ResizeOptions } from 'sharp';
import crypto from 'crypto';
import { ReadStream } from 'fs';

export interface Config {
  bucket: Bucket;
  maxFileSize?: number;
  supportedFileTypes?: FileExtension[];
}

export interface UploadConfig {
  stream: ReadStream;
  fileName: string;
  bucketDir?: string;
  sizes?: number[];
}

export const SUPPORTED_FILE_TYPES: FileExtension[] = ['jpg', 'png', 'webp'];
export const DEFAULT_MAX_FILE_SIZE: number = 20 * 1024 * 1024;
export const DEFAULT_SIZES: number[] = [72, 192, 512, 1024, 2560];

class ImageStorage {
  private readonly bucket: Bucket;

  private readonly maxFileSize: number;

  public constructor(config: Config) {
    this.bucket = config.bucket;
    this.maxFileSize = config.maxFileSize || DEFAULT_MAX_FILE_SIZE;
  }

  /**
   * Uploads an image to Google Storage.
   */
  public async upload(config: UploadConfig): Promise<string> {
    const { sizes = DEFAULT_SIZES } = config;
    const readableStream = await fileTypeStream(config.stream);
    const { fileType } = readableStream;

    // Validate the file type
    if (!fileType) {
      throw new Error(
        `Supported only ${SUPPORTED_FILE_TYPES.join(', ')} file types`
      );
    }
    if (!SUPPORTED_FILE_TYPES.includes(fileType.ext)) {
      throw new Error(
        `Type ${
          fileType.ext
        } is invalid. Supported only ${SUPPORTED_FILE_TYPES.join(
          ', '
        )} file types.`
      );
    }

    // Validate sizes
    if (sizes.length === 0) {
      throw new Error('Sizes must be is not empty');
    }

    // Join the bucket directory and the file name
    const fileBucketPath = config.bucketDir
      ? path.join(config.bucketDir, config.fileName)
      : config.fileName;

    // The hash is appended to the file name to avoid caching
    const hash = crypto.randomBytes(2).toString('hex');

    // Upload to Google Storage
    await new Promise((resolve, reject) => {
      const pipeline = sharp()
        .rotate()
        .flatten({ background: { r: 255, g: 255, b: 255 } })
        .jpeg({
          quality: 90,
          progressive: true,
        });

      let finished = 0;
      const finish = () => {
        finished += 1;
        if (finished === sizes.length * 2) resolve(true);
      };

      const addPipelineToSaveImage = (size: number, cropped: boolean) => {
        const resizeOptions: ResizeOptions = cropped
          ? { width: size, height: size }
          : { width: size };

        let fileName = `${fileBucketPath}-${hash}-${size}`;
        if (cropped) fileName += '-c';

        const writableBucketStream = this.bucket
          .file(fileName)
          .createWriteStream({ contentType: 'image/jpeg' })
          .on('finish', finish);

        pipeline.clone().resize(resizeOptions).pipe(writableBucketStream);
      };

      // Create pipelines to save original and cropped images for each size
      sizes.forEach((size) => {
        addPipelineToSaveImage(size, false);
        addPipelineToSaveImage(size, true);
      });

      // Validate the file size and upload all images
      let fileSize = 0;
      readableStream
        .on('data', (chunk) => {
          fileSize += chunk.length;

          if (fileSize > this.maxFileSize) {
            const maxFileSizeMb =
              Math.round((this.maxFileSize / 1024 / 1024) * 100) / 100;
            readableStream.destroy(
              new Error(
                `Image size should be less than ${maxFileSizeMb} megabytes`
              )
            );
          }
        })
        .on('error', reject)
        .pipe(pipeline);
    });

    // Delete old images from Google Storage
    await this.delete(fileBucketPath, `${fileBucketPath}-${hash}`);

    return `${config.fileName}-${hash}`;
  }

  /**
   * Deletes files in the bucket.
   */
  public async delete(prefix?: string, excludePrefix?: string): Promise<void> {
    if (!excludePrefix) {
      return this.bucket.deleteFiles({ prefix });
    }

    // If `excludePrefix` exists
    // 1. Get all files in the bucket by prefix
    const getFilesRes = await this.bucket.getFiles({ prefix });

    // 2. Create promises to delete found files, except for files starting with `excludePrefix`
    const promises: Promise<void>[] = [];
    getFilesRes[0].forEach((file) => {
      const isNotExcluded = !file.name.startsWith(excludePrefix);
      const isNotCropped = file.name.slice(-2) !== '-c';
      if (isNotExcluded && isNotCropped) {
        promises.push(this.delete(file.name));
      }
    });

    // 3. Fulfill all promises
    await Promise.all(promises);
    return Promise.resolve();
  }

  /**
   * Counts files in the bucket.
   */
  public async count(prefix?: string): Promise<number> {
    const getFilesRes = await this.bucket.getFiles({ prefix });
    return getFilesRes[0].length;
  }
}

export default ImageStorage;
