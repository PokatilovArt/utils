# @os-team/image-storage [![NPM version](https://img.shields.io/npm/v/@os-team/image-storage)](https://yarnpkg.com/package/@os-team/image-storage) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/image-storage)](https://bundlephobia.com/result?p=@os-team/image-storage)

Library for uploading images to Google Storage.

Usually when you want to store an image you need:

1. Validate the file type (e.g. allow only JPG, PNG, WEBP). The file type should be detected by the first bytes, not by the file name (e.g. using the [file-type](https://github.com/sindresorhus/file-type) library).
1. Validate the file size.
1. Upload the image.
1. Append a hash to the file name to avoid caching.
1. Convert the image to some extension (e.g. JPG).
1. Create multiple sizes and cropped versions of the image to use it on the frontend side in different places (e.g. a large avatar on the profile page, a small avatar in the header).
1. Delete old images that have been replaced with new ones.

This library performs all these steps.

## Usage

### Step 1. Install the package

Install the package using the following command:

```
yarn add @os-team/image-storage
```

### Step 2. Install `@google-cloud/storage`

Install it using the following command:

```
npx install-peerdeps @os-team/image-storage
```

### Step 3. Create an instance of ImageStorage

Create a separate file, e.g. `imageStorage.ts` with the following content:

```ts
import path from 'path';
import ImageStorage from '@os-team/image-storage';
import { Storage } from '@google-cloud/storage';

const storage = new Storage({
  keyFilename: path.resolve(__dirname, './google-cloud-key.json'),
  projectId: 'PROJECT_ID',
});

const bucket = storage.bucket('BUCKET_ID');
const imageStorage = new ImageStorage({ bucket });

export default imageStorage;
```

Now you can use the `imageStorage` variable anywhere.

### Step 4. Upload the image

```ts
const image = await imageStorage.upload({
  stream: (await input.image).createReadStream(),
  fileName: 'fileName',
});

console.log(image); // fileName-hash
```

By default, the `ImageStorage` uploads the following images:

- fileName-hash-72
- fileName-hash-192
- fileName-hash-512
- fileName-hash-1024
- fileName-hash-2560
- fileName-hash-72-c
- fileName-hash-192-c
- fileName-hash-512-c
- fileName-hash-1024-c
- fileName-hash-2560-c

The full path to the image looks like this:

```
https://storage.googleapis.com/BUCKET_ID/fileName-hash-72
```

### Step 5. Delete the image

In most cases, you need to delete an image when the object associated with that image has been deleted.
For example, if you delete a post, you also need to delete the post cover.

You can delete all images by passing the image prefix to the `delete` method.

```ts
await imageStorage.delete('fileName-hash');
```

Note that if you specify a bucket directory, your prefix should be:

```ts
await imageStorage.delete('directory/fileName-hash');
```

Similarly, the `count` method works, which returns the number of files with the specified prefix.

## Customization

### Set the max file size

Pass the max file size to the `ImageStorage` constructor:

```ts
const imageStorage = new ImageStorage({
  bucket,
  maxFileSize: 50 * 1024 * 1024, // 50 MB (20 MB by default)
});
```

### Set the bucket directory

Pass the bucket directory to the `upload` method:

```ts
const image = await imageStorage.upload({
  stream: (await input.image).createReadStream(), // Can be fs.createReadStream(path.resolve(__dirname, './image.jpg'))
  fileName: 'fileName',
  bucketDir: 'directory', // Can be `directory1/directory2`
});
```

Please note that if you change the bucket directory, the full path to the image will also be changed:

```
https://storage.googleapis.com/BUCKET_ID/directory/fileName-hash-72
```

### Set the image sizes

Pass the array of sizes to the `upload` method:

```ts
const image = await imageStorage.upload({
  stream: (await input.image).createReadStream(),
  fileName: 'fileName',
  sizes: [72, 192, 512, 1024], // The width of the image
});
```
