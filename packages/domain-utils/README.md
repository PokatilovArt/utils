# @os-team/domain-utils [![NPM version](https://img.shields.io/npm/v/@os-team/domain-utils)](https://yarnpkg.com/package/@os-team/domain-utils) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/domain-utils)](https://bundlephobia.com/result?p=@os-team/domain-utils)

Utils for extracting parts of a domain name.

## Usage

Install the package using the following command:

```
yarn add @os-team/domain-utils
```

### Getting a top level domain

```ts
import { getTopLevelDomain } from '@os-team/domain-utils';

console.log(getTopLevelDomain('sub.domain.com')); // com
```

### Getting a second level domain

```ts
import { getSecondLevelDomain } from '@os-team/domain-utils';

console.log(getSecondLevelDomain('sub.domain.com')); // domain
```

### Getting a root domain

```ts
import { getRootDomain } from '@os-team/domain-utils';

console.log(getRootDomain('sub.domain.com')); // domain.com
```
