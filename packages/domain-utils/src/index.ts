export { default as getRootDomain } from './utils/getRootDomain';
export { default as getSecondLevelDomain } from './utils/getSecondLevelDomain';
export { default as getTopLevelDomain } from './utils/getTopLevelDomain';
