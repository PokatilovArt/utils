import getTopLevelDomain from './getTopLevelDomain';
import getSecondLevelDomain from './getSecondLevelDomain';

const getRootDomain = (host: string): string => {
  const tld = getTopLevelDomain(host);
  const sld = getSecondLevelDomain(host);
  return `${sld}.${tld}`;
};

export default getRootDomain;
