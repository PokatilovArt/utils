const getSecondLevelDomain = (host: string): string => {
  const parts = host.split('.');
  return parts[parts.length - 2];
};

export default getSecondLevelDomain;
