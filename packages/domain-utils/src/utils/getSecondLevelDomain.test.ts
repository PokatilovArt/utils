import getSecondLevelDomain from './getSecondLevelDomain';

test('domain.com', () => {
  const sld = getSecondLevelDomain('domain.com');
  expect(sld).toBe('domain');
});

test('sub.domain.com', () => {
  const sld = getSecondLevelDomain('sub.domain.com');
  expect(sld).toBe('domain');
});

test('sub.sub.domain.com', () => {
  const sld = getSecondLevelDomain('sub.sub.domain.com');
  expect(sld).toBe('domain');
});
