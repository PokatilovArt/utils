import getTopLevelDomain from './getTopLevelDomain';

test('domain.com', () => {
  const tld = getTopLevelDomain('domain.com');
  expect(tld).toBe('com');
});

test('domain.ru', () => {
  const tld = getTopLevelDomain('domain.ru');
  expect(tld).toBe('ru');
});

test('sub.domain.com', () => {
  const tld = getTopLevelDomain('sub.domain.com');
  expect(tld).toBe('com');
});
