import getRootDomain from './getRootDomain';

test('domain.com', () => {
  const root = getRootDomain('domain.com');
  expect(root).toBe('domain.com');
});

test('sub.domain.com', () => {
  const root = getRootDomain('sub.domain.com');
  expect(root).toBe('domain.com');
});
