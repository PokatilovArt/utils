# @os-team/sendgrid-mailer [![NPM version](https://img.shields.io/npm/v/@os-team/sendgrid-mailer)](https://yarnpkg.com/package/@os-team/sendgrid-mailer) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/sendgrid-mailer)](https://bundlephobia.com/result?p=@os-team/sendgrid-mailer)

The wrapper of the @sendgrid/mail which allows to set the shared mail data.

## Usage

### Step 1. Install the package

Install the package using the following command:

```
yarn add @os-team/sendgrid-mailer
```

### Step 2. Create an instance of SendGridMailer

The shared mail data will be used when sending each email.

```ts
const mailer = new SendGridMailer({
  apiKey: process.env.SENDGRID_API_KEY,
  from: 'Company <mg@m.domain.com>',
  replyTo: 'support@domain.com',
});
```

### Step 3. Send an email

```ts
await mailer.send({
  to: 'user@domain.com',
  subject: 'Subject',
  text: 'Message',
});
```

See about sending emails in the [SendGrid documentation](https://sendgrid.api-docs.io/v3.0/mail-send/v3-mail-send).
