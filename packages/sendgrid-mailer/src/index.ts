import sgMail from '@sendgrid/mail';
import { MailContent, MailData } from '@sendgrid/helpers/classes/mail';

export type MailDataContent =
  | { text: string }
  | { html: string }
  | { templateId: string }
  | { content: MailContent[] & { 0: MailContent } };
export type MailDataRequired = Partial<MailData> & MailDataContent;

export interface SendgridMailerProps extends Partial<MailData> {
  apiKey: string;
}

const SUCCESS_STATUS_CODE = 202; // The message is both valid, and queued to be delivered

class SendGridMailer {
  protected readonly sharedMailData: MailData;

  public constructor({ apiKey, ...sharedMailData }: SendgridMailerProps) {
    sgMail.setApiKey(apiKey);
    this.sharedMailData = {
      from: 'from@example.com',
      ...sharedMailData,
    };
  }

  public async send(
    data: MailDataRequired | MailDataRequired[],
    isMultiple?: boolean
  ): Promise<boolean> {
    const sendgridData = Array.isArray(data)
      ? data.map((item) => ({ ...this.sharedMailData, ...item }))
      : { ...this.sharedMailData, ...data };
    const res = await sgMail.send(sendgridData, isMultiple);
    return res[0].statusCode === SUCCESS_STATUS_CODE;
  }

  public async sendMultiple(data: MailDataRequired): Promise<boolean> {
    const res = await sgMail.sendMultiple({ ...this.sharedMailData, ...data });
    return res[0].statusCode === SUCCESS_STATUS_CODE;
  }
}

export default SendGridMailer;
