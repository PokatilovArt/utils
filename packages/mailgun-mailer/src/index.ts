import mailgun, { ConstructorParams, Mailgun, messages } from 'mailgun-js';

type MailData =
  | messages.SendData
  | messages.BatchData
  | messages.SendTemplateData;

class MailgunMailer {
  protected readonly mailgun: Mailgun;

  protected readonly sharedMailData: Partial<MailData>;

  public constructor(
    params: ConstructorParams,
    sharedMailData: Partial<MailData> = {}
  ) {
    this.mailgun = mailgun(params);
    this.sharedMailData = sharedMailData;
  }

  public async send(data: MailData): Promise<boolean> {
    return new Promise((resolve) => {
      this.mailgun
        .messages()
        .send({ ...this.sharedMailData, ...data }, (error) => {
          resolve(!error);
        });
    });
  }
}

export default MailgunMailer;
