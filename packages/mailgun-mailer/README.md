# @os-team/mailgun-mailer [![NPM version](https://img.shields.io/npm/v/@os-team/mailgun-mailer)](https://yarnpkg.com/package/@os-team/mailgun-mailer) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/mailgun-mailer)](https://bundlephobia.com/result?p=@os-team/mailgun-mailer)

The wrapper of the mailgun-js which allows to set the shared mail data.

## Usage

### Step 1. Install the package

Install the package using the following command:

```
yarn add @os-team/mailgun-mailer
```

### Step 2. Create an instance of MailgunMailer

The shared mail data will be used when sending each email.

```ts
const sharedMailData = {
  from: 'Company <mg@m.domain.com>',
  'o:dkim': true,
  'o:tracking': true,
  'o:tracking-opens': true,
  'o:tracking-clicks': true,
  'h:Reply-To': 'support@domain.com',
};

const mailer = new MailgunMailer(
  {
    apiKey: process.env.MAILGUN_API_KEY,
    domain: 'm.domain.com',
    mute: process.env.NODE_ENV === 'production',
    host: 'api.eu.mailgun.net',
  },
  sharedMailData
);
```

### Step 3. Send an email

```ts
await mailer.send({
  to: 'user@domain.com',
  subject: 'Subject',
  text: 'Message',
});
```

See about sending emails in the [Mailgun documentation](https://documentation.mailgun.com/en/latest/api-sending.html).
