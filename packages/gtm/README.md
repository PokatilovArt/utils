# @os-team/gtm [![NPM version](https://img.shields.io/npm/v/@os-team/gtm)](https://yarnpkg.com/package/@os-team/gtm) [![BundlePhobia](https://img.shields.io/bundlephobia/minzip/@os-team/gtm)](https://bundlephobia.com/result?p=@os-team/gtm)

The library for working with Google Tag Manager.

Implements the official [Google Tag Manager Guide](https://developers.google.com/tag-manager/quickstart).

## Usage

### Step 1. Install the package

Install the package using the following command:

```
yarn add @os-team/gtm
```

### Step 2. Initialize Google Tag Manager

#### 2.1. Simple case

Call the init method and pass the ID of your GTM container.

```ts
import GoogleTagManager from '@os-team/gtm';

GoogleTagManager.init('GTM-XXXXXXX');
```

For example:

```tsx
import React, { useEffect } from 'react';
import GoogleTagManager from '@os-team/gtm';
import AppRouter from './AppRouter';

const App: React.FC = () => {
  // Initialize Google Tag Manager
  useEffect(() => {
    GoogleTagManager.init('GTM-XXXXXXX');
  }, []);

  return <AppRouter />;
};

export default App;
```

If you use `Next.js` we recommend initializing Google Tag Manager in the `./pages/_app.tsx` file:

```tsx
import React, { useEffect } from 'react';
import GoogleTagManager from '@os-team/gtm';

const App: React.FC = ({ Component, pageProps }) => {
  // Initialize Google Tag Manager
  useEffect(() => {
    GoogleTagManager.init('GTM-XXXXXXX');
  }, []);

  return <Component {...pageProps} />;
};

export default App;
```

#### 2.2. Set up the data layer

You can configure data layer variables when initializing the Google Tag Manager like this:

```ts
GoogleTagManager.init('GTM-XXXXXXX', {
  dataLayer: {
    userId: '1',
  },
});
```

In this case, the code of the GTM will be inserted after the data layer code, [as it should be](https://developers.google.com/tag-manager/devguide#incorrect).

#### 2.3. Rename the data layer

By default, the name of the data layer is `dataLayer`.
If you want, you can change this name to your own.

```ts
GoogleTagManager.init('GTM-XXXXXXX', {
  dataLayerName: 'customDataLayer',
});
```

#### 2.4. The debug mode

By default, the Google Tag Manager will be inserted into the HTML code only in the production environment.
If you want to test it, you should enable the debug mode.

```ts
GoogleTagManager.init('GTM-XXXXXXX', {
  debug: true,
});
```

You can also set up the environment variables as follows:

```ts
GoogleTagManager.init('GTM-XXXXXXX', {
  debug: true,
  env: {
    auth: 'XXXXXXXXXXXXXXXXXXXXXX',
    preview: 'env-1',
  },
});
```

For more information about the environments feature in Google Tag Manager, see [this help page](https://support.google.com/tagmanager/answer/6311518).

#### 2.5. Multiple containers on a page

You can insert more than one container on a page as follows:

```tsx
import React, { useEffect } from 'react';
import GoogleTagManager from '@os-team/gtm';
import AppRouter from './AppRouter';

const App: React.FC = () => {
  useEffect(() => {
    GoogleTagManager.init('GTM-XXXXXX1');
    GoogleTagManager.init('GTM-XXXXXX2');
  }, []);

  return <AppRouter />;
};

export default App;
```

For the best performance, initialize as few GTM containers as possible.

For more information about using multiple containers on a page, see [Google Tag Manager Guide / Multiple Containers on a Page](https://developers.google.com/tag-manager/devguide#multiple-containers).

### Step 3. Fire events

To fire an event you should push the information about it to the data layer.
This is accomplished by calling the `push` method as follows:

```ts
GoogleTagManager.push({
  event: 'event_name',
  variable_name: 'variable_value',
});
```

By default, the information will be added to the `dataLayer` layer, but you can change the layer name to your own:

```ts
GoogleTagManager.push(
  {
    event: 'event_name',
    variable_name: 'variable_value',
  },
  'customDataLayer'
);
```

For more information about using the data layer, see [Google Tag Manager Guide / Using the Data Layer with HTML Event Handlers](https://developers.google.com/tag-manager/devguide#events).
