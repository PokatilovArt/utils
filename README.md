# utils

Shared utils.

1. [@os-team/app-store-server-notifications](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/app-store-server-notifications) – Types for the App Store Server Notification. Contains the function to validate receipts.
1. [@os-team/app-ua-parser](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/app-ua-parser) – Detects the mobile app, OS, and device information from the user agent created by the @os-team/relay-network-mw-app-user-agent library.
1. [@os-team/aws-ses](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/aws-ses) – The wrapper of the @aws-sdk/client-ses which allows to set the shared mail data.
1. [@os-team/class-validators](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/class-validators) – Additional class validators.
1. [@os-team/domain-utils](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/domain-utils) – Utils for extracting parts of a domain name.
1. [@os-team/draft-to-html](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/draft-to-html) – Converts the Draft.js state (RawDraftContentState) to HTML.
1. [@os-team/draft-to-react-nodes](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/draft-to-react-nodes) – Converts the Draft.js state (RawDraftContentState) to ReactNode.
1. [@os-team/escape-string-regexp](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/escape-string-regexp) – Escapes special characters in a string for regular expressions.
1. [@os-team/form-utils](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/form-utils) – Utils for working with forms.
1. [@os-team/graphql-test-client](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/graphql-test-client) – The wrapper of the superagent to make GraphQL queries in tests.
1. [@os-team/graphql-to-rest](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/graphql-to-rest) – Creates a REST API using the GraphQL schema.
1. [@os-team/graphql-transformers](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/graphql-transformers) – The type-graphql decorator to transform arguments. Contains the most used transformers.
1. [@os-team/graphql-utils](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/graphql-utils) – Shared utils for GraphQL.
1. [@os-team/graphql-validators](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/graphql-validators) – The type-graphql decorator to validate arguments using i18next. Contains the most used validators.
1. [@os-team/gtm](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/gtm) – The Google Tag Manager initializer.
1. [@os-team/i18next-react-native-language-detector](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/i18next-react-native-language-detector) – The i18next language detector, which is used to detect the user's language in React Native.
1. [@os-team/i18next-tld-language-detector](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/i18next-tld-language-detector) – The i18next language detector, which is used to detect the user's language by a top-level domain (TLD).
1. [@os-team/image-storage](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/image-storage) – Library for uploading images to Google Storage.
1. [@os-team/mailgun-mailer](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/mailgun-mailer) – The wrapper of the mailgun-js which allows to set the shared mail data.
1. [@os-team/measurement-protocol](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/measurement-protocol) – The Measurement Protocol allows sending events to Google Analytics by making HTTP requests.
1. [@os-team/password-score](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/password-score) – Calculates a password strength score.
1. [@os-team/plural-forms](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/plural-forms) – Declines nouns in the plural form.
1. [@os-team/relay-network-creator](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/relay-network-creator) – Super tiny network layer for Relay.
1. [@os-team/relay-network-mw-app-user-agent](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/relay-network-mw-app-user-agent) – The middleware for @os-team/relay-network-creator to include more detailed app's user agent in each request.
1. [@os-team/relay-network-mw-credentials](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/relay-network-mw-credentials) – The middleware for @os-team/relay-network-creator to include credentials in each request.
1. [@os-team/relay-network-mw-timeout](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/relay-network-mw-timeout) – The middleware for @os-team/relay-network-creator to abort a request if it takes more than N ms.
1. [@os-team/relay-network-mw-upload](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/relay-network-mw-upload) – The middleware for @os-team/relay-network-creator to transform each request by GraphQL multipart request specification for file uploads.
1. [@os-team/sendgrid-mailer](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/sendgrid-mailer) – The wrapper of the @sendgrid/mail which allows to set the shared mail data.
1. [@os-team/session](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/session) – Reliable, feature-rich, easy-to-use session middleware for Express, developed based on the OWASP recommendations. Stores sessions in Redis. 100% test coverage.
1. [@os-team/sitemap](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/sitemap) – Creates the express middleware to generate the sitemap.xml file.
1. [@os-team/tinkoff-oplata](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/tinkoff-oplata) – Tinkoff Oplata API. All methods described in the official documentation are implemented.
1. [@os-team/typeorm-seeder](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/typeorm-seeder) – Simple seeder for seeding data in tests using TypeORM.
1. [@os-team/typeorm-utils](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/typeorm-utils) – Additional functions to work with databases using TypeORM.
1. [@os-team/yc-translate](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/yc-translate) – Translates the text to the specific language by Yandex.Cloud.
1. [@os-team/yoo-money](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/yoo-money) – YooMoney API v3. All methods described in the official documentation are implemented (payments, refunds and receipts).
1. [@os-team/youtube-captions](https://gitlab.com/os-team/libs/utils/-/tree/master/packages/youtube-captions) – The library for extracting subtitles of videos from YouTube without using the API.
